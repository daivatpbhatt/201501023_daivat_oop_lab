/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Help a shop store data of customers and products.
Date: 11th January, 2015
*/



#include <cstdlib>
#include<iostream>
#include<string>

using namespace std;

class customer
{
    string cust_name, address;

    public:

         string getName()
         {
             return cust_name;
         }

         string gerAddr()
         {
            return address;
         }

         void setName(string i)
         {
            cust_name=i;
         }

         void setAddr(string i)
         {
            address=i;
         }

};

class item
{
	string prod_name;
	int qty;
	float price;

    public:

        string getName()
        {
            return prod_name;
        }

        void setName(string i)
        {
           prod_name=i;
        }

        float getPrice()
        {
            return price;
        }

        void setPrice(float i)
        {
            price=i;
        }

        int getqty()
        {
            return qty;
        }
        void setqty(int i)
        {
            qty=i;
        }
};

int main(int argc, char** argv)
{
	customer c;
	item i;
	string n;
	int ch, itemp;
	float ftemp;

	while(true)
	{
        cout<<"\n\n1.Create Customer\n2.Create Item\n3. Create Bill\n0. Exit: ";
        cin>>ch;

        switch(ch)
        {
            case 1:
                cout<<"\nEnter name: ";
                cin>>n;
                c.setName(n);

                cout<<"Enter Address: ";
                cin.ignore(1000,'\n');
                getline(cin,n);
                c.setAddr(n);
                break;

            case 2:

                cout<<"\nEnter name: ";
                cin>>n;
                i.setName(n);

                cout<<"Enter qty: ";
                cin>>itemp;
                i.setqty(itemp);

                cout<<"Enter price: ";
                cin>>ftemp;
                i.setPrice(ftemp);
                break;

            case 3:

                cout<<"\nCustomer Name: "<<c.getName();
                cout<<"\nCustomer Address: "<<c.gerAddr();
                cout<<"\nItem name: "<<i.getName();
                cout<<"\nItem no: "<<i.getqty();
                cout<<"\nItem price: "<<i.getPrice();
                break;

            case 0:
                return 0;

        }
	}

}