/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Help the bank save the data and transactions of a customer.
Date: 10th January, 2016
*/


#include <cstdlib>
#include<iostream>
#include<string>

using namespace std;

class customer
{
    int savAccountNum, currAccountNum, branch;
    string cust_name, address;
    float currAccountBal, savAccountBal;

    public:

        int getsavAccountNum()
        {
            return savAccountNum;
        }

        int getcurrAccountNum()
        {
            return currAccountNum;
        }

        string getName()
        {
            return cust_name;
        }

        string gerAddr()
        {
            return address;
        }

        float getsavAccountBal()
        {
            return savAccountBal;
        }

        float getcurrAccountBal()
        {
            return currAccountBal;
        }

        void setsavAccountNum(int i)
        {
            savAccountNum=i;
        }

        void setcurrAccountNum(int i)
        {
            currAccountNum=i;
        }

        void setName(string i)
        {
            cust_name=i;
        }

        void setAddr(string i)
        {
            address=i;
        }

        void setcurrAccountBal(float i)
        {
            currAccountBal=i;
        }

        void setsavAccountBal(float i)
        {
            savAccountBal=i;
        }

        void setBranch(int i)
        {
            branch=i;
        }

        //TRANSACTIONS

        bool withdrawC(float i)
        {

            float bal = getcurrAccountBal();
            bal=bal-i;

            if(bal>1000)
            {
            setcurrAccountBal(bal);
            return true;
            }

            else
                return false;
        };

        bool withdrawS(float i)
        {
            float bal = getsavAccountBal();
            bal=bal-i;

            if (bal>1000)
            {
                setsavAccountBal(bal);
                return true;
            }

            else
                return false;
        };

        void depC(float i)
        {
            float bal = getcurrAccountBal();
            bal=bal+i;
            setcurrAccountBal(bal);
        };

        void depS(float i)
        {
            float bal = getsavAccountBal();
            bal=bal+i;
            setsavAccountBal(bal);
        };

	cust()      //Constructor to initialize balance to 0
	{
		currAccountBal=0;
		savAccountBal=0;
	}

};



int main(int argc, char** argv) {

	customer p;
	string n;


	int ch,tmp, acc, trans;
	float amount;
	bool err;

	while(true)
	{
        cout<<"\n\nSelect your choice\n";
        cout<<"1.Create Customer\n2.Get account info\n3.Transaction\n0.Exit: \n";
        cin>>ch;


        switch(ch)
        {
            case 1:

                cout<<"Enter Name: ";
                cin>>n;
                p.setName(n);

                cout<<"Enter Address: ";
                cin.ignore(1000,'\n');
                getline(cin,n);
                p.setAddr(n);

                cout<<"Enter Branch: ";
                cin>>tmp;
                p.setBranch(tmp);

                cout<<("Enter currAccountNum Account: ");
                cin>>tmp;
                p.setcurrAccountNum(tmp);

                cout<<"Enter savAccountNum Account: ";
                cin>>tmp;
                p.setsavAccountNum(tmp);

                break;

            case 2:

                cout<<"\nName: "<<p.getName();
                cout<<"\nAddress: "<<p.gerAddr();
                cout<<"\nSaving account number: "<<p.getsavAccountNum();
                cout<<"\nSaving account balance: "<<p.getsavAccountBal();
                cout<<"\nCurrent account number: "<<p.getcurrAccountNum();
                cout<<"\nCurrent account balance: "<<p.getcurrAccountBal();

                break;

            case 3:

                cout<<"\nSelect account: \n1.Saving\n2.Current: ";
                cin>>acc;

                cout<<"\n1. Withdraw\n2. Deposit";
                cin>>trans;

                cout<<"\nEnter amount: ";
                cin>>amount;

                if(acc==1)//for saving account
                {
                    if(trans==1)//withdraw
                    {
                        err=p.withdrawS(amount);

                        //min balance required is Rs. 1000
                        if(err==true)
                            cout<<"\nCash withdrawn successfully";
                        else
                            cout<<"\nCannot withdraw cash. Balance is below 1000";
                    }

                    else if (trans == 2)//deposit
                    {
                        p.depS(amount);

                        cout<<"\nCash Deposited Successfully";
                    }

                    else
                        cout<<"\nSelect valid choice";

                }

                else if(acc==2)//for current account
                {
                    if(trans==1)//withdraw
                    {
                        err=p.withdrawC(amount);

                        //Min balance required is Rs. 1000
                        if(err==true)
                            cout<<"\nCash withdrawn successfully";
                        else
                            cout<<"\nCannot withdraw cash. Min Balance should be 1000";
                    }

                    else if (trans == 2)//deposit
                    {
                        p.depC(amount);

                        cout<<"\nCash Deposited Successfully";
                    }

                    else
                        cout<<"\nSelect valid choice";

                }


                break;
            case 0:
                return 0;

        }
    }

    return 0;
}