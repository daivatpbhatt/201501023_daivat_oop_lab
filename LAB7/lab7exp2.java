/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Create a Vehicle categorization system using Linked List. Vehicles are either two wheelers or four wheelers.
Date: 9th March, 2016
*/

package lab7exp2;

import java.util.*;

abstract class Vehicle                                                  //Abstract Child Class.
{
    protected String name, chassis_num, company, datebought, colour, owner_name, owner_contact;
    protected double price;
    public int mileage;
    public boolean insurance, sold;
    protected Engine eng;                       //Reference to Engine Class
    
    
    public Vehicle()                            //Constructor to initialise Values.
    {
        name=null;
        chassis_num=null;
        company=null;
        datebought=null;
        owner_name=null;
        owner_contact=null;
        price=0;
        mileage=0;
        insurance = false;
        eng=new Engine();                   //Object of Engine Class.
    }
    
    
    public void readdetails()                   //Member function to read details.
    {
        Scanner in = new Scanner(System.in);
        System.out.println("\nEnter Name of the Vehicle: ");
        name = in.next();
        System.out.println("Enter Company of the Vehicle: ");
        company = in.next();
        System.out.println("Enter Chassis Number of the Vehicle: ");
        chassis_num = in.next();
        System.out.println("Enter Price of the Vehicle: ");
        price = in.nextDouble();
        System.out.println("Enter Approx. Mileage of the Vehicle: ");
        mileage = in.nextInt();
        System.out.println("Colour of Vehicle: ");
        colour=in.next();
        System.out.println("Insurance Existing for Vehicle? (1: Yes ; 0: No): ");
        if(in.nextInt()==1)
            insurance = true;
        else
            insurance = false;
        
        System.out.println("Vehicle Sold? (1: Yes ; 0: No): ");
        if(in.nextInt()==1)
            sold = true;
        else
            sold = false;
        
        if(sold)
        {
            System.out.println("Enter the Date of Sale of Vehicle(DD/MM/YYYY): ");
            datebought=in.next();
            {
                System.out.println("\nEnter the Name of the Owner: ");
                owner_name=in.next();
                System.out.println("Enter Contact number of the Owner: ");
                owner_contact=in.next();            
            }
        }            
        
        eng.readdetails();        
    }
    
    public void displaydetails()                   //Member function to display details.
    {
        System.out.println("Name: "+name);
        System.out.println("Company: "+company);
        System.out.println("Chassis Number: "+chassis_num);
        System.out.println("Price: "+price);
        System.out.println("Mileage: "+mileage);
        System.out.println("Colour: "+colour);
        
        if(insurance)
            System.out.println("Insurance included");
        
        else
            System.out.println("Insurance NOT included");
        
        eng.displaydetails();
        
        if(sold)
        {
            System.out.println("\nVehicle is Sold!");
            System.out.println("Name of Owner: "+owner_name);
            System.out.println("Contact Number of Owner: "+owner_contact);                
            System.out.println("Vehicle Bought on Date: "+datebought);            
        }
        
        else
            System.out.println("\nVehicle is NOT Sold!");
        
        
    }
    
  
}

class Engine                                //Class to display Containership Relationship to Class Vehicle.
{
    String name, type, model_num, maker;
    double price;
    float weight;
    
    public Engine()                         //Constructor to initialise values.
    {
        name=null;
        type=null;
        model_num=null;
        maker=null;
        price=0;
        weight=0;
    }
    
    
    public void readdetails()                   //Member function to read details.
    {
        Scanner in = new Scanner(System.in);
        System.out.println("\nEnter Name of the Engine: ");
        name=in.next();
        System.out.println("Enter the Type of the Engine: ");
        type=in.next();
        System.out.println("Enter the Model Number of the Engine: ");
        model_num=in.next();
        System.out.println("Enter the Maker of the Engine: ");
        maker = in.next();
        System.out.println("Enter the Price of the Engine: ");
        price=in.nextDouble();
        System.out.println("Enter the Weight of the Engine: ");
        weight = in.nextFloat();
    }
    
    
    public void displaydetails()                   //Member function to display details.
    {
        System.out.println("\nEngine Name: "+name);
        System.out.println("Engine Type: "+type);
        System.out.println("Model Number: "+model_num);
        System.out.println("Engine Maker: "+maker);
        System.out.println("Engine Price: "+price);
        System.out.println("Engine Weight: "+weight);
    }
        
}

class TwoWheeler extends Vehicle                //Child Class of Class Vehicle.
{
    boolean accessories;
    
    
    public TwoWheeler()                         //Constructor to intialise values.
    {
        super();                                //Calling Constructor of Parent Class Vehicle.
        accessories=false;
    }
    
    @Override
    public void readdetails()                  //Member function to read details.
    {
        Scanner in = new Scanner(System.in);
        
        super.readdetails();
        System.out.println("Accessories included? (1: Yes ; 0: No): ");
        if(in.nextInt()==1)
            accessories=true;
        else
            accessories = false;
        
        
        
    }
    
    @Override
    public void displaydetails()                   //Member function to display details.
    {
        System.out.println("\n----------------------------------------------------");
        System.out.println("\nVehicle type: Two Wheeler\n");
        super.displaydetails();
        if(accessories)
            System.out.println("\nAccessories included!");
        else
            System.out.println("\nAccessories NOT included!");
        
        
    }
    
}

abstract class FourWheeler extends Vehicle              //Abstract Child Class of Class Vehicle.
{
    String type, fuel;
    
    public FourWheeler()                                //Constructor to initialise values.
    {
        super();                    //Calling Constructor of Parent Class Vehicle.
        type=null;
        fuel=null;
    }
    
    @Override
    public void readdetails()                           //Member function to read details.
    {
        Scanner in = new Scanner(System.in);
        super.readdetails();                           //Calling Member function of Parent Class Vehicle to read details.
        System.out.println("Enter the Type of Four Wheeler: ");
        type = in.next();
        System.out.println("Enter the Fuel of the Four Wheeler: ");
        fuel = in.next();
    }
    
    @Override
    public void displaydetails()                           //Member function to display details.
    {
        System.out.println("\n----------------------------------------------------");
        System.out.println("\nVehicle type: Four Wheeler\n");
        super.displaydetails();                                 //Calling Member function of Parent Class Vehicle to display details.
        System.out.println("Type: "+type);
        System.out.println("Fuel: "+fuel);
        
    }
}

class PrivateCar extends FourWheeler                        //Child Class of Class FourWheeler.
{
    
    public PrivateCar()                             //Constructor to initialise values.
    {
        super();                                //Calling Constructor of Parent Class FourWheeler.
    }
    
    @Override
    public void readdetails()                           //Member function to read details.
    {
        Scanner in = new Scanner(System.in);
        super.readdetails();                           //Calling Member function of Parent Class FourWheeler to read details.
    }
    
    @Override
    public void displaydetails()                        //Member function to display details.
    {
        super.displaydetails();                           //Calling Member function of Parent Class FourWheeler to display details.
    }
        
}

class CommercialCar extends FourWheeler
{
    String comp_repname;
    
    public CommercialCar()                  //Constructor to initialise values.
    {
        super();                        //Calling Constructor of Parent Class FourWheeler.
        comp_repname=null;
    }
    
    @Override
    public void readdetails()                           //Member function to read details.
    {
        super.readdetails();                           //Calling Member function of Parent Class FourWheeler to read details.
        if(sold)
        {
            Scanner in = new Scanner(System.in);
            System.out.println("\nEnter the Name of the Representative of Owning Company: ");
            comp_repname=in.next();        
        }
        
    }
    
    @Override
    public void displaydetails()                           //Member function to display details.
    {
        super.displaydetails();                           //Calling Member function of Parent Class FourWheeler to display details.
        if(sold)
            System.out.println("Representative Name: "+comp_repname);
        else
            System.out.println("Car is NOT sold!");
    }
}



public class Lab7exp2 
{
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        byte ch=0, choice;
        Vehicle V=null;                                     //Reference of Vehicle Class.
        LinkedList head = new LinkedList();                 //Object of LinkedList Class.   
        
        
        while(true)
        {
            System.out.println("\n1. Add a Vehicle to the List.");
            System.out.println("2. View the List.");
            System.out.println("3. Delete a Vehicle from the List.");
            System.out.println("4. Edit the List.");
            System.out.println("0. Exit.");
            System.out.println("\nEnter your choice: ");
                      
            choice=in.nextByte();
            
            if(choice==1)
            {
                System.out.println("\n1. Two Wheeler.");
                System.out.println("2. Private Four Wheeler.");
                System.out.println("3. Commerical Four Wheeler.");
                System.out.println("Enter the type of vehicle: ");
                ch = in.nextByte();
            }
            
            switch(choice)
            {
                case 1:
                {
                    if(ch==1)
                    {
                        V = new TwoWheeler();                   //Object of TwoWheeler Class.
                        V.readdetails();                           //Calling Member function to read detials.
                        System.out.println("\n"+V.name+"\n");
                        head.insert(V);                           //Calling Member function to append data into the Linked List.
                    }
                    
                    else if(ch==2)
                    {
                        V = new PrivateCar();                   //Object of PrivateCar Class.
                        V.readdetails();                           //Calling Member function to read details.
                        head.insert(V);                           //Calling Member function to append data into the Linked List.                         
                    }
                    
                    else if(ch==3)
                    {
                        V = new CommercialCar();                    //Object of CommercialCar Class.
                        V.readdetails();                           //Calling Member function to read details.
                        head.insert(V);                           //Calling Member function to append data into the Linked List.
                    }
                        
                }
                
                break;
                
                case 2:
                {
                    if(head!=null)
                        head.display();                           //Calling Member function to display the Linked List.
                    
                    else
                        System.out.println("\nNo Vehicles in the List!");
                           
                }
                
                break;
                
                case 3:
                {
                    System.out.println("Enter the Chassis Number of Vehicle to delete: ");
                    head=head.delete(in.next());                                         //Calling Member function to delete a Vehicle from the Linked List.
                }
                
                break;
                
                case 4:
                {
                    System.out.println("Enter the Chassis Number of Vehicle to edit: ");
                    head.edit(in.next());                                                //Calling Member function to edit the data of a Vehicle in the Linked List.
                    
                }
                
                break;
                
                case 0: 
                    return;
                
            }   
        
        }
        
    }
    
}

class LinkedList
{
    private Vehicle data = null;                            //Reference of Vehicle Class.
    private LinkedList next = null;                         //Reference of LinkedList Class.    
    
    
    public void insert(Vehicle newData)                     //Reference of LinkedList Class.
    {
        if(this.data==null)
        {
            this.data=newData;
            System.out.println("\nAccessed!\n");
        }            
            
        
        else
        {
            LinkedList curr;
            for(curr=this;curr.next!=null;curr=curr.next);
            
            curr.next = new LinkedList();
            curr = curr.next;
            curr.data = newData;
            curr.next = null;
        }
    }
    
        
    public void display()                               //Member function to display details.
    {        
        if(this.data==null)
            System.out.println("\nNo Vehicles in the List!");
        
        else
        {
            LinkedList curr;
            for(curr=this;curr!=null;curr=curr.next)
            {
                curr.data.displaydetails();                               //Calling Member function to display details.
                System.out.println("\n");
            }
        }
    }    
    
    
    public LinkedList delete(String chassis)                               //Member function to delete a Vehicle.
    {
        LinkedList curr, prev;
        int cnt=0, i=0, size=0;

        if(this==null)
        {
            System.out.println("\nNo vehicles in the List!");
            return null;
        }
            
        
        else
        {
            for(size=0, curr=this;curr!=null;curr=curr.next, size++);
            
            for(curr=this;curr!=null;curr=curr.next)
            {
                if(!(chassis.equals(curr.data.chassis_num)))
                cnt++;

                if(chassis.equals(curr.data.chassis_num))
                {
                    break;                    
                }                    
            }

            if(curr.next==null && cnt ==0)
            {
                curr.next=new LinkedList();
                System.out.println("\nVehicle Successfully Removed!\n");
                return curr.next;
            }


            else if(cnt==0 && curr.next!=null)
            {
                System.out.println("\nVehicle Successfully Removed!\n");
                return curr.next;
            }


            else if(cnt>0)
            {
                for(curr = this, i=0; curr.next!=null && i<cnt; curr = curr.next, i++);
                for(prev = this, i=0 ; prev.next!=null && i<cnt-1; prev = prev.next, i++);

                prev.next=curr.next;

                System.out.println("\nVehicle Successfully Removed!\n");
                return this;
            }

            else if(cnt==size)
            {
                System.out.println("The Vehicle doesn't Exist in the List!");
                return this;            
            }
            
            else
                return null;
        }
        
            
    }
    
    
    public void edit(String chassis)                               //Member function to edit the data of the Vehicle.
    {
        LinkedList curr, prev;
        int cnt=0, i=0;
        
        if(this==null)
            System.out.println("No Vehicles in the List!");
        
        else
        {
            for(curr=this;curr.next!=null;curr=curr.next)
            {
                if(!(chassis.equals(curr.data.chassis_num)))
                    cnt++;

                if(chassis.equals(curr.data.chassis_num))
                {
                    break;                    
                }
            }

            if(cnt==0)
            {
                System.out.println("Enter the new details of Vehicle: \n");
                this.data.readdetails();                                        //Calling Member function to read details.
                System.out.println("\nVehicle Successfully Edited!\n");
            }

            else if(cnt>0)
            {
                for(curr = this; curr.next!=null && i<cnt; curr = curr.next, i++);
                System.out.println("Enter the new details of Vehicle: \n");
                curr.data.readdetails();                                               //Calling Member function to read details.
                System.out.println("\nVehicle Successfully Edited!\n");
            }

            else
            {
                System.out.println("The Vehicle doesn't Exist in the List!");            
            }  
        }
    }
}