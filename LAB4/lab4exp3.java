/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Implement the stack data structure.
Date: 11th February, 2016
*/

package lab4exp3;

import java.util.*;

class Stack
{
    //Data Members
    int [] arraystack;
    int size;
    int top;
    
    Stack(int i)                //Constructor to specify size.
    {
        size = i;
        arraystack = new int[size];
        top = -1;
    }
    
    
    public void pushelements()              //Member function to Push elements into stack.
    {
        Scanner in = new Scanner(System.in);
        int i;
        
        
        for(i=0;i<arraystack.length;i++)
        {
            System.out.println("\nEnter value no."+(i+1)+": ");
            arraystack[i] = in.nextInt();
            top++;
        }
    }
    
    
    public void displaynumbers()                //Member function to Display the stack. FOR REFERENCE.
    {
        int i;
        
        System.out.print("\n(");
        for(i=arraystack.length-1;i>=0;i--)
        {
            if(i>0)
                System.out.print(""+arraystack[i]+", ");
            
            else
                System.out.print(""+arraystack[i]+")\n");
                
        }
    }
    
    
    public void popelement()                //Member function to Pop the topmost element. Change the value of top element.
    {
        System.out.println("The topmost #"+top+" element is: " +arraystack[top]);
        
        arraystack[top]=0;
        top--;
        
        System.out.println("\nThe topmost element is removed!\n");
        
    }
    
    
    public void peepelement()                   //Member function to Display the topmost element.
    {
        System.out.println("\n\nThe topmost #"+top+" element is: " +arraystack[top]+"\n");
    }
    
    
    public void emptycheck()                         //Member function to Check whether stack is empty or not
    {
        if(top==-1)
            System.out.println("\nThe stack is empty!\n");
        
        else
            System.out.println("\nThe stack is not empty!\n");
    }
    
    
    public void fullcheck()                         //Member function to Check whether stack is full or not
    {
        if(top==arraystack.length-1)
            System.out.println("\nThe stack is full!\n");
        
        else
            System.out.println("\nThe stack is not full!\n");
    }
    
    
}

public class Lab4exp3 
{
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        int size, choice;
        System.out.println("Enter the size of the array(stack): ");             //Accepting size of stack.
        size = in.nextInt();
        
        
        Stack newnumbers = new Stack(size);                     //Creating object.
        
        
        
        while(true)
        {
            System.out.println("\n1. Push elements into stack.");
            System.out.println("2. Pop an element from the stack.");
            System.out.println("3. Peep the topmost element.");
            System.out.println("4. Check whether stack is empty or not.");
            System.out.println("5. Check whether stack is full or not.");
            System.out.println("0. Exit");
            System.out.println("\nEnter your choice: ");
            
            choice = in.nextInt();
            
            switch(choice)
            {
                case 1:
                {
                    newnumbers.pushelements();                      //Calling member function to push elements into stack.
                    
                    System.out.println("******\n");
                    System.out.println("FOR REFERENCE: ");
                    newnumbers.displaynumbers();                    //Calling member function to display stack elements. FOR REFERENCE.
                    System.out.println("\n******");
                }
                
                break;
                
                case 2:
                {
                    newnumbers.popelement();                        //Calling member function to pop the topmost element.
                    System.out.println("\nThe array is: ");
                    newnumbers.displaynumbers();                    //Calling member function to display stack elements. FOR REFERENCE.
                }
                
                break;
                
                case 3:
                {
                    newnumbers.peepelement();                        //Calling member function to peep into the topmost element.
                }
                
                break;
                
                case 4:
                {
                    newnumbers.emptycheck();                           //Calling member function to check whether stack is empty or not.
                }
                
                break;
                
                case 5:
                {
                    newnumbers.fullcheck();                        //Calling member function to check whether stack is full or not.
                }
                
                break;
                
                case 0: return;
                    
            }
        }
        
    }
    
}