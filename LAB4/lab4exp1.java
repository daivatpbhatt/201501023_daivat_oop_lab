/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Demonstrate the use of static members to count the number of objects created.
Date: 09th February, 2016
*/



package lab4exp1;
import java.util.Scanner;

class Book                              
{
    //Data members
    private int ISBN;
    private String title;
    private int edition;
    private String author;
    private String publisher;
    private int price;
    static int cnt=0;
    
    public void Book()              //Constructor to count the number of objects
    {
        cnt++;
    }
    
    public void readdetails()           //Read the details of each book.
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the ISBN Number: ");
        ISBN = in.nextInt();
        System.out.println("Enter the title of the book: ");
        title = in.next();
        System.out.println("Enter the edition of the book: ");
        edition = in.nextInt();
        System.out.println("Enter the author of the book: ");
        author = in.next();
        System.out.println("Enter the publisher of the book: ");
        publisher = in.next();
        System.out.println("Enter the price of the book: ");
        price = in.nextInt();
        
        
    }
    
    
    public void printdetails()          //Display details of each book.
    {
        System.out.println("Enter the ISBN Number: " +ISBN);
        System.out.println("Enter the title of the book: " +title);
        System.out.println("Enter the edition of the book: " +edition);
        System.out.println("Enter the author of the book: "+author);
        System.out.println("Enter the publisher of the book: "+publisher);
        System.out.println("Enter the price of the book: "+price);
        System.out.print("\n\n");
        
    }
    
    public static void getnumber()              //Display total books(objects)
    {
        System.out.println("Total books are: "+cnt);
    }
}


public class Lab4exp1 
{

    public static void main(String[] args)              //Main function
    {
        Scanner in = new Scanner(System.in);
        int choice;
        int i;
        
        Book[] bdetails = new Book[10];                 //Creating an array of books, with maximum size 10.
        
        System.out.println("1. Enter details.");
        System.out.println("2. Print details.");
        System.out.println("0. Exit.");
        System.out.println("Enter your choice: ");
        choice = in.nextInt();
        
        while(true)
        {
                
         
            switch(choice)
            {
                case 1:
                {
                    System.out.println("\n\nEnter details: \n");
                    int j=1;

                    for(i=0;i<10 && j!=0;i++)
                    {
                        bdetails[i] =  new Book();                          //Creating each new object as a value of the array of class Book
                        bdetails[i].readdetails();                          //Reading details.
                        System.out.println("\nEnter details again? (1 - Yes, 0 - No): ");
                        j = in.nextInt();
                    }

                }
                break;


                case 2:
                {
                    System.out.println("The details are: ");
                    for(i=0;i<10;i++)
                    {
                        if(bdetails[i] == null)
                        {
                            System.out.println("End of data!");
                            break;
                        }

                        else
                        {
                            bdetails[i].printdetails();
                        }
                    }
                    Book.getnumber();
                    
                }

                case 0:
                    return;
            }
        }
        
    
    }
    
}