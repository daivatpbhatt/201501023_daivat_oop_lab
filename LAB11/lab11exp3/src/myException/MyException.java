/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 2 of Assignment 6 [CIRCULAR QUEUE] by using GUI and Event Handling.
FILE: MyException Class.
Date: 10th April, 2016
*/

package myException;



@SuppressWarnings("serial")
public class MyException extends Exception			//Custom Exception!
{
	public MyException(String s)
	{
		super(s);
	}
}
