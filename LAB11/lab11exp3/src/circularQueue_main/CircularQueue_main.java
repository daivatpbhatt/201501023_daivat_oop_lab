/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 2 of Assignment 6 [CIRCULAR QUEUE] by using GUI and Event Handling.
FILE: Main Class.
Date: 10th April, 2016
*/


package circularQueue_main;

import javax.swing.JFrame;

import circularQueue_GUI.CircularQueue_GUI;


public class CircularQueue_main 
{
	public static void main(String[] args) 
	{
		JFrame frame = new CircularQueue_GUI(); 				//Creating frame.
	    frame.setTitle("Queue Circle");
	     
	    
	    frame.setLocationRelativeTo(null); 
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	    frame.setSize(1366, 768); 
	    frame.setVisible(true);  
	}
	
	
	
}
