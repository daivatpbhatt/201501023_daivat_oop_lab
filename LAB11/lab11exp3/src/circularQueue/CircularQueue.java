package circularQueue;
/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 2 of Assignment 6 [CIRCULAR QUEUE] by using GUI and Event Handling.
FILE: CircularQueue Class.
Date: 10th April, 2016
*/


import myException.MyException;

public class CircularQueue 
{
	private int size, front, end;
    private static int cnt;
    
    public int[] circularqueue;
    
    public CircularQueue(int s)							//Parameterised Constructor.
    {
    	if(s>0)
    	{
    		end=0;
    		front=0;
            size=s;
            circularqueue = new int[size];
            cnt=0;
    	}
    	
            
    }
    
    
    public void EnQueue(int I) throws MyException				//Member function to enqueue element.
    { 
    	if(this.checkempty())
    	{
    		end=0;
    		front=0;
   		
    	}
    	
    	if(!(this.checkforfull()))
    		throw new MyException("Queue is FULL!");
        
    	else
    	{
    		 System.out.println("end"+end);
    		 System.out.println("size"+size);
    		circularqueue[end]= I;
           
    		
            end= (end+1)%size;
            cnt++;
    	}
        
    }    
 
    
    public int DeQueue()throws MyException				//Member function to dequeue element.
    {
    	int temp=0;
        
    	if(this.checkempty())
    	{
    		end=0;
    		front=0;
    		throw new MyException("Queue is EMPTY!");
    	
    	}
    		
    	
        else
        {
            temp=circularqueue[front];
            circularqueue[front]=0;
            front=(front+1)%size;

                        
           System.out.println("end*"+end);
           System.out.println("front*"+front);
            cnt--;
        }
        
        return temp;
    }

    
    public boolean checkforfull()					//Member function to check whether queue is full or not.
    {
    
        if(cnt==size)
            return false;
        
        else
            return true;
    }
    
    
    public boolean checkempty()						//Member function to check whether queue is empty or not.
    {
        if(cnt==0)
            return true;
        
        else
            return false;
    }
    
    //Getter Functions.
    public int getFront() 			//Return front.
    {
		return front;
	}

	public int getEnd() 			//Return end.
	{
		return end;
	}

    
    public int getSize()			//Return size.
    {
    	return cnt;    	
    }
    
    

}
