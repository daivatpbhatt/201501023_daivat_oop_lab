/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 2 of Assignment 6 [CIRCULAR QUEUE] by using GUI and Event Handling.
FILE: CircularQueue_GUI, panel Class.
Date: 10th April, 2016
*/

package circularQueue_GUI;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


import circularQueue.CircularQueue;


@SuppressWarnings("serial")
public class CircularQueue_GUI extends JFrame
{	
	
	//Panels
	JPanel inputpanel;
	JPanel exceptionpan;
	
	//Panel circle, object of class panel, child class of JPanel.
	panel circle = new panel(); 
	
	
	//Controls for inputpanel Panel.
	JButton btnenq = new JButton("Enqueue"); 
	JButton btndeq= new JButton("Dequeue");
	JLabel lblnum = new JLabel("Enter a Number");
	JTextField txtnum = new JTextField(20); 
	
	//Controls for exceptionpan Panel.
	JLabel lblexc = new JLabel("");
	

	
	String input = JOptionPane.showInputDialog("Enter the size of queue"); 
	public  int  size =Integer.parseInt(input);
	
	CircularQueue circ = new CircularQueue(size);
	
	public CircularQueue_GUI() 
	{ 
		
		
		//Creating inputpanel Panel.
		inputpanel = new JPanel(); 
		
		//Setting exceptionpan Panel Display Details.
		exceptionpan = new JPanel();		
		exceptionpan.setSize(200, 300);
		exceptionpan.setLayout(new GridLayout(0, 1));
		
		//Creating object of CircQueueActionListener class.
		CircQueueActionListener listen = new CircQueueActionListener(this, circ);
		
				
		//Adding Action Listener for each button.
		btnenq.addActionListener(listen);
		btndeq.addActionListener(listen);
	      
		
		//Formatting lblexc Label.
		lblexc.setForeground(Color.RED);
		lblexc.setBackground(Color.BLACK);
		lblexc.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 34));
		
		//Formatting Controls of inputpanel Panel.	
		btnenq.setBackground(Color.white);
		btndeq.setBackground(Color.white);
		
		//Adding controls to inputpanel Panel.
		inputpanel.add(lblnum);
		inputpanel.add(txtnum);
		inputpanel.add(btnenq);
		inputpanel.add(btndeq);
		
		//Adding controls of exceptionpan Panel.
		exceptionpan.add(lblexc);
		
		//Adding panels and cirles to frame.
		this.add(circle, BorderLayout.CENTER); 
		this.add(inputpanel, BorderLayout.NORTH); 
		this.add(exceptionpan, BorderLayout.EAST);
	
	}


}



@SuppressWarnings("serial")
class panel extends JPanel
{

	CircularQueue obj;			//Reference of CircularQueue object.
	public void enqueue(CircularQueue obj)
	{
		this.obj = obj;
		repaint();
	}
	
	public void dequeue(CircularQueue obj)
	{
		this.obj = obj;
		repaint(); 
	}
	
	@Override 
	protected void paintComponent(Graphics graphic) 
	{
		super.paintComponent(graphic);    

		graphic.setColor(Color.YELLOW);
		graphic.fillOval(500,100,300,320);						//Painting the circle on frame.
		graphic.setColor(Color.WHITE);
		graphic.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
		int i;
		if(obj!=null)
		{
			for(i=obj.getFront(); i<obj.circularqueue.length;i++)
	        {   
				if(obj.circularqueue[i] != 0)
				{
					 if(i==obj.getFront())
					 {
						 graphic.setColor(Color.GREEN);				//For first element.
						 graphic.drawString(Integer.toString(obj.circularqueue[i]), (int)(650+120*Math.cos(2*i*Math.PI/obj.getSize())) , (int)(280-100*Math.sin(2*i*Math.PI/obj.getSize())));
					 }
						 
					 
	                 else if(i<obj.circularqueue.length-1)
	                 {
	                	 graphic.setColor(Color.DARK_GRAY);			//For middle elements.
	                	 graphic.drawString(Integer.toString(obj.circularqueue[i]), (int)(650+120*Math.cos(2*i*Math.PI/obj.getSize())) , (int)(280-100*Math.sin(2*i*Math.PI/obj.getSize())));
	                 }
	                	  
	                
	                 else if(i==obj.circularqueue.length-1)
	                 {
	                	 graphic.setColor(Color.RED);			//For last element.
	                	 graphic.drawString(Integer.toString(obj.circularqueue[i]), (int)(650+120*Math.cos(2*i*Math.PI/obj.getSize())) , (int)(280-100*Math.sin(2*i*Math.PI/obj.getSize())));
	                 }
	                	 
					 
					 
	            } 
	        }
		}
	}
}