/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 2 of Assignment 6 [CIRCULAR QUEUE] by using GUI and Event Handling.
FILE: CircQueueActionListener Class.
Date: 10th April, 2016
*/

package circularQueue_GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import circularQueue.CircularQueue;
import myException.MyException;

import circularQueue_GUI.CircularQueue_GUI;

public class CircQueueActionListener implements ActionListener 
{
	CircularQueue mycircqueue;
	CircularQueue_GUI mycirc;
	
	public CircQueueActionListener(CircularQueue_GUI circq, CircularQueue circ)
	{
		mycirc=circq;
		mycircqueue=circ;
	}
	
	public void actionPerformed(ActionEvent event) 
    {
		try
		{
			if(event.getActionCommand() == mycirc.btnenq.getText())			//Enqueue Button
			{  
				if(mycirc.txtnum.getText().isEmpty())						//Checking if entry field for element is empty.
					throw new MyException("Enter an ELEMENT!");
				
				else
				{
					int val;
					
					mycirc.exceptionpan.setVisible(false);
					val = Integer.parseInt(mycirc.txtnum.getText());
					mycircqueue.EnQueue(val);								//Calling Member function to enqueue.
					mycirc.circle.enqueue(mycircqueue);						//Editing the circle on frame.
					
					mycirc.txtnum.setText("");
				}
			}
			
			
			else if(event.getActionCommand()==mycirc.btndeq.getText()) 			//Dequeue Button.
			{
				int val;
				
				mycirc.lblexc.setText("");
				val= mycircqueue.DeQueue();								//Calling Member function to dequeue.
				mycirc.lblexc.setText("Element Removed: "+Integer.toString(val));
				mycirc.exceptionpan.setVisible(true);
	            mycirc.circle.dequeue(mycircqueue);						//Editing the circle on frame.
			}
		}
		
		catch(MyException err)
		{
			mycirc.exceptionpan.setVisible(true);
			mycirc.lblexc.setText(err.getMessage());
		}
		
    }
}
