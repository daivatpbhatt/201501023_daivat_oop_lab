/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 2 of Assignment 2 [EMPLOYEE LINKEDLIST] by using GUI and Event Handling.
FILE: Employee Class.
Date: 9th April, 2016
*/


package employee_linked;

import java.util.Scanner;

public class Employee 
{
	
	private String number;
	private String name, dateofbirth, contact;
	private String gender;
	private double baseSal, netSal=this.calNetSal(baseSal);
	
	//Getter functions
	public String getNo()
    {
        return number;
    }
	
	public String getName()
    {
        return name;
    }
	
	public String getDateofBirth()
    {
        return dateofbirth;
    }
	
	public String getContact()
    {
        return contact;
    }
	
	public String getGender()
    {
        return gender;
    }
	
	public double getBaseSal()
    {
        return baseSal;
    }
	
	public double getNetSal()
	{
		return netSal;
	}
	
	//Setter functions
	
	public void setNo(String i)
	{
		number=i;
	}
	
	public void setName(String sname)
	{
		name=sname;
	}
	
	public void setDateofbirth(String ndob)
	{
		dateofbirth=ndob;
	}
	
	public void setContact(String ncont)
	{
		contact=ncont;
	}
	
	public void setGender(String ngend)
	{
		gender=ngend;
	}
	
	public void setSalary(double fbase)
	{
		baseSal=fbase;
	}
	
	public void read()
	{
		Scanner in = new Scanner(System.in);
		System.out.println("Enter name: ");
		name=in.next();
		System.out.println("Enter number: ");
		number=in.next();
		System.out.println("Enter date of birth: ");
		dateofbirth = in.next();
		System.out.println("Enter Gender: ");
		gender=in.next();
		System.out.println("Enter contact number: ");
		contact=in.next();
		System.out.println("Enter basic salary: ");
		baseSal=in.nextDouble();
		
		in.close();
	}
	
	
	//print data
	public void print()
	{
		System.out.println("\nNumber: " + number);
		System.out.println("Name: " +name);
		System.out.println("DOB: " +dateofbirth);
		System.out.println("Gender: "+gender);
		System.out.println("Contact: "+contact);
		System.out.println("Base Sal: "+baseSal);
		System.out.println("Net Sal: "+calNetSal(baseSal)+"\n");
	}
	
	//Calculating Gross Salary
	
	public double calGrossSal(double BaseSal)
	{
		double DA=(0.6)*BaseSal;
		double HRA=0.12*BaseSal;
		double TA=12000;
		
		double GrossSal=BaseSal + DA + HRA + TA;
		return GrossSal;
		
	}
	
	public double calNetSal(double BS)
	{
		double GrossSal=calGrossSal(BS);
		double PF=0.1*BS;
		double PT=200;
		double TA=12000;
		double TDS=0.1*(GrossSal - TA - PF - PT);
		double TD=PF + PT + TDS;
		double NetSal=GrossSal-TD;
		netSal=NetSal;
		return NetSal;
	}
	
	public Employee(String number, String name, String dateofbirth, String gender, double baseSal, String contact_num)
	{
		this.number=number;
		this.name=name;
		this.dateofbirth=dateofbirth;
		this.gender=gender;
		this.baseSal=baseSal;
		this.contact=contact_num;
	}
	
	public Employee()
	{
		
	}
}