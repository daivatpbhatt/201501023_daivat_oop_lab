/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 2 of Assignment 2 [EMPLOYEE LINKEDLIST] by using GUI and Event Handling.
FILE: LinkedList Class.
Date: 9th April, 2016
*/


package employee_linked;

public class LinkedList 
{
	
	protected Employee data = null;
	private LinkedList next = null;
    static int cnt;

    public int returnsize()						//Member function to return total size of LinkedList.
    {
    	int size=0;
    	LinkedList curr;
    	
    	for(curr=this;curr!=null && curr.data!=null;size++, curr=curr.next);
    	return size;
    	
    }
    	

	public void add(Employee newDetails)						//Member function to add an employee to the LinkedList.
	{ 
		if(data == null)
			this.data = newDetails;
		else
		{
			LinkedList curr;
			for(curr = this; curr.next!=null; curr = curr.next);

			curr.next = new LinkedList();
			curr = curr.next;
			curr.data = newDetails;
			curr.next = null;
		}
	}
	
	public Employee get(int i)						//Member function to get details of specific employee.
	{
		LinkedList curr;
		for(curr = this,cnt=0; curr.next!=null && cnt<i; curr = curr.next, cnt++);
		
		return curr.data;
	}

	public void display()						//Member function to display the list.
	{
		if(this.data == null)
			System.out.println("\nList is Empty!");
		else
		{
			LinkedList curr;
			for(curr = this; curr!=null; curr = curr.next)
			{
				curr.data.print();
				System.out.println();
			}
		}
	}
	
	public int find(String findname)						//Member function to search for an employee by name.
	{
		LinkedList curr;
		for(curr = this, cnt=0; curr!=null; curr = curr.next,cnt++)
		{
			if(curr.data.getName().equals(findname))
			{
				return cnt;
			}
			
		}
		
		return -1;
		
	}
	
	public LinkedList remove(int i)
	{
		int size;
		
		
		LinkedList curr,prev,head=this;
		
		for(curr=this, size=0; curr.next!=null;size++);
		for(curr = this,cnt=0; curr.next!=null && cnt<i; curr = curr.next, cnt++);
		for(prev = this,cnt=0; prev.next!=null && cnt<i-1; prev = prev.next, cnt++);

		
		if(cnt==0)
		{
			if(curr.next!=null)
				return curr.next;
			
			else
			{
				curr.next=new LinkedList();
				return curr.next;
			}
		
		}
		
		else if(cnt<size)
		{
			prev.next=curr.next;
			return head;
		}
		
		else if(cnt==size)
		{
			return head;
		}
		
		else
			return head;
	}

}

