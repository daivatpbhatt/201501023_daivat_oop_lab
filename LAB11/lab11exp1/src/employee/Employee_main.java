/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 2 of Assignment 2 [EMPLOYEE LINKEDLIST] by using GUI and Event Handling.
FILE: Main Class.
Date: 9th April, 2016
*/

package employee;

import employeeGUI.EmployeeGUI;
import javax.swing.JFrame;

public class Employee_main 
{

	public static void main(String[] args)
	{
		EmployeeGUI emp = new EmployeeGUI();			//Creating frame.
		
		
		emp.setVisible(true);
		emp.setLocationRelativeTo(null);
		emp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		

	}

}
