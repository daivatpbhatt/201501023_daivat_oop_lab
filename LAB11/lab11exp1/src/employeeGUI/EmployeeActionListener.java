/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 2 of Assignment 2 [EMPLOYEE LINKEDLIST] by using GUI and Event Handling.
FILE: EmployeeActionListener Class..
Date: 9th April, 2016
*/

package employeeGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import employee_linked.Employee;
import employee_linked.LinkedList;



public class EmployeeActionListener implements ActionListener
{
	private EmployeeGUI myemployee;
	private Employee emp=new Employee();;
	LinkedList head=new LinkedList();;
	
	
	public EmployeeActionListener(EmployeeGUI empl)
	{
		myemployee=empl;					//Referring to the frame we created.
	}
	
	public void actionPerformed(ActionEvent event)
	{
				
		if(event.getActionCommand()==myemployee.btnAdd.getText())
		{
			
			if(myemployee.rbtno.isSelected())
				emp = new Employee(myemployee.txtNum.getText(), myemployee.txtName.getText(), myemployee.txtdob.getText(), myemployee.rbtno.getText() , Double.parseDouble(myemployee.txtSal.getText()), myemployee.txtcontact.getText());					//Calling constructor of Employee Class and passing values.
				
			
			else if(myemployee.rbtnf.isSelected())
				emp = new Employee(myemployee.txtNum.getText(), myemployee.txtName.getText(), myemployee.txtdob.getText(), myemployee.rbtnf.getText() , Double.parseDouble(myemployee.txtSal.getText()), myemployee.txtcontact.getText());					//Calling constructor of Employee Class and passing values.
			
			else if(myemployee.rbtnm.isSelected())
				emp = new Employee(myemployee.txtNum.getText(), myemployee.txtName.getText(), myemployee.txtdob.getText(), myemployee.rbtnm.getText() , Double.parseDouble(myemployee.txtSal.getText()), myemployee.txtcontact.getText());					//Calling constructor of Employee Class and passing values.
			
			else
				emp = new Employee(myemployee.txtNum.getText(), myemployee.txtName.getText(), myemployee.txtdob.getText(), "Invalid Gender!" , Double.parseDouble(myemployee.txtSal.getText()), myemployee.txtcontact.getText());					//Calling constructor of Employee Class and passing values.
			
			head.add(emp);
			
			
			
			myemployee.txtcontact.setText("");
			myemployee.txtdob.setText("");
			myemployee.txtName.setText("");
			myemployee.txtNum.setText("");
			myemployee.txtSal.setText("");
			myemployee.txtfindn.setText("");
			
		}
		
		else if(event.getActionCommand()==myemployee.btndisp.getText())
		{			
			head.display();							//Calling member function to display the list.
			System.out.println("\nTotal nodes: " +head.returnsize());
			
			myemployee.txtcontact.setText("");
			myemployee.txtdob.setText("");
			myemployee.txtName.setText("");
			myemployee.txtNum.setText("");
			myemployee.txtSal.setText("");
			myemployee.txtfindn.setText("");
		}
			
		
		else if(event.getActionCommand()==myemployee.btnDel.getText())
		{
			int index;
			
			index=head.find(myemployee.txtfindn.getText());							//Calling member function to find the employee.
			
			if(index!=-1)
			{
				head=head.remove(index);							//Calling member function to remove the corresponding employee.
			}
			
			else
				System.out.println("\nINVALID NAME!");
			
			myemployee.txtfindn.setText("");
		}
			
		
		else if(event.getActionCommand()==myemployee.btnSearch.getText())
		{
			int index;
			
			index=head.find(myemployee.txtfindn.getText());							//Calling member function to find the employee.
			
			if(index!=-1)
				head.get(index).print();							//Calling member function to display details of the employee.
			
			else
				System.out.println("INVALID NAME!");
			
			myemployee.txtfindn.setText("");
			
		}
		
		else if(event.getActionCommand()==myemployee.btnMod.getText())
		{
			int index;
			
			index=head.find(myemployee.txtfindn.getText());							//Calling member function to find the employee
			
			if(index!=-1)
			{
				Scanner in = new Scanner(System.in);
				
				System.out.println("Enter name: ");
				head.get(index).setName(in.next());							//Calling member function to set the name of the employee.
				
				System.out.println("Enter number: ");
				head.get(index).setNo(in.next());							//Calling member function to set the employee Number.
				
				System.out.println("Enter date of birth: ");
				head.get(index).setDateofbirth(in.next());
				
				System.out.println("Enter Gender: ");
				head.get(index).setGender(in.next());							//Calling member function to set the employee gender.
				
				System.out.println("Enter contact number: ");
				head.get(index).setContact(in.next());							//Calling member function to set the contact number of the employee.
				
				System.out.println("Enter basic salary: ");
				head.get(index).setSalary(in.nextDouble());							//Calling member function to set the salary of the employee.
				
				in.close();
				
				head.display();									//Calling member function to display the list.
				
			}
				
			
			else
				System.out.println("INVALID NAME!");
			
			myemployee.txtfindn.setText("");
			
		}
		
		else if(event.getActionCommand()==myemployee.btnE.getText())
			myemployee.dispose();
	}

}
