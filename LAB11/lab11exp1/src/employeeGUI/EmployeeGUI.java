/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 2 of Assignment 2 [EMPLOYEE LINKEDLIST] by using GUI and Event Handling.
FILE: EmployeeGUI Class.
Date: 9th April, 2016
*/

package employeeGUI;

import java.awt.*;
import javax.swing.*;


@SuppressWarnings("serial")
public class EmployeeGUI extends JFrame
{
	//Panels
	JPanel inputpanel = new JPanel();
	JPanel gendpan = new JPanel();
	JPanel btnpan = new JPanel();
	JPanel indexpan = new JPanel();
	
	//Controls of Input Panel
	JLabel lblName = new JLabel("Employee Name");
	JTextField txtName = new JTextField(10);	
	JLabel lblNum = new JLabel("Employee Number");
	JTextField txtNum = new JTextField(5);	
	JLabel lblGend = new JLabel("Gender");	
	JLabel lbldob = new JLabel("Date of Birth");
	JTextField txtdob = new JTextField(10);	
	JLabel lblcontact = new JLabel("Contact Number");
	JTextField txtcontact = new JTextField(10);	
	JLabel lblSal = new JLabel("Basic Salary");
	JTextField txtSal = new JTextField(10);	
	
	//Controls for gendpan Panel.
	ButtonGroup gend = new ButtonGroup();
	JRadioButton rbtnm = new JRadioButton("Male");
	JRadioButton rbtnf = new JRadioButton("Female");
	JRadioButton rbtno = new JRadioButton("Other");	
	

	//Controls for btnpan Panel.
	JButton btnAdd = new JButton("Add");
	JButton btnE = new JButton("Exit");
	JButton btnSearch = new JButton("Search");
	JButton btnMod = new JButton("Modify");
	JButton btnDel = new JButton("Delete");
	JButton btndisp = new JButton("Display");
	
	//Controls for indexpan Panel.
	JLabel lblfindn = new JLabel("Enter Name");
	JTextField txtfindn = new JTextField(9);
	
	
	
	public EmployeeGUI()
	{
		//Setting Frame Display Details.
		this.setTitle("EMPLOYEE DETAILS");
		this.setSize(600, 600);
		this.setLayout(new GridLayout(0, 1));
		this.setBackground(Color.white);
		
		//Creating object of EmployeeActionListener class.
		EmployeeActionListener listen = new EmployeeActionListener(this);
		
		//Setting inputpanel Display Details.
		inputpanel.setLayout(new GridLayout(0, 2));
		inputpanel.setSize(500, 500);
		inputpanel.setBackground(Color.white);	
		
		//Setting gendpan Display Details.
		gendpan.setLayout(new GridLayout(0, 4));
		gendpan.setSize(200, 250);
		
		//Setting btnpan Display Details.
		btnpan.setLayout(new FlowLayout(FlowLayout.CENTER));
		btnpan.setSize(10, 10);
		
		//Setting indexpan Display Details.
		indexpan.setLayout(new GridLayout(0, 2));
		indexpan.setSize(20, 20);
		
		
		//Adding Action Listener for each button.
		btnAdd.addActionListener(listen);
		btndisp.addActionListener(listen);
		btnDel.addActionListener(listen);
		btnSearch.addActionListener(listen);
		btnMod.addActionListener(listen);
		btnE.addActionListener(listen);
		

		//Adding radio buttons to ButtonGroup.
		gend.add(rbtno);
		gend.add(rbtnm);
		gend.add(rbtnf);
		
		//Adding controls to inputpanel Panel
		inputpanel.add(lblName);
		inputpanel.add(txtName);
		inputpanel.add(lblNum);
		inputpanel.add(txtNum);
		inputpanel.add(new JLabel());
		inputpanel.add(new JLabel());
		inputpanel.add(lbldob);
		inputpanel.add(txtdob);
		inputpanel.add(lblcontact);
		inputpanel.add(txtcontact);
		
		inputpanel.add(new JLabel());
		inputpanel.add(new JLabel());
		
		inputpanel.add(lblSal);
		inputpanel.add(txtSal);

		inputpanel.add(new JLabel());
		inputpanel.add(new JLabel());
		
		//Adding controls to gendpan Panel.
		gendpan.add(lblGend);
		gendpan.add(rbtno);
		gendpan.add(rbtnm);
		gendpan.add(rbtnf);
		
		//Adding controls to btnpan Panel.
		btnpan.add(btnAdd);
		btnpan.add(btndisp);
		btnpan.add(btnE);
		btnpan.add(new JLabel());
		btnpan.add(new JLabel());
		
		//Adding controls to indexpan Panel.
		indexpan.add(new JLabel());
		indexpan.add(new JLabel());
		indexpan.add(lblfindn);
		indexpan.add(txtfindn);
		indexpan.add(btnDel);
		indexpan.add(new JLabel());
		indexpan.add(btnMod);
		indexpan.add(btnSearch);
		
		
		//Adding Panels to Frame.
		this.add(inputpanel, BorderLayout.NORTH);
		this.add(gendpan, BorderLayout.NORTH);
		this.add(btnpan);
		this.add(indexpan, BorderLayout.CENTER);
		

	}
	
	
}
