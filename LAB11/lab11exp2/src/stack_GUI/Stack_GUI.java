/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 3 of Assignment 4 [STACK] by using GUI and Event Handling.
FILE: StackGUI Class.
Date: 9th April, 2016
*/

package stack_GUI;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")

public class Stack_GUI extends JFrame 
{
	//Panels.
	JPanel inputpanel;
	JPanel inputsize;
	JPanel outputpanel;
	JPanel exceptionpan;
	
	//Controls for inputpanel Panel
	JButton btnpush = new JButton("Push");
	JButton btnpeep = new JButton("Peep");
	JButton btnpop = new JButton("Pop");
	JButton btndisp = new JButton("Display");
	
	JLabel lblval = new JLabel("Enter Element");
	JTextField txtval = new JTextField(10);
	
	//Controls for inputsize Panel
	JButton btnsize = new JButton("Enter Size!");
	JTextField txtsize = new JTextField(10);
	
	//Controls for outputpanel Panel
	JLabel lblstack = new JLabel("");
	JLabel lbldisp = new JLabel("");
	
	//Controls for exceptionpan Panel
	JLabel lblexcept = new JLabel("", SwingConstants.CENTER);
	
	
	
	public Stack_GUI()
	{
		//Setting Frame Display Details.
		this.setSize(550, 300);
		this.setLayout(new BorderLayout(10, 10));
		this.setTitle("STACKS!");
		
		//Creating object of StackActionListener class.
		StackActionListener listen = new StackActionListener(this);		
		
		//Setting inputpanel Display Details.
		inputpanel = new JPanel();
		inputpanel.setLayout(new GridLayout(0, 3));
		inputpanel.setSize(50, 50);
		inputpanel.setBackground(Color.white);
		
		
		//Setting inputsize Display Details.
		inputsize = new JPanel();
		inputsize.setLayout(new GridLayout(0, 1));
		inputsize.setSize(200, 100);
		inputsize.setName("SIZE!");
		inputsize.setBackground(Color.white);
			
		
		//Setting outputpanel Display Details.
		outputpanel = new JPanel();
		outputpanel.setLayout(new GridLayout(0, 1));
		outputpanel.setSize(50, 50);
		
		
		//Setting exceptionpan Display Details.
		exceptionpan = new JPanel();
		exceptionpan.setLayout(new BorderLayout());
		exceptionpan.setSize(50, 300);
		exceptionpan.setBackground(Color.BLACK);		
		
		
		//Adding Action Listener for each button.
		btnsize.addActionListener(listen);
		btnpush.addActionListener(listen);
		btnpeep.addActionListener(listen);
		btnpop.addActionListener(listen);
		btndisp.addActionListener(listen);
		
		//Formatting Font of Controls of outputpanel Panel.
		lblstack.setFont(new Font(Font.MONOSPACED ,Font.BOLD, 15));
		lbldisp.setFont(new Font(Font.SERIF, Font.BOLD, 18));
		lbldisp.setBackground(Color.darkGray);
		lbldisp.setForeground(Color.BLUE);
		
		
		//Formatting Font of Controls of exceptionpan Panel.
		lblexcept.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
		lblexcept.setForeground(Color.RED);
		
				
		//Adding controls to inputpanel Panel.
		inputpanel.add(lblval);
		inputpanel.add(txtval);
		inputpanel.add(btnpush);
		inputpanel.add(btndisp);
		inputpanel.add(btnpop);
		inputpanel.add(btnpeep);
				
		
		//Adding controls to inputsize Panel.
		inputsize.add(txtsize);
		inputsize.add(btnsize);

		
		//Adding controls to outputpanel Panel.
		outputpanel.add(lblstack);
		outputpanel.add(lbldisp);
		
		
		//Adding controls to exceptionpan Panel.
		exceptionpan.add(lblexcept);		
		
		
		//Adding Panels to Frame.
		this.add(inputsize, BorderLayout.NORTH);
		this.add(exceptionpan, BorderLayout.EAST);
		this.add(outputpanel, BorderLayout.CENTER);		
		this.add(inputpanel, BorderLayout.SOUTH);
		
		
	}
}



