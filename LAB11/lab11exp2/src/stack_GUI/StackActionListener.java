/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 3 of Assignment 4 [STACK] by using GUI and Event Handling.
FILE: StackActionListener Class.
Date: 9th April, 2016
*/

package stack_GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import stack.Stack;
import myException.MyException;

public class StackActionListener implements ActionListener
{
	Stack  stack;
	Stack_GUI stack_gui;

	public StackActionListener(Stack_GUI s)
	{
		stack_gui=s;				//Refering to frame we created.
	}
	
	public void actionPerformed(ActionEvent event)
	{
		try
		{
			if(event.getActionCommand()==stack_gui.btnsize.getText())	//Add Button
			{
				int size;
				
				if(stack_gui.txtsize.getText().isEmpty())		//Check for empty text field for size.
					throw new MyException("Enter SIZE first!");
					
				else
				{
					stack_gui.exceptionpan.setVisible(false);
					size = Integer.parseInt(stack_gui.txtsize.getText());
					stack = new Stack(size);				//Creating object of Stack class.

				}
				
						
			}
			
			else if(event.getActionCommand()==stack_gui.btnpush.getText())		//Push Button
			{	
				if(stack_gui.txtsize.getText().isEmpty())		//Check for empty text field for size.
					throw new MyException("Enter SIZE first!");
				
				else if(stack_gui.txtval.getText().isEmpty())	//Check for empty text field for element entered.
				{
					throw new MyException("Enter an ELEMENT!");
				}
				
				else
				{	
					stack_gui.exceptionpan.setVisible(false);
					stack_gui.outputpanel.setVisible(false);
					int i = Integer.parseInt(stack_gui.txtval.getText());
					
					stack.pushelements(i);						//Push element into stack.
					
					stack_gui.txtval.setText("");
			
				}
			}	
			
			else if(event.getActionCommand()==stack_gui.btnpop.getText())		//Pop Button.
			{
				if(stack_gui.txtsize.getText().isEmpty())		//Check for empty text field for size.
					throw new MyException("Enter SIZE first!");
				
				else
				{	
					stack_gui.lbldisp.setText("Element Poped: " +String.valueOf(stack.peepelement()));
					stack_gui.lblstack.setVisible(false);
					stack_gui.exceptionpan.setVisible(false);
					stack.popelement();						//Pop Element from Stack.
			
				}
			}	
			
			else if(event.getActionCommand()==stack_gui.btnpeep.getText())		//Peep Button.
			{
				if(stack_gui.txtsize.getText().isEmpty())		//Check for empty text field for size.
					throw new MyException("Enter SIZE first!");
				
				else
				{
					int i;
					stack_gui.exceptionpan.setVisible(false);
					stack_gui.outputpanel.setVisible(true);
					i = stack.peepelement();				//Store topmost Element.
					stack_gui.lblstack.setText("Topmost element is:");
					stack_gui.lbldisp.setText(String.valueOf(i));		//Display Topmost element.
				}
			}
				
			
			else if(event.getActionCommand()==stack_gui.btndisp.getText())	//Display Button
			{	
				if(stack_gui.txtsize.getText().isEmpty())		//Check for empty text field for size.
					throw new MyException("Enter SIZE first!");				
				
				else if(stack.emptycheck())
					throw new MyException("Stack is EMPTY!");
				
				
				else
				{
					stack_gui.lbldisp.setText("");
					stack_gui.exceptionpan.setVisible(false);
					stack_gui.lblstack.setText("The Stack is: ");
					stack_gui.lbldisp.setText(stack.toString());			//Display in String Form.
					stack_gui.outputpanel.setVisible(true);
					stack_gui.lbldisp.setVisible(true);
					stack_gui.lblstack.setVisible(true);
				
				}
				
			}
				
			
			
		}
		
		catch(MyException err)				//Catching Exception.
		{
			stack_gui.outputpanel.setVisible(false);
			stack_gui.exceptionpan.setVisible(true);
			stack_gui.lblexcept.setText(err.getMessage());
			stack_gui.exceptionpan.setVisible(true);
		}
		
		
	}
	
}
