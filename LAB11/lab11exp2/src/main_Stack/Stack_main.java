/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 3 of Assignment 4 [STACK] by using GUI and Event Handling.
FILE: Main Class.
Date: 9th April, 2016
*/


package main_Stack;

import javax.swing.JFrame;

import stack_GUI.Stack_GUI;

public class Stack_main 
{

	public static void main(String[] args) 
	{
		Stack_GUI stackgui = new Stack_GUI();				//Creating Frame.
		
		stackgui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		stackgui.setVisible(true);
		stackgui.setLocationRelativeTo(null);
		

	}

}
