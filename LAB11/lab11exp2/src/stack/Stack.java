/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 3 of Assignment 4 [STACK] by using GUI and Event Handling.
FILE: Stack Class.
Date: 9th April, 2016
*/

package stack;

import myException.MyException;

public class Stack
{
    //Data Members
    int [] arraystack;
    int size;
    int top;
    
    public Stack(int i) throws MyException               //Constructor to specify size.
    {
    	if(i>0)
    	{
    		size = i;
            arraystack = new int[size];
            top = -1;
    	}
    	
    	else
    		throw new MyException("Enter SIZE greater than ZERO!");
    }
    
    
    public void pushelements(int val) throws MyException              //Member function to Push elements into stack.
    {
        if(fullcheck())
        {
        	arraystack[++top]=val;
        }
        
        else
        	throw new MyException("Stack is FULL!");
        	
        	
    }
    
    
    public void displaynumbers()                //Member function to Display the stack.
    {
        int i;
        
        System.out.print("\n(");
        for(i=arraystack.length-1;i>=0;i--)
        {
            if(i>0)
                System.out.print(""+arraystack[i]+", ");
            
            else
                System.out.print(""+arraystack[i]+")\n");
                
        }
    }
    
    @Override
    public String toString()			//Member Function to convert stack data into String.
    {
    	if(emptycheck())
    	{
    		return "EMPTY STACK!";
    	}
    		
    	
    	else
    	{
    		String mystack = new String();
        	int i;
        	
        	        	
        	mystack = "(";
        	for(i = 0; i<=top; i++)
        	{
        		if(i<top)
        			mystack += arraystack[i]+ ", ";
                
                else
                	mystack += arraystack[i]+")";
        	}
    			
    		
    		return mystack;
    	}
    	
    }
    
    
    public void popelement() throws MyException                //Member function to Pop the topmost element. Change the value of top element.
    {
               
        if(emptycheck())
        {
        	throw new MyException("Stack is EMPTY!");
        }
        
        else
        {
        	arraystack[top]=0;
            top--;
        }
        	
    }
    
    
    public int peepelement() throws MyException                  //Member function to Display the topmost element.
    {
    	if(top!=-1)
    		return arraystack[top];
    	
    	else
    		throw new MyException("Stack is EMPTY!");
        
    }
    
    
    public boolean emptycheck()                         //Member function to Check whether stack is empty or not
    {
        if(top==-1)
            return true;
        
        else
            return false;
    }
    
    
    public boolean fullcheck()                         //Member function to Check whether stack is full or not
    {
        if(top==arraystack.length-1)
            return false;
        
        else
            return true;
    }
    
    
}