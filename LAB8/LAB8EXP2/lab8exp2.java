/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Create a Vehicle categorization system using Linked List. Implement Sellable and Rentable Interfaces.
Date: 13th March, 2016

FILE: Main Function containing Class.
*/

package lab8exp2;

import java.util.*;

public class Lab8exp2 
{
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        byte ch=0, choice;
        Vehicle V=null;                                     //Reference of Vehicle Class.
        LinkedList head = new LinkedList();                 //Object of LinkedList Class.   
        
        
        while(true)
        {
            System.out.println("\n1. Add a Vehicle to the List.");
            System.out.println("2. View the List.");
            System.out.println("3. Delete a Vehicle from the List.");
            System.out.println("4. Edit the List.");
            System.out.println("5. Sell or Rent a Vehicle.");
            System.out.println("0. Exit.");
            System.out.println("\nEnter your choice: ");
                      
            choice=in.nextByte();
            
            if(choice==1)
            {
                System.out.println("\n1. Two Wheeler.");
                System.out.println("2. Private Four Wheeler.");
                System.out.println("3. Commerical Four Wheeler.");
                System.out.println("Enter the type of vehicle: ");
                ch = in.nextByte();
            }
            
            switch(choice)
            {
                case 1:
                {
                    if(ch==1)
                    {
                        V = new TwoWheeler();                   //Object of TwoWheeler Class.
                        V.readdetails();                           //Calling Member function to read detials.
                        head.insert(V);                           //Calling Member function to append data into the Linked List.
                    }
                    
                    else if(ch==2)
                    {
                        V = new PrivateCar();                   //Object of PrivateCar Class.
                        V.readdetails();                           //Calling Member function to read details.
                        head.insert(V);                           //Calling Member function to append data into the Linked List.                         
                    }
                    
                    else if(ch==3)
                    {
                        V = new CommercialCar();                    //Object of CommercialCar Class.
                        V.readdetails();                           //Calling Member function to read details.
                        head.insert(V);                           //Calling Member function to append data into the Linked List.
                    }
                        
                }
                
                break;
                
                case 2:
                {
                    if(head!=null)
                        head.display();                           //Calling Member function to display the Linked List.
                    
                    else
                        System.out.println("\nNo Vehicles in the List!");                          
                }
                
                break;
                
                case 3:
                {
                    System.out.println("Enter the Chassis Number of Vehicle to delete: ");
                    head=head.delete(in.next());                                         //Calling Member function to delete a Vehicle from the Linked List.
                }
                
                break;
                
                case 4:
                {
                    System.out.println("\nEnter the Chassis Number of Vehicle to edit: ");
                    head.edit(in.next());                                                //Calling Member function to edit the data of a Vehicle in the Linked List.
                    
                }
                
                break;
                
                case 5:
                {
                    System.out.println("\nEnter the Chassis Number of Vehicle to edit: ");
                    head.transact(in.next());                                           //Calling Member function to edit rent or sell details.
                }
                
                break;
                
                case 0: 
                    return;
                    
                default: 
                    System.out.println("\nEnter valid choice!");
                
            }   
        
        }
        
    }
    
}