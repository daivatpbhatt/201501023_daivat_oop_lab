/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Create a Vehicle categorization system using Linked List. Vehicles are either two wheelers or four wheelers. Implement Sellable and Rentable Interfaces.
Date: 13th March, 2016

FILE: File defining Interfaces.
*/

package lab8exp2;

interface Sellable                          //Interface for Selling.
{
    public void sell(double value);             //Member function to sell.
}

interface Rentable                          //Interface for Renting.
{
    public void rent();                     //Member function to rent.
}
