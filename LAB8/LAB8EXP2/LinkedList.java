/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Create a Vehicle categorization system using Linked List. Vehicles are either two wheelers or four wheelers. Implement Sellable and Rentable Interfaces.
Date: 13th March, 2016

FILE: Class to edit and display the LinkedList.
*/

package lab8exp2;


import java.util.Scanner;

class Node                              //Node Class
{
    Vehicle data;                   //Reference of Vehicle Class.
    Node next;                          //Reference of Node Class.
}


class LinkedList
{
    private Node head = new Node();                            //Reference of Node Class.
    
    public void insert(Vehicle newData)                     //Reference of LinkedList Class.
    {
        if(this.head.data==null)
        {
            this.head.data=newData;
        }           
            
        
        else
        {
            Node curr;
            for(curr=this.head;curr.next!=null;curr=curr.next);
            
            curr.next = new Node();
            curr = curr.next;
            curr.data = newData;
            curr.next = null;
        }
    }
    
        
    public void display()                               //Member function to display details.
    {        
        if(this.head.data==null)
            System.out.println("\nNo Vehicles in the List!");
        
        else
        {
            Node curr;
            for(curr=this.head;curr!=null;curr=curr.next)
            {
                curr.data.displaydetails();                               //Calling Member function to display details.
                System.out.println("\n");
            }
        }
    }    
    
    
    public void delete(String chassis)                               //Member function to delete a Vehicle.
    {
        Node curr, prev;
        int cnt=0, i=0, size=0;
        
        if(this.head==null)
        {
            System.out.println("\nNo Vehicles in the List!");           
        }
        
            
        
        else
        {
            for(size=0, curr=this.head ;curr!=null; curr= curr.next, size++);
            
            for(curr=this.head, cnt=0;curr!=null;curr=curr.next)
            {
                if(!(chassis.equals(curr.data.chassis_num)))
                    cnt++;

                if(chassis.equals(curr.data.chassis_num))
                {
                    break;                    
                }                    
            }
            
            if(cnt==size)
                    System.out.println("\nVehicle doesn't exist in the List!\n");
            
            else
            {               
                
                if(this.head.next==null && cnt==0)
                {
                    System.out.println("\nVehicle Successfully Removed!\n");
                    this.head.data=null;

                }

                else if(cnt==0)
                {
                    this.head.data=null;
                    this.head=this.head.next;
                }


                else if(cnt>0)
                {
                    for(curr = this.head, i=0; curr.next!=null && i<cnt; curr = curr.next, i++);
                    for(prev = this.head, i=0 ; prev.next!=null && i<cnt-1; prev = prev.next, i++);

                    prev.next=curr.next;

                    System.out.println("\nVehicle Successfully Removed!\n");
                }

            }
            
            
        }
        
    }
        
        
    public void edit(String chassis)                               //Member function to edit the data of the Vehicle.
    {
        Node curr, prev;
        int cnt=0, i=0;
        
        if(this.head==null)
            System.out.println("No Vehicles in the List!");
        
        else
        {
            for(curr=this.head;curr.next!=null;curr=curr.next)
            {
                if(!(chassis.equals(curr.data.chassis_num)))
                    cnt++;

                if(chassis.equals(curr.data.chassis_num))
                {
                    break;                    
                }
            }

            if(cnt==0)
            {
                System.out.println("Enter the new details of Vehicle: \n");
                this.head.data.readdetails();                                        //Calling Member function to read details.
                System.out.println("\nVehicle Successfully Edited!\n");
            }

            else if(cnt>0)
            {
                for(curr = this.head; curr.next!=null && i<cnt; curr = curr.next, i++);
                System.out.println("Enter the new details of Vehicle: \n");
                curr.data.readdetails();                                               //Calling Member function to read details.
                System.out.println("\nVehicle Successfully Edited!\n");
            }

            else
            {
                System.out.println("The Vehicle doesn't Exist in the List!");            
            }  
        }
    }
    
    
    public void transact(String chassis)
    {
        Scanner in = new Scanner(System.in);
        byte choice;
        int cnt=0, i=0;
        
        System.out.print("Sell : 1 ; Rent : 2 ? : ");
        choice=in.nextByte();
        
        if(choice == 1)
        {
            Node curr;
            
            for(curr=this.head;curr!=null;curr=curr.next)
            {
                if(!(chassis.equals(curr.data.chassis_num)))
                    cnt++;

                if(chassis.equals(curr.data.chassis_num))
                {
                    break;                    
                }                    
            }
            
            if(cnt==0)
            {
                if(this.head.data.rented==true)
                    System.out.println("\nVehicle is Rented!");
                
                else if(this.head.data.sold==true)
                    System.out.println("\nVehicle is Sold!");
                
                else
                {                        
                    System.out.println("\nEnter the details of the transaction: ");
                    this.head.data.sell(this.head.data.price);
                    this.head.data.sold=true;
                }
                
            }            
            
            else if(cnt>0)
            {
                for(curr=this.head, i=0;curr!=null && i<cnt;curr=curr.next, i++);
                {
                    System.out.println("\n"+i+"\n");
                    if(curr.data.rented==true)
                        System.out.println("\nVehicle is Rented!");
                    
                    else if(curr.data.sold==true)
                        System.out.println("\nVehicle is Sold!");
                
                    else
                    {                        
                        System.out.println("\nEnter the details of the transaction: ");
                        curr.data.sell(curr.data.price);
                        curr.data.sold=true;
                    }
                }
            }            
            
            else
                System.out.println("The Vehicle doesn't exist in the list.");
        }
        
        else if(choice==2)
        {
            Node curr;
            
            for(curr=this.head;curr!=null;curr=curr.next)
            {
                if(!(chassis.equals(curr.data.chassis_num)))
                    cnt++;

                if(chassis.equals(curr.data.chassis_num))
                {
                    break;                    
                }                    
            }
            
            if(cnt==0)
            {   
                if(this.head.data instanceof TwoWheeler)
                {
                    if(this.head.data.sold==true)
                        System.out.println("Vehicle is already Sold!");
                    
                    else
                    {                            
                        System.out.println("\nEnter the details of the transaction: ");
                        ((TwoWheeler)this.head.data).rent();
                        this.head.data.rented=true;
                        this.head.data.sold=false;
                    }
                }                    
                
                else if(this.head.data instanceof CommercialCar)
                {
                    if(this.head.data.sold==true)
                        System.out.println("Vehicle is already Sold!");
                    
                    else((CommercialCar)this.head.data).rent();
                    {
                        System.out.println("\nEnter the details of the transaction: ");
                        ((CommercialCar)this.head.data).rent();
                        this.head.data.rented=true;
                        this.head.data.sold=false;
                    }
                }
                
                else if(this.head.data instanceof PrivateCar)
                {
                    System.out.println("\nPrivate Car cannot be Rented.");
                }
            }
            
            else if(cnt>0)
            {
                for(curr=this.head, i=0;curr!=null && i<cnt;curr=curr.next);
                {
                    if(curr.data instanceof TwoWheeler)
                    {
                        if(curr.data.sold==true)
                            System.out.println("Vehicle is Already Sold!");
                        
                        else
                        {
                            System.out.println("\nEnter the details of the transaction: ");
                            ((TwoWheeler)curr.data).rent();
                            curr.data.rented=true;
                            curr.data.sold=false;
                        }
                            
                       
                    }                    

                    else if(curr.data instanceof CommercialCar)
                    {
                        if(curr.data.sold==true)
                            System.out.println("Vehicle is Already Sold!");
                        
                        else
                        {
                            System.out.println("\nEnter the details of the transaction: ");
                            ((CommercialCar)curr.data).rent();
                            curr.data.rented=true;
                            curr.data.sold=false;
                            
                        }
                        
                    }

                    else if(curr.data instanceof PrivateCar)
                    {
                        System.out.println("\nPrivate Car cannot be Rented.");
                    }
                }
            }
            
            else
                System.out.println("The Vehicle doesn't exist in the list.");           
            
        }
    }
}