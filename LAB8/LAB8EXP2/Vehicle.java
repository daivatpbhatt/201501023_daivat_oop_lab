/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Create a Vehicle categorization system using Linked List. Vehicles are either two wheelers or four wheelers. Implement Sellable and Rentable Interfaces.
Date: 13th March, 2016

FILE: Defining Parent Class Vehicle, its Child Classes and implementing Containership.
*/

package lab8exp2;


import java.util.Scanner;


abstract class Vehicle implements Sellable                                                  //Abstract Child Class.
{
    protected String name, chassis_num, company, datebought, colour, owner_name;
    protected double price;
    public int mileage;
    public boolean insurance, sold, rented;
    protected Engine eng;                       //Reference to Engine Class
    
    
    public Vehicle()                            //Constructor to initialise Values.
    {
        name=null;
        chassis_num=null;
        company=null;
        datebought=null;
        owner_name=null;
        price=0;
        mileage=0;
        insurance = false;
        sold=false;
        rented = false;
        eng=new Engine();                   //Object of Engine Class.
    }
    
    
    public void readdetails()                   //Member function to read details.
    {
        byte choice;
        Scanner in = new Scanner(System.in);
        System.out.println("\nEnter Name of the Vehicle: ");
        name = in.next();
        System.out.println("Enter Company of the Vehicle: ");
        company = in.next();
        System.out.println("Enter Chassis Number of the Vehicle: ");
        chassis_num = in.next();
        System.out.println("Enter Price of the Vehicle: ");
        price = in.nextDouble();
        System.out.println("Enter Approx. Mileage of the Vehicle: ");
        mileage = in.nextInt();
        System.out.println("Colour of Vehicle: ");
        colour=in.next();
        System.out.println("Insurance Existing for Vehicle? (1: Yes ; 0: No): ");
        if(in.nextInt()==1)
            insurance = true;
        else
            insurance = false;
        
               
        eng.readdetails();        
    }
    
    public void displaydetails()                   //Member function to display details.
    {
        System.out.println("Name: "+name);
        System.out.println("Company: "+company);
        System.out.println("Chassis Number: "+chassis_num);
        System.out.println("Price: "+price);
        System.out.println("Mileage: "+mileage);
        System.out.println("Colour: "+colour);
        
        if(insurance)
            System.out.println("\nInsurance included");
        
        else
            System.out.println("\nInsurance NOT included");
        
        eng.displaydetails();
        
        if(sold==true && rented == false)
        {
            System.out.println("\nVehicle is Sold!");
            System.out.println("\nOwner name: "+owner_name);
            System.out.println("Price sold at: "+price);
        }
        
        else if(sold == false)
                System.out.println("\nVehicle NOT Sold.");
        
    }
    
    public void sell(double value)              //Member function to edit sell details.
    {
        Scanner in = new Scanner(System.in);
        
        System.out.println("\nThe current price of the vehicle is: "+price);
        System.out.println("\nName of owner: ");
        owner_name = in.next();
        System.out.println("Selling price: ");
        price=in.nextDouble();
        rented=false;
        sold=true;
    }   
  
}


class Engine                                //Class to display Containership Relationship to Class Vehicle.
{
    String name, type, model_num, maker;
    double price;
    float weight;
    
    public Engine()                         //Constructor to initialise values.
    {
        name=null;
        type=null;
        model_num=null;
        maker=null;
        price=0;
        weight=0;
    }
    
    
    public void readdetails()                   //Member function to read details.
    {
        Scanner in = new Scanner(System.in);
        System.out.println("\nEnter Name of the Engine: ");
        name=in.next();
        System.out.println("Enter the Type of the Engine: ");
        type=in.next();
        System.out.println("Enter the Model Number of the Engine: ");
        model_num=in.next();
        System.out.println("Enter the Maker of the Engine: ");
        maker = in.next();
        System.out.println("Enter the Price of the Engine: ");
        price=in.nextDouble();
        System.out.println("Enter the Weight of the Engine: ");
        weight = in.nextFloat();
    }
    
    
    public void displaydetails()                   //Member function to display details.
    {
        System.out.println("\nEngine Name: "+name);
        System.out.println("Engine Type: "+type);
        System.out.println("Model Number: "+model_num);
        System.out.println("Engine Maker: "+maker);
        System.out.println("Engine Price: "+price);
        System.out.println("Engine Weight: "+weight);
    }
        
}


class TwoWheeler extends Vehicle implements Rentable                //Child Class of Class Vehicle.
{
    boolean accessories;
    String rented_name;
    double rented_price;
    int rented_months;
    
    
    public TwoWheeler()                         //Constructor to intialise values.
    {
        super();                                //Calling Constructor of Parent Class Vehicle.
        accessories=false;
    }
    
    @Override
    public void readdetails()                  //Member function to read details.
    {
        Scanner in = new Scanner(System.in);
        
        super.readdetails();
        System.out.println("Accessories included? (1: Yes ; 0: No): ");
        if(in.nextInt()==1)
            accessories=true;
        else
            accessories = false;      
    }
    
    
    @Override
    public void displaydetails()                   //Member function to display details.
    {
        System.out.println("\n----------------------------------------------------");
        System.out.println("\nVehicle type: Two Wheeler\n");
        super.displaydetails();
        
        
        if(rented==true && sold == false)
        {
            System.out.println("\nVehicle is Rented!");
            System.out.println("\nRented to: "+rented_name);
            System.out.println("Rented at Price: "+rented_price);
            System.out.println("Rented for Months: "+rented_months);
        }
        
        else if(rented == false)
                System.out.println("\nVehicle NOT Rented yet.");
        
        if(accessories)
            System.out.println("\nAccessories included!");
        else
            System.out.println("\nAccessories NOT included!");
    }
    
    public void rent()                              //Member function to edit rent details.
    {
        Scanner in = new Scanner(System.in);
        System.out.println("\nThe current price of the vehicle is: "+price);
        System.out.println("Name of Person Rented to: ");
        rented_name = in.next();
        System.out.println("Rented at Price: ");
        rented_price = in.nextDouble();
        System.out.println("Rented for Months: ");
        rented_months = in.nextInt();
        rented=true;
        sold=false;
    }
    
}


abstract class FourWheeler extends Vehicle              //Abstract Child Class of Class Vehicle.
{
    String type, fuel;
    
    public FourWheeler()                                //Constructor to initialise values.
    {
        super();                    //Calling Constructor of Parent Class Vehicle.
        type=null;
        fuel=null;
    }
    
    @Override
    public void readdetails()                           //Member function to read details.
    {
        Scanner in = new Scanner(System.in);
        super.readdetails();                           //Calling Member function of Parent Class Vehicle to read details.
        System.out.println("Enter the Type of Four Wheeler: ");
        type = in.next();
        System.out.println("Enter the Fuel of the Four Wheeler: ");
        fuel = in.next();
    }
    
    @Override
    public void displaydetails()                           //Member function to display details.
    {
        System.out.println("\n----------------------------------------------------");
        System.out.println("\nVehicle type: Four Wheeler\n");
        super.displaydetails();                                 //Calling Member function of Parent Class Vehicle to display details.
        System.out.println("\nType: "+type);
        System.out.println("Fuel: "+fuel);
        
    }
}


class PrivateCar extends FourWheeler                        //Child Class of Class FourWheeler.
{    
    public PrivateCar()                             //Constructor to initialise values.
    {
        super();                                //Calling Constructor of Parent Class FourWheeler.
        rented=false;
    }
    
    @Override
    public void readdetails()                           //Member function to read details.
    {
        Scanner in = new Scanner(System.in);
        super.readdetails();                           //Calling Member function of Parent Class FourWheeler to read details.
    }
    
    @Override
    public void displaydetails()                        //Member function to display details.
    {
        super.displaydetails();                           //Calling Member function of Parent Class FourWheeler to display details.
    }        
}


class CommercialCar extends FourWheeler implements Rentable
{
    String rented_name;
    double rented_price;
    int rented_months;
    
    public CommercialCar()                      //Constructor to initialise values.
    {
        super();                                //Calling Constructor of Parent Class FourWheeler.
    }
    
    @Override
    public void readdetails()                           //Member function to read details.
    {
        super.readdetails();                           //Calling Member function of Parent Class FourWheeler to read details.
               
    }
    
    @Override
    public void displaydetails()                                //Member function to display details.
    {
        super.displaydetails();                                 //Calling Member function of Parent Class FourWheeler to display details.
        
        if(rented==true && sold == false)
        {
            System.out.println("\nVehicle is Rented!");
            System.out.println("\nRented to: "+rented_name);
            System.out.println("Rented at Price: "+rented_price);
            System.out.println("Rented for Months: "+rented_months);
        }
        
        else if(rented == false && sold == false)
                System.out.println("\nVehicle NOT Rented yet.");
    }
    
    public void rent()                              //Member function to edit rent details.
    {
        Scanner in = new Scanner(System.in);
        System.out.println("\nThe current price of the vehicle is: "+price);
        System.out.println("Name of Person Rented to: ");
        rented_name = in.next();
        System.out.println("Rented at Price: ");
        rented_price = in.nextDouble();
        System.out.println("Rented for Months: ");
        rented_months = in.nextInt();
        rented=true;
        sold=false;
    }
}