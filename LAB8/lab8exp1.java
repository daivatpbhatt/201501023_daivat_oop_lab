/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Create appropriate interfaces called Searchable and Sortable for an integer List.
Date: 13th March, 2016
*/



package lab8exp1;

import java.util.*;

interface Searchable
{
    public void search(int i);
}

interface Sortable
{
    public void sort(byte choice);
}

class MyList implements Searchable, Sortable
{
    int[] array;
    int size;
    
    public MyList(int n)
    {
        size=n;
        array= new int[size];
    }
    
    
    public MyList(MyList A)
    {
       int i;
       this.size=A.size;
       this.array=new int[size];
       for(i=0;i<A.array.length;i++)
       {
           this.array[i]=A.array[i];
       }
    }
    
    
    public void readdetails()
    {
        Scanner in = new Scanner(System.in);
        int i;
        System.out.println("\nEnter the details of the array: ");
        for(i=0;i<array.length;i++)
        {
            System.out.print("Element #"+(i+1)+": ");
            array[i]=in.nextInt();
        }
    }
    
    public void displaydetails()
    {
        int i;
        System.out.println("\nThe array is: \n");
        
        System.out.print("(");
        for(i=0;i<array.length;i++)
        {
            if(i<array.length-1)
                System.out.print(+array[i]+", ");
            
            else if(i==array.length-1)
                System.out.println(+array[i]+")");
        }
        
    }
    
    public void search(int i)
    {
        int j, cnt=0;
        
        for(j=0;j<array.length;j++)
        {
            if(array[j]==i)
            {
                System.out.println("The number "+i+" exists in the array at position #"+(j+1));
                break;
            }
            
            else
            {
                cnt++;
            }
        }
        
        if(cnt==array.length-1)
            System.out.println("The number "+i+" does NOT exist in the array!");
    }
    
    public void sort(byte choice)
    {
        int i, j, temp;
        
        if(choice==1)
        {
            for(i=0;i<array.length-1;i++)
            {
                for(j=i+1;j<array.length;j++)
                {
                    if(array[i]>array[j])
                    {
                        temp=array[i];
                        array[i]=array[j];
                        array[j]=temp;
                    }
                }
            }
        }
        
        else if(choice==2)
        {
            for(i=0;i<array.length-1;i++)
            {
                for(j=i+1;j<array.length;j++)
                {
                    if(array[i]<array[j])
                    {
                        temp=array[i];
                        array[i]=array[j];
                        array[j]=temp;
                    }
                }
            }
            
        }
        
        else
            System.out.println("\nEnter valid choice!");
        
        displaydetails();
        
    }
}

public class Lab8exp1 
{
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        byte choice, sortchoice;
        int size, search;
        MyList M;
       
        System.out.println("Enter the size of the array: ");
        size = in.nextInt();
        M= new MyList(size);
       
        while(true)
        {
           
            System.out.println("\n1. Enter the elements of the array.");
            System.out.println("2. Display the array.");
            System.out.println("3. Search a number.");
            System.out.println("4. Sort the array.");
            System.out.println("0. Exit.");
            System.out.println("\nEnter your choice: ");
            choice = in.nextByte();
            
            switch(choice)
            {
                case 1:
                    M.readdetails();
                    
                break;
                
                case 2:
                    M.displaydetails();
                    
                break;
                
                case 3:
                {
                    System.out.println("Enter the number to search: ");
                    search = in.nextInt();
                    M.search(search);                    
                }
                
                break;
                
                case 4:
                {
                    System.out.println("Search by? (1 : Ascending ; 2 : Descending): ");
                    sortchoice=in.nextByte();
                    MyList Copy = new MyList(M);
                    
                    Copy.sort(sortchoice);
                }
                
                break;
                
                case 0:
                    return;
                
                default:
                    System.out.println("Enter valid choice!");
                    
            }
       }
    }
    
}