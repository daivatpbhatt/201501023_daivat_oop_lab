/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 1 [CUSTOMER BANKING] by using GUI.
Date: 31st March, 2016
*/


package lab10exp1;

import java.awt.*;
import javax.swing.*;

public class Lab10exp1 {

	public static void main(String[] args) 
	{
		//Frames
		JFrame Frame1 = new JFrame();		//For Details.
		JFrame Frame2 = new JFrame();		//For Transactions.
		
		
		Frame1.setLayout(new FlowLayout(FlowLayout.CENTER));
		Frame1.setSize(500, 500);
		Frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);				
		Frame1.setLocationRelativeTo(null);		
		
		Frame2.setLayout(new FlowLayout(FlowLayout.CENTER));
		Frame2.setSize(300, 300);
		Frame2.setLocationRelativeTo(null);
		
		
		//Panels
		JPanel panel1 = new JPanel();		
		JPanel panel2 = new JPanel();
		
		panel1.setLayout(new GridLayout(0, 2));
		panel1.setSize(10, 10);
		panel1.setBackground(Color.white);		
		
		panel2.setLayout(new GridLayout(0, 2));
		panel2.setSize(5, 5);
		panel2.setBackground(Color.CYAN);
		
		//Controls
		JLabel lblName = new JLabel("Customer Name");
		JTextField txtName = new JTextField(9);
		
        JLabel lblContactNo = new JLabel("Customer Account Number");        
        JTextField txtContactNo = new JTextField(9);
        
        JLabel lblAddress = new JLabel("Customer Address");
        JTextField txtAddress = new JTextField(5);
        
        JLabel lblAmt = new JLabel("Enter Amount");
        JTextField txtAmt = new JTextField(7);
        
        JButton btnWithdraw = new JButton("Withdraw");
        JButton btnDeposit = new JButton("Deposit");
        
        JButton btnSubmit = new JButton("Show Balance");
        
        ButtonGroup radgrp = new ButtonGroup();			//Grouping Radio Buttons.        
        
       	JRadioButton rbutSav = new JRadioButton("Savings");
       	JRadioButton rbutCur = new JRadioButton("Current");
       	
       	radgrp.add(rbutCur);
       	radgrp.add(rbutSav);
       	
        
        
        
        lblAmt.setSize(10, 10);
        lblAddress.setSize(10, 10);        
        lblName.setSize(10, 10);
        lblContactNo.setSize(10, 10);
        btnWithdraw.setSize(5, 5);
        btnDeposit.setSize(5, 5);
        btnSubmit.setSize(5, 5);         
        
        //Adding to the Panel 1
        panel1.add(lblName);
        panel1.add(txtName);
        panel1.add(lblContactNo);
        panel1.add(txtContactNo);
        panel1.add(lblAddress);
        panel1.add(txtAddress);
        panel1.add(rbutSav);
        panel1.add(rbutCur);

        //Adding to the Panel 2
        panel2.add(lblAmt);
        panel2.add(txtAmt);        
        panel2.add(btnWithdraw);
        panel2.add(new JLabel(""));
        panel2.add(btnDeposit);
        panel2.add(new JLabel(""));
        panel2.add(btnSubmit);
        
        //Setting Visibility of Frame.
        Frame1.setVisible(true);
        Frame2.setVisible(true);
        
        //Adding Panels to corresponding Frames.
        Frame1.add(panel1);
        
        Frame2.add(panel2);
        
        
		
		
		
	}

}
