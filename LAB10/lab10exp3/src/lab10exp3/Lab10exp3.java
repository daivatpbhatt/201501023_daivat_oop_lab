/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 2 of Assignment 2 [EMPLOYEE MANAGEMENT] by using GUI.
Date: 31st March, 2016
*/


package lab10exp3;

import java.awt.*;
import javax.swing.*;

public class Lab10exp3 
{

	static JFrame Frame;
	static JPanel panel1, panel2, panel3;
	
	public static void main(String[] args) 
	{
		//Frame
		Frame = new JFrame();
		
		Frame.setLayout(new FlowLayout(FlowLayout.CENTER));
		Frame.setSize(400, 400);
		Frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Frame.setLocationRelativeTo(null);
		
		//Panels
		panel1 = new JPanel();	
		panel2 = new JPanel();
		panel3 = new JPanel();
		
		panel1.setLayout(new GridLayout(0, 2));
		panel1.setSize(100, 100);
		panel1.setBackground(Color.lightGray);		
		
		panel2.setLayout(new GridLayout(0, 2));
		panel2.setSize(200, 200);
		panel2.setBackground(Color.WHITE);
		
		panel3.setLayout(new GridLayout(0, 2));
		panel3.setSize(250, 250);
		panel3.setBackground(Color.CYAN);
		
		//Controls for Panel1
		JLabel lblName = new JLabel("Employee Name");
		JTextField txtName = new JTextField(10);
		
		JLabel lblNum = new JLabel("Employee Number");
		JTextField txtNum = new JTextField(5);
		
		JLabel lblGend = new JLabel("Gender");
		
		ButtonGroup gend = new ButtonGroup();
		JRadioButton rbtnm = new JRadioButton("Male");
		JRadioButton rbtnf = new JRadioButton("Female");
		JRadioButton rbtno = new JRadioButton("Other");
		
		gend.add(rbtnm);
		gend.add(rbtnf);
		gend.add(rbtno);
		
		JLabel lbldob = new JLabel("Date of Birth");
		JTextField txtdob = new JTextField(10);
		
		JLabel lblcontact = new JLabel("Contact Number");
		JTextField txtcontact = new JTextField(10);
		
		JLabel lblSal = new JLabel("Basic Salary");
		JTextField txtSal = new JTextField(10);
		
		
			
		//Controls for Panel2
		JButton btnNS = new JButton("Show Net Salary");
		JButton btnE = new JButton("Exit");
		;
		
		//Controls for Panel3
		JLabel lblnet = new JLabel("Net Salary");
		JTextField txtnet = new JTextField(9);
		
		//Adding controls to Panel1.
		panel1.add(lblName);
		panel1.add(txtName);
		panel1.add(lblNum);
		panel1.add(txtNum);
		panel1.add(lblGend);
		panel1.add(rbtno);
		panel1.add(rbtnm);
		panel1.add(rbtnf);
		panel1.add(new JLabel());
		panel1.add(new JLabel());
		panel1.add(lbldob);
		panel1.add(txtdob);
		panel1.add(lblcontact);
		panel1.add(txtcontact);
		
		panel1.add(new JLabel());
		panel1.add(new JLabel());
		
		panel1.add(lblSal);
		panel1.add(txtSal);

		//Adding controls to Panel12
		panel2.add(new JLabel());
		panel2.add(new JLabel());
		panel2.add(btnNS);
		panel2.add(btnE);
		panel2.add(new JLabel());
		panel2.add(new JLabel());
		
		//Adding controls to Panel3.
		panel3.add(lblnet);
		panel3.add(txtnet);
		
		//Adding Panels to the frame.
		Frame.add(panel2);
		Frame.add(panel1);
		
		Frame.add(panel3);
		
		Frame.setVisible(true);
		
		
		
		
		

	}

}
