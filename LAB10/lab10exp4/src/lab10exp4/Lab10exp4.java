/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 3 of Assignment 4 [STACK] by using GUI.
Date: 31st March, 2016
*/

package lab10exp4;

import java.awt.*;
import javax.swing.*;
public class Lab10exp4 
{
	static JFrame Frame;
	static JPanel panel1, panel2, panel3;
	public static void main(String[] args) 
	{
		//Frame.
		Frame = new JFrame();
		
		Frame.setLayout(new FlowLayout(FlowLayout.CENTER));
		Frame.setSize(250, 300);
		Frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Frame.setLocationRelativeTo(null);

		//Panels
		panel1 = new JPanel();
		panel1.setLayout(new GridLayout(0, 1));
		panel1.setSize(500, 200);
		
		panel2 = new JPanel();
		panel2.setLayout(new GridLayout(0, 1));
		panel2.setSize(90, 200);
		
		panel3 = new JPanel();
		panel3.setLayout(new GridLayout(0, 3, 2, 2));
		panel3.setSize(200, 100);
		
		//Control for Panel1.
		JLabel lbldisp = new JLabel("The Stack is");
		
		
		//Control for Panel2.
		JLabel lblstck = new JLabel("*Stack displayed here*");
		
		
		//Control for Panel3.
		JButton btnpush = new JButton("Push");
		JButton btnpeep = new JButton("Peep");
		JButton btnpop = new JButton("Pop");
		
		JLabel lblenter = new JLabel("Enter element");
		JTextField txtenter = new JTextField(5);
		
		//Adding controls to Panel1.
		panel1.add(lbldisp);
		
		//Adding controls to Panel2.
		panel2.add(lblstck);
		
		//Adding controls to Panel3.
		panel3.add(btnpush);
		panel3.add(btnpeep);
		panel3.add(btnpop);
		panel3.add(lblenter);
		panel3.add(txtenter);
		
		//Adding Panels to the frame.
		Frame.add(panel1);
		Frame.add(panel2);
		Frame.add(panel3);
		
		Frame.setVisible(true);
		
	}

}
