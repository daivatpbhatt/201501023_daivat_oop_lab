/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 2 of Assignment 1 [CUSTOMER SALES] by using GUI.
Date: 31st March, 2016
*/

package lab10exp2;

import java.awt.*;
import javax.swing.*;

public class Lab10exp2 
{
	
	static JFrame Frame1, Frame2, Frame3;
	static JPanel panel1, panel2, panel3, panel4, headpanel1, headpanel2, headpanel3;
	
	public static void main(String[] args) 
	{
		//Frames
		Frame1 = new JFrame();			//Customer Details.
		Frame2 = new JFrame();			//Product Details.
		Frame3 = new JFrame();			//Transactions
		
		
		Frame1.setLayout(new FlowLayout(FlowLayout.CENTER));
		Frame1.setSize(400, 350);
		Frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);				
		Frame1.setLocationRelativeTo(null);		
		
		Frame2.setLayout(new FlowLayout(FlowLayout.CENTER));
		Frame2.setSize(400, 250);
		Frame2.setLocationRelativeTo(Frame1);

		Frame3.setLayout(new FlowLayout(FlowLayout.CENTER));
		Frame3.setSize(400, 350);
		Frame3.setLocationRelativeTo(Frame2);
		
		//Panels
		panel1 = new JPanel();	
		panel2 = new JPanel();
		panel3 = new JPanel();
		panel4 = new JPanel();
		
		//HeadPanels
		headpanel1= new JPanel();
		headpanel2= new JPanel();
		headpanel3= new JPanel();
		
		headpanel1.setLayout(new GridLayout(1, 1));
		headpanel1.setSize(50, 20);
		headpanel1.setBackground(Color.magenta);
		
		headpanel2.setLayout(new GridLayout(1, 1));
		headpanel2.setSize(50, 20);
		headpanel2.setBackground(Color.magenta);
		
		headpanel3.setLayout(new GridLayout(1, 1));
		headpanel3.setSize(50, 20);
		headpanel3.setBackground(Color.magenta);
		
		panel1.setLayout(new GridLayout(0, 2));
		panel1.setSize(100, 100);
		panel1.setBackground(Color.white);		
		
		panel2.setLayout(new GridLayout(0, 2));
		panel2.setSize(200, 200);
		panel2.setBackground(Color.CYAN);
		
		panel3.setLayout(new GridLayout(0, 2));
		panel3.setSize(250, 250);
		panel3.setBackground(Color.CYAN);
		
		//Controls for Headpanel1
		JLabel lblCust = new JLabel("CUSTOMER");
		lblCust.setFont(new Font("Serif", Font.BOLD, 24));
		
		
		
		//Controls for Panel1
		JLabel lblCName = new JLabel("Customer Name");
		JTextField txtCName = new JTextField(9);
		JLabel lblCAddress = new JLabel("Customer Address");
		JTextField txtCAddress = new JTextField(15);
		JLabel lblCNum = new JLabel("Contact Number");
		JTextField txtCNum = new JTextField(10);
		
		ButtonGroup grp = new ButtonGroup();					//Grouping Radio Buttons
		JRadioButton rbutReg = new JRadioButton("Regular Customer");
		JRadioButton rbutNew = new JRadioButton("New Customer");
		
		//Adding radio buttons to the group.
		grp.add(rbutNew);
		grp.add(rbutReg);
		
		
		//Controls for Headpanel2
		JLabel lblProd = new JLabel("PRODUCT");
		lblProd.setFont(new Font("Serif", Font.BOLD, 24));
		
		
		
		//Controls for Panel2
		JLabel lblPName = new JLabel("Product Name");
		JTextField txtPName = new JTextField(9);
		JLabel lblPCode = new JLabel("Product ID");
		JTextField txtPCode = new JTextField(10);
		JLabel lblPPrice = new JLabel("Product Price");		
		JTextField txtPPrice = new JTextField(15);
		
		
		//Controls for Headpanel3
		JLabel lblTran = new JLabel("TRANSACTIONS");
		lblTran.setFont(new Font("Serif", Font.BOLD, 24));
		
		
		//Controls for Panel3
		JLabel lblC1Name = new JLabel("Customer Name");
		JTextField txtC1Name = new JTextField(9);
		JLabel lblC1Num = new JLabel("Contact Number");
		JTextField txtC1Num = new JTextField(10);
		
		JLabel lblP1Name = new JLabel("Product Name");
		JTextField txtP1Name = new JTextField(9);
		JLabel lblP1Code = new JLabel("Product ID");
		JTextField txtP1Code = new JTextField(10);
		JLabel lblP1Price = new JLabel("Product Price");		
		JTextField txtP1Price = new JTextField(15);
		
		
		//Controls for Panel4
		JButton btnadd = new JButton("Add ITEM!");
		panel4.add(btnadd);
		
		
		//Adding controls to headpanel1
		headpanel1.add(lblCust);
		
		//Adding controls to Panel1
		panel1.add(lblCName);
		panel1.add(txtCName);
		panel1.add(lblCAddress);
		panel1.add(txtCAddress);
		panel1.add(lblCNum);
		panel1.add(txtCNum);
		panel1.add(rbutReg);
		panel1.add(rbutNew);
		
		//Adding controls to headpanel2
		headpanel2.add(lblProd);
		
		//Adding controls to Panel2
		panel2.add(lblPName);
		panel2.add(txtPName);
		panel2.add(lblPCode);
		panel2.add(txtPCode);
		panel2.add(lblPPrice);
		panel2.add(txtPPrice);
		
		
		//Adding controls to headpanel3
		headpanel3.add(lblTran);
		
		//Adding controls to Panel3
		panel3.add(lblC1Name);
		panel3.add(txtC1Name);
		panel3.add(lblC1Num);
		panel3.add(txtC1Num);
		panel3.add(new JLabel());
		panel3.add(new JLabel());
		panel3.add(new JLabel());
		panel3.add(new JLabel());
		panel3.add(lblP1Name);
		panel3.add(txtP1Name);
		panel3.add(lblP1Code);
		panel3.add(txtP1Code);
		panel3.add(lblP1Price);
		panel3.add(txtP1Price);
		
		
		//Adding panels to corresponding frames.
		Frame1.add(headpanel1);
		Frame1.add(panel1);		
		Frame1.setVisible(true);
		
		Frame2.add(headpanel2);
		Frame2.add(panel2);
		Frame2.setVisible(true);
		
		
		Frame3.add(headpanel3);
		Frame3.add(panel3);
		Frame3.add(panel4);
		Frame3.setVisible(true);		
		
		
		
	}

}
