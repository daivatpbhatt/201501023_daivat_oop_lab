/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 1 [BANKING] by using GUI, Event Handling and DataBase Storage.
FILE: Main Class.
Date: 14th April, 2016
*/

package banking_main;

import javax.swing.JFrame;

import banking_GUI.Banking_GUI;

public class Banking_Main 
{

	public static void main(String[] args) 
	{
		Banking_GUI bankgui = new Banking_GUI();
		bankgui.setLocationRelativeTo(null);
		bankgui.setVisible(true);
		bankgui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
