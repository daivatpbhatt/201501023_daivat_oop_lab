/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 1 [BANKING] by using GUI, Event Handling and DataBase Storage.
FILE: Banking_GUI Class.
Date: 14th April, 2016
*/

package banking_GUI;

import java.awt.*;
import javax.swing.*;

import banking_listener.BankingActionListener;

@SuppressWarnings("serial")
public class Banking_GUI extends JFrame
{
	//Panels
	JPanel inputpanel = new JPanel();
	JPanel gendpan = new JPanel();
	JPanel btnpan = new JPanel();
	JPanel typepan = new JPanel();
	JPanel temp = new JPanel();
	public JPanel outputpanel = new JPanel();
	
	//Controls of Input Panel
	JLabel lblName = new JLabel("Name");
	JTextField txtName = new JTextField(10);	
	JLabel lblNum = new JLabel("Account Number");
	JTextField txtNum = new JTextField(5);	
	JLabel lblGend = new JLabel("Gender");	
	JLabel lblage = new JLabel("Age");
	JTextField txtage = new JTextField(10);	
	JLabel lbltype = new JLabel("Account Type");
	JLabel lblbal = new JLabel("Balance");
	JTextField txtbal = new JTextField(10);	
	
	//Controls for gendpan Panel.
	ButtonGroup gend = new ButtonGroup();
	JRadioButton rbtnm = new JRadioButton("Male");
	JRadioButton rbtnf = new JRadioButton("Female");
	JRadioButton rbtno = new JRadioButton("Other");	
	
	
	//Controls for typepan Panel.
	ButtonGroup type = new ButtonGroup();
	JRadioButton rbtns = new JRadioButton("Savings");
	JRadioButton rbtnc = new JRadioButton("Current");

	//Controls for btnpan Panel.
	public JButton btnAdd = new JButton("Add");
	public JButton btnE = new JButton("Exit");
	public JButton btndisp = new JButton("Display");
	
	public Banking_GUI()
	{
		
		//Setting Frame Display Details.
		this.setTitle("EMPLOYEE DETAILS");
		this.setSize(900, 650);
		this.setLayout(new GridLayout(0, 1));
		this.setBackground(Color.white);
		
				
		
		//Creating object of EmployeeActionListener class.
		BankingActionListener listen = new BankingActionListener(this);
		
		//Adding Action Listener for each button.
		btnAdd.addActionListener(listen);
		btndisp.addActionListener(listen);
		btnE.addActionListener(listen);
		
		
		//Setting inputpanel Panel Display Details.
		inputpanel.setLayout(new GridLayout(0, 2));
		inputpanel.setSize(500, 500);
		inputpanel.setBackground(Color.white);	
		
		//Setting gendpan Panel Display Details.
		gendpan.setLayout(new GridLayout(0, 4));
		gendpan.setSize(200, 250);
		
		//Setting btnpan Panel Display Details.
		btnpan.setLayout(new FlowLayout(FlowLayout.CENTER));
		btnpan.setSize(10, 10);

		//Setting btnpan Panel Display Details.
		typepan.setLayout(new GridLayout(1, 0));
		typepan.setSize(10, 20);

		
		//Setting temp Panel Display Details.
		temp.setLayout(new GridLayout(0, 1));
		temp.setSize(25, 25);
		
		//Setting outputpanel Panel Display Details.
		outputpanel.setLayout(new GridLayout(0,1, 10, 10));
		outputpanel.setSize(1000, 300);
		outputpanel.setBackground(Color.WHITE);
		
		

		//Adding radio buttons to gend ButtonGroup.
		gend.add(rbtno);
		gend.add(rbtnm);
		gend.add(rbtnf);
		
		//Adding radio buttons to type ButtonGroup.
		type.add(rbtnc);
		type.add(rbtns);
		
		//Adding controls to inputpanel Panel
		inputpanel.add(lblNum);
		inputpanel.add(txtNum);
		inputpanel.add(lblName);
		inputpanel.add(txtName);
		inputpanel.add(new JLabel());
		inputpanel.add(new JLabel());
		inputpanel.add(lblage);
		inputpanel.add(txtage);
		
		inputpanel.add(new JLabel());
		inputpanel.add(new JLabel());
		
		inputpanel.add(lblbal);
		inputpanel.add(txtbal);

		inputpanel.add(new JLabel());
		inputpanel.add(new JLabel());
		
		//Adding controls to gendpan Panel.
		gendpan.add(lblGend);
		gendpan.add(rbtno);
		gendpan.add(rbtnm);
		gendpan.add(rbtnf);
		
		//Adding controls to btnpan Panel.
		btnpan.add(btnAdd);
		btnpan.add(btndisp);
		btnpan.add(btnE);
		btnpan.add(new JLabel());
		btnpan.add(new JLabel());
		
		//Adding controls to typepan Panel.
		typepan.add(lbltype);
		typepan.add(rbtnc);
		typepan.add(rbtns);
		
		
		temp.add(gendpan);
		temp.add(typepan);
		
		//Adding Panels to Frame.
		this.add(inputpanel, BorderLayout.NORTH);
		this.add(temp, BorderLayout.NORTH);
		this.add(btnpan, BorderLayout.CENTER);
		this.add(outputpanel, BorderLayout.SOUTH);
	

	}
	
	public String getAccNum()
	{
		return this.txtNum.getText();
	}
	
	public String getName()
	{
		return this.txtName.getText();
	}
	
	public String getAge()
	{
		return this.txtage.getText();
	}
	
	public String getBal()
	{
		return this.txtbal.getText();
	}
	
	public String getGender()
	{
		if(this.rbtnm.isSelected())
			return "Male";
		
		else if(this.rbtnf.isSelected())
			return "Female";
		
		else
			return "Other";
	}
	
	public String gettype()
	{
		if(this.rbtnc.isSelected())
			return "Current";
		
		else if(this.rbtns.isSelected())
			return "Savings";
		
		else
			return "None";
	}

}
