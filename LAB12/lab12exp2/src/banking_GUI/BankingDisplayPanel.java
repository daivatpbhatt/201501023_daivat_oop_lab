/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 1 [BANKING] by using GUI, Event Handling and DataBase Storage.
FILE: BankingDisplayPanel Class.
Date: 14th April, 2016
*/

package banking_GUI;


import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class BankingDisplayPanel extends JPanel
{
	//Controls for Panel
	private JLabel lblIdValue = new JLabel();
	private JLabel lblNameValue = new JLabel();
	private JLabel lblGender = new JLabel();
	private JLabel lbldob = new JLabel();
	private JLabel lbltype = new JLabel();
	private JLabel lblbal = new JLabel();
	
	
	public BankingDisplayPanel(String strAccNum, String strName, String Gender, String dob, String type, String balance)
	{
		lblIdValue.setText(strAccNum);
		lblNameValue.setText(strName);
		lblGender.setText(Gender);
		lbldob.setText(dob);
		lbltype.setText(type);
		lblbal.setText(balance);
		
		//Setting Panel Display Details.
		this.setLayout(new GridLayout(1, 0, 2, 2));
		
		//Adding Controls to Panel.
		this.add(lblIdValue);
		this.add(lblNameValue);
		this.add(lblGender);
		this.add(lbldob);
		this.add(lbltype);
		this.add(lblbal);
		
		
		
	}

}
