/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 1 [BANKING] by using GUI, Event Handling and DataBase Storage.
FILE: Banking Class.
Date: 14th April, 2016
*/

package banking;

public class Banking 
{
	private String acc_num, name, age, gender, type;
	private double balance;
	
	public Banking(){}
	
	public Banking(String accnum, String accname, String accage, String accgend, String acctype, Double bal)
	{
		this.acc_num=accnum;
		this.name=accname;
		this.age=accage;
		this.gender=accgend;
		this.type = acctype;
		this.balance=bal;
	}
	
    public String getAcc_num() 
    {
		return acc_num;
	}
    
	public String getName() 
	{
		return name;
	}
	
	public String getAge() 
	{
		return age;
	}
	
	public String getGender() 
	{
		return gender;
	}
	
	public String getType() 
	{
		return type;
	}
	public double getBalance() 
	{
		return balance;
	}
	
	public void setAcc_num(String acc_num) 
	{
		this.acc_num = acc_num;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public void setAge(String age) 
	{
		this.age = age;
	}
	
	public void setGender(String gender) 
	{
		this.gender = gender;
	}
	
	public void setType(String type) 
	{
		this.type = type;
	}
	public void setBalance(double balance)
	{
		this.balance = balance;
	}
	
}
