/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 1 [BANKING] by using GUI, Event Handling and DataBase Storage.
FILE: BankingList Class.
Date: 14th April, 2016
*/

package banking;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import banking_connection.BankingConnect;

public class BankingList extends Banking 
{
	public ArrayList<Banking> fetchRecords() throws SQLException, Exception
	{
		ArrayList<Banking> banklist = new ArrayList<Banking>();
		
		//Fetch Records from Database
		//STEP 4: Execute a query
	   System.out.println("Creating statement...");
	   Connection conn = BankingConnect.getConnection();
	   Statement stmt = conn.createStatement();
	   String sql;
	   sql = "SELECT * FROM banking ORDER BY AccountNum";
	   ResultSet rs = stmt.executeQuery(sql);

	   //STEP 5: Extract data from result set
	   while(rs.next())
	   {
	      //Retrieve by column name
	      String id  = rs.getString("AccountNum");
	      String empname = rs.getString("Name");
	      String contact = rs.getString("Gender");
	      String gender = rs.getString("Age");
	      String dob = rs.getString("Acc_Type");
	      Double base = rs.getDouble("Balance");
	      
	      banklist.add(new Banking(id, empname, contact, gender, dob, base));
      
	   }
	   return banklist;
	}
	
	public void addRecord(Banking bank) throws SQLException, Exception
	{
		//STEP 4: Execute a query
		System.out.println("Creating statement...");
		Connection conn = BankingConnect.getConnection();
		Statement stmt = conn.createStatement();
		String sql;
		sql = "INSERT INTO banking VALUES(" + " '" + bank.getAcc_num()+ "' " + ", '" + bank.getName() + "', '" + bank.getGender() + "', '" + bank.getAge()+ "', '" + bank.getType() + "', " + bank.getBalance() + ")";
		stmt.execute(sql);
		
	}
}
