/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 1 [BANKING] by using GUI, Event Handling and DataBase Storage.
FILE: BankingActionListener Class.
Date: 14th April, 2016
*/

package banking_listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import banking.Banking;
import banking.BankingList;
import banking_GUI.BankingDisplayPanel;
import banking_GUI.Banking_GUI;

public class BankingActionListener implements ActionListener
{
	private Banking_GUI bankgui;
	
	
	public BankingActionListener(Banking_GUI bank)
	{
		bankgui= bank;
	}
	
	public void displayStudentRecords()
	{
		ArrayList<BankingDisplayPanel> BankRecords = new ArrayList<BankingDisplayPanel>();
		BankRecords.add(new BankingDisplayPanel("Account Number", "Name", "Gender", "Age", "Account Type", "Balance"));

		try
		{
			BankingList list = new BankingList();
			
			ArrayList<Banking> records = list.fetchRecords();
			
			for(Banking bank: records)
				BankRecords.add(new BankingDisplayPanel(bank.getAcc_num(), bank.getName(), bank.getGender(), bank.getAge(), bank.getType(), Double.toString(bank.getBalance())));
			
	
				bankgui.outputpanel.removeAll();
				
				for(BankingDisplayPanel r: BankRecords)
					bankgui.outputpanel.add(r);
			
				bankgui.validate();
				this.bankgui.repaint();
		
		
		}
		
		catch(SQLException err)
		{
			err.printStackTrace();
		}
		catch(Exception err)
		{
			err.printStackTrace();
		}
		
	}
	
	public void actionPerformed(ActionEvent event)
	{
		BankingList list = new BankingList();
		
		if(event.getActionCommand() == bankgui.btnAdd.getText())
		{
			System.out.println("New Record");
			Banking newemp = new Banking(bankgui.getAccNum(), bankgui.getName(), bankgui.getGender(), bankgui.getAge(), bankgui.gettype(), Double.parseDouble(bankgui.getBal()));
			
			try
			{			
				
				list.addRecord(newemp);
				
				this.displayStudentRecords();
			}
			catch(SQLException err)
			{
				err.printStackTrace();
			}
			catch(Exception err)
			{
				err.printStackTrace();
			}
			
		}
		
		else if(event.getActionCommand() == bankgui.btndisp.getText())
		{
			try
			{
				this.displayStudentRecords();
			}
			
			catch(Exception err)
			{
				err.printStackTrace();
			}
			
		}
		
		else if(event.getActionCommand() == bankgui.btnE.getText())
			bankgui.dispose();
	}

}
