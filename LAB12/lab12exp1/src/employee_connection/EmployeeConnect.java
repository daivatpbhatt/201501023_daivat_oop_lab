/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 11 [EMPLOYEE] by using GUI, Event Handling and DataBase Storage.
FILE: EmployeeConnect Class.
Date: 14th April, 2016
*/

package employee_connection;

import java.sql.*;

public class EmployeeConnect 
{
   //JDBC driver name and database URL
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
   static final String DB_URL = "jdbc:mysql://localhost/employee?autoReconnect=true&useSSL=false";

   //  Database credentials
   static final String USER = "root";
   static final String PASS = "root";

   private static Connection conn = null;

	public static Connection getConnection() throws SQLException, ClassNotFoundException
	{
		if(conn != null)
			return conn;
		else
		{
			try
			{
			   //STEP 2: Register JDBC driver
			   Class.forName("com.mysql.jdbc.Driver");
	
			   //STEP 3: Open a connection
			   System.out.println("Connecting to database...");
			   conn = DriverManager.getConnection(DB_URL,USER,PASS);
					   
			   return conn;
			}
			
			finally
			{}
		}
	}
	
	public static void closeConnection() throws SQLException
	{
		try
		{
			if(conn != null)
				conn.close();
		}
		
		finally
		{}
	}
	
	
	
}

