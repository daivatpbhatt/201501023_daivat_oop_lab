/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 11 [EMPLOYEE] by using GUI, Event Handling and DataBase Storage.
FILE: Employee Class.
Date: 14th April, 2016
*/


package employee;

public class Employee 
{
	private String emp_num, emp_name, dateofbirth, contact_num;
	private String gender;
	private double baseSal, netSal;
	
	
	public Employee(){}
	
	//Parameterised Constructor.
	public Employee(String number, String name, String contact, String gender, String dateofbirth, double baseSal)
	{
		this.emp_num=number;
		this.emp_name=name;
		this.contact_num=contact;
		this.dateofbirth=dateofbirth;
		this.gender=gender;
		this.baseSal=baseSal;
		this.netSal=this.calNetSal(baseSal);
	}	
	
	
	//Getters
	public String getEmp_num() 
	{
		return emp_num;
	}
	public String getEmp_name() 
	{
		return emp_name;
	}
	public String getDateofbirth() 
	{
		return dateofbirth;
	}
	public String getContact_num() 
	{
		return contact_num;
	}
	public String getGender() 
	{
		return gender;
	}
	public double getBaseSal() 
	{
		return baseSal;
	}
	public double getNetSal() 
	{
		return netSal;
	}
	
	
	public void setEmp_num(String emp_num) 
	{
		this.emp_num = emp_num;
	}
	public void setEmp_name(String emp_name) 
	{
		this.emp_name = emp_name;
	}
	public void setDateofbirth(String dateofbirth) 
	{
		this.dateofbirth = dateofbirth;
	}
	public void setContact_num(String contact_num) 
	{
		this.contact_num = contact_num;
	}
	public void setGender(String gender) 
	{
		this.gender = gender;
	}
	public void setBaseSal(double baseSal) 
	{
		this.baseSal = baseSal;
	}
	public void setNetSal(double netSal) 
	{
		this.netSal = calNetSal(this.baseSal);
	}
	
	public double calGrossSal(double BaseSal)
	{
		double DA=(0.6)*BaseSal;
		double HRA=0.12*BaseSal;
		double TA=12000;
		
		double GrossSal=BaseSal + DA + HRA + TA;
		return GrossSal;
		
	}
	
	public double calNetSal(double BaseSal)
	{
		double GrossSal=calGrossSal(BaseSal);
		double PF=0.1*BaseSal;
		double PT=200;
		double TA=12000;
		double TDS=0.1*(GrossSal - TA - PF - PT);
		double TD=PF + PT + TDS;
		double NetSal=GrossSal-TD;
		netSal=NetSal;
		return NetSal;
	}
	
}
