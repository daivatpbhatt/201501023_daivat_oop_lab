/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 11 [EMPLOYEE] by using GUI, Event Handling and DataBase Storage.
FILE: EmployeeList Class.
Date: 14th April, 2016
*/

package employee;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import employee_connection.EmployeeConnect;

public class EmployeeList extends Employee
{
	public ArrayList<Employee> fetchRecords() throws SQLException, Exception
	{
		ArrayList<Employee> list = new ArrayList<Employee>();
		
		//Fetch Records from Database
		//STEP 4: Execute a query
	   System.out.println("Creating statement...");
	   Connection conn = EmployeeConnect.getConnection();
	   Statement stmt = conn.createStatement();
	   String sql;
	   sql = "SELECT * FROM employee ORDER BY EmpID";
	   ResultSet rs = stmt.executeQuery(sql);

	   //STEP 5: Extract data from result set
	   while(rs.next())
	   {
	      //Retrieve by column name
	      String id  = rs.getString("EmpID");
	      String empname = rs.getString("Emp_Name");
	      String contact = rs.getString("Contact_Number");
	      String gender = rs.getString("Gender");
	      String dob = rs.getString("Date_Of_Birth");
	      Double base = rs.getDouble("Base_Salary");
	      
	      list.add(new Employee(id, empname, contact, gender, dob, base));
      
	   }
	   return list;
 
	}
	
	public void addRecord(Employee emp) throws SQLException, Exception
	{
		//STEP 4: Execute a query
		System.out.println("Creating statement...");
		Connection conn = EmployeeConnect.getConnection();
		Statement stmt = conn.createStatement();
		String sql;
		sql = "INSERT INTO employee VALUES(" + " '" + emp.getEmp_num()+ "' " + ", '" + emp.getEmp_name() + "', '" + emp.getContact_num() + "', '" + emp.getGender() + "', '" + emp.getDateofbirth() + "', '" + emp.getBaseSal() + "', '" + emp.getNetSal() + "')";
		stmt.execute(sql);
		
	}
	
	public void deleteRecord(String id) throws Exception
	{
		//STEP 4: Execute a query
		System.out.println("Creating statement...");
		Connection conn = EmployeeConnect.getConnection();
		Statement stmt = conn.createStatement();
		String sql;
		
		sql = "DELETE FROM employee WHERE EmpID = " + "'" + id + "'" ;
		   
		stmt.executeUpdate(sql);		
		   
	}
	
	public void updateRecord(String id, Employee emp) throws Exception
	{
		//STEP 4: Execute a query
		System.out.println("Creating statement...");
		Connection conn = EmployeeConnect.getConnection();
		Statement stmt = conn.createStatement();
		String sql;
		
		
		sql = "DELETE FROM employee WHERE EmpID = " + "'" + id + "'";
		stmt.executeUpdate(sql);

		
		sql = "INSERT INTO employee VALUES(" + " '" + emp.getEmp_num() + "' " + ", '" + emp.getEmp_name() + "', '" + emp.getContact_num() + "', '" + emp.getGender() + "', '" + emp.getDateofbirth() + "', '" + emp.getBaseSal() + "', '" + emp.getNetSal() + "')";
		stmt.executeUpdate(sql);
		
		
		   		
		
	}
}
