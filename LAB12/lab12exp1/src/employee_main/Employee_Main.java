/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 11 [EMPLOYEE] by using GUI, Event Handling and DataBase Storage.
FILE: Main Class.
Date: 14th April, 2016
*/

package employee_main;

import javax.swing.JFrame;

import employee_GUI.Employee_GUI;

public class Employee_Main {

	public static void main(String[] args) 
	{
		
		Employee_GUI myempgui = new Employee_GUI();
		
		myempgui.setLocationRelativeTo(null);
		myempgui.setVisible(true);
		myempgui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		

	}

}
