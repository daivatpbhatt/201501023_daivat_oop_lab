/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 11 [EMPLOYEE] by using GUI, Event Handling and DataBase Storage.
FILE: EmployeeActionListener Class.
Date: 14th April, 2016
*/

package employee_listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import employee.Employee;
import employee.EmployeeList;
import employee_GUI.*;

public class EmployeeActionListener implements ActionListener 
{
	private Employee_GUI empgui;
	private Modifyframe modify;
	
	
	public EmployeeActionListener(Employee_GUI emp, Modifyframe mod)
	{
		empgui= emp;
		modify=mod;
	}

	
	public void displayStudentRecords()
	{
		ArrayList<EmployeeDisplayPanel> EmpRecords = new ArrayList<EmployeeDisplayPanel>();
		EmpRecords.add(new EmployeeDisplayPanel("Employee ID", "Name", "Phone", "Gender", "Date of Birth", "Base Salary", "Net Salary"));

		try
		{
			EmployeeList list = new EmployeeList();
			
			ArrayList<Employee> records = list.fetchRecords();
			
			for(Employee emp: records)
				EmpRecords.add(new EmployeeDisplayPanel(emp.getEmp_num(), emp.getEmp_name(), emp.getContact_num(), emp.getGender(), emp.getDateofbirth(), Double.toString(emp.getBaseSal()), Double.toString(emp.getNetSal())));
			
	
				empgui.outputpanel.removeAll();
				
				for(EmployeeDisplayPanel r: EmpRecords)
					empgui.outputpanel.add(r);
			
				empgui.validate();
				this.empgui.repaint();
		
		
		}
		catch(SQLException err)
		{
			err.printStackTrace();
		}
		catch(Exception err)
		{
			err.printStackTrace();
		}
		

		
	}
	
	public void actionPerformed(ActionEvent event)
	{
		EmployeeList list = new EmployeeList();
		
		if(event.getActionCommand() == empgui.btnAdd.getText())
		{
			System.out.println("New Record");
			Employee newemp = new Employee(empgui.getID(), empgui.getName(), empgui.getContact(), empgui.getGender(), empgui.getDoB(), Double.parseDouble(empgui.getbasesal()));
			
			try
			{			
				
				list.addRecord(newemp);
				
				this.displayStudentRecords();
			}
			catch(SQLException err)
			{
				err.printStackTrace();
			}
			catch(Exception err)
			{
				err.printStackTrace();
			}
			
		}
		
		if(event.getActionCommand() == empgui.btndisp.getText())
		{
			this.displayStudentRecords();
		}
		
		if(event.getActionCommand() == empgui.btnDel.getText())
		{
			try
			{
				list.deleteRecord(empgui.getNumber());
				this.displayStudentRecords();
			}
			
			catch(Exception err)
			{
				err.printStackTrace();
			}
			
		}
		
		if(event.getActionCommand() == empgui.btnMod.getText())
		{
			modify.setVisible(true);
			
		}
		
		if(event.getActionCommand() == modify.modbtnsubmit.getText())
		{
			try
			{
				Employee newemployee = new Employee(modify.getmodID(), modify.modgetName(), modify.modgetContact(), modify.modgetGender(), modify.modgetDoB(), Double.parseDouble(modify.modgetbasesal()));
				

				list.updateRecord(empgui.getNumber(), newemployee);
				this.displayStudentRecords();
				
				modify.setVisible(false);
			}
			
			catch(Exception err)
			{
				err.printStackTrace();
			}
			
		}
		
		if(event.getActionCommand() == empgui.btnE.getText())
		{
			empgui.dispose();
		}
		
		
		
		empgui.clear();
	}
	
}
