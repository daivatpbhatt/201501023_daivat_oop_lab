/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 11 [EMPLOYEE] by using GUI, Event Handling and DataBase Storage.
FILE: Employee_GUI Class.
Date: 14th April, 2016
*/

package employee_GUI;

import java.awt.*;
import javax.swing.*;

import employee_listener.EmployeeActionListener;

@SuppressWarnings("serial")
public class Employee_GUI extends JFrame
{

	//Panels
	JPanel inputpanel = new JPanel();
	JPanel gendpan = new JPanel();
	JPanel btnpan = new JPanel();
	JPanel indexpan = new JPanel();
	JPanel temp = new JPanel();
	public JPanel outputpanel = new JPanel();
	
	//Controls of Input Panel
	JLabel lblName = new JLabel("Employee Name");
	JTextField txtName = new JTextField(10);	
	JLabel lblNum = new JLabel("Employee Number");
	JTextField txtNum = new JTextField(5);	
	JLabel lblGend = new JLabel("Gender");	
	JLabel lbldob = new JLabel("Date of Birth");
	JTextField txtdob = new JTextField(10);	
	JLabel lblcontact = new JLabel("Contact Number");
	JTextField txtcontact = new JTextField(10);	
	JLabel lblSal = new JLabel("Basic Salary");
	JTextField txtSal = new JTextField(10);	
	
	//Controls for gendpan Panel.
	ButtonGroup gend = new ButtonGroup();
	JRadioButton rbtnm = new JRadioButton("Male");
	JRadioButton rbtnf = new JRadioButton("Female");
	JRadioButton rbtno = new JRadioButton("Other");	
	

	//Controls for btnpan Panel.
	public JButton btnAdd = new JButton("Add");
	public JButton btnE = new JButton("Exit");
	public JButton btnMod = new JButton("Modify");
	public JButton btnDel = new JButton("Delete");
	public JButton btndisp = new JButton("Display");
	
	//Controls for indexpan Panel.
	JLabel lblfindn = new JLabel("Enter Employee Number");
	JTextField txtfindn = new JTextField(9);
	
	//Creating mod frame
	Modifyframe mod = new Modifyframe(this);

	
	
	public Employee_GUI()
	{
		
		//Setting Frame Display Details.
		this.setTitle("EMPLOYEE DETAILS");
		this.setSize(900, 650);
		this.setLayout(new GridLayout(0, 1));
		this.setBackground(Color.white);
		
		//Setting mod Frame Display Details.
		mod.setSize(450, 450);
		mod.setLocationRelativeTo(this);
		mod.setVisible(false);
				
		
		//Creating object of EmployeeActionListener class.
		EmployeeActionListener listen = new EmployeeActionListener(this, mod);
		
		//Adding Action Listener for each button.
		btnAdd.addActionListener(listen);
		btndisp.addActionListener(listen);
		btnDel.addActionListener(listen);
		btnMod.addActionListener(listen);
		btnE.addActionListener(listen);
		
		
		//Setting inputpanel Panel Display Details.
		inputpanel.setLayout(new GridLayout(0, 2));
		inputpanel.setSize(500, 500);
		inputpanel.setBackground(Color.white);	
		
		//Setting gendpan Panel Display Details.
		gendpan.setLayout(new GridLayout(0, 4));
		gendpan.setSize(200, 250);
		
		//Setting btnpan Panel Display Details.
		btnpan.setLayout(new FlowLayout(FlowLayout.CENTER));
		btnpan.setSize(10, 10);
		
		//Setting indexpan Panel Display Details.
		indexpan.setLayout(new GridLayout(0, 2));
		indexpan.setSize(20, 20);
		
		
		//Setting temp Panel Display Details.
		temp.setLayout(new GridLayout(0, 1));
		temp.setSize(25, 25);
		
		//Setting outputpanel Panel Display Details.
		outputpanel.setLayout(new GridLayout(0,1, 10, 10));
		outputpanel.setSize(1000, 300);
		outputpanel.setBackground(Color.WHITE);
		
		

		//Adding radio buttons to mod ButtonGroup.
		gend.add(rbtno);
		gend.add(rbtnm);
		gend.add(rbtnf);
		
		//Adding controls to inputpanel Panel
		inputpanel.add(lblNum);
		inputpanel.add(txtNum);
		inputpanel.add(lblName);
		inputpanel.add(txtName);
		inputpanel.add(new JLabel());
		inputpanel.add(new JLabel());
		inputpanel.add(lbldob);
		inputpanel.add(txtdob);
		inputpanel.add(lblcontact);
		inputpanel.add(txtcontact);
		
		inputpanel.add(new JLabel());
		inputpanel.add(new JLabel());
		
		inputpanel.add(lblSal);
		inputpanel.add(txtSal);

		inputpanel.add(new JLabel());
		inputpanel.add(new JLabel());
		
		//Adding controls to gendpan Panel.
		gendpan.add(lblGend);
		gendpan.add(rbtno);
		gendpan.add(rbtnm);
		gendpan.add(rbtnf);
		
		//Adding controls to btnpan Panel.
		btnpan.add(btnAdd);
		btnpan.add(btndisp);
		btnpan.add(btnE);
		btnpan.add(new JLabel());
		btnpan.add(new JLabel());
		
		//Adding controls to indexpan Panel.
		indexpan.add(new JLabel());
		indexpan.add(new JLabel());
		indexpan.add(lblfindn);
		indexpan.add(txtfindn);
		indexpan.add(btnDel);
		indexpan.add(btnMod);
		
		temp.add(btnpan);
		temp.add(indexpan);
		
		//Adding Panels to Frame.
		this.add(inputpanel, BorderLayout.NORTH);
		this.add(gendpan, BorderLayout.NORTH);
		//this.add(btnpan);
		//this.add(indexpan, BorderLayout.CENTER);
		this.add(temp);
		this.add(outputpanel, BorderLayout.SOUTH);

	}
	
	public String getID()
	{
		return this.txtNum.getText();
	}
	
	public String getName()
	{
		return this.txtName.getText();
	}
	
	public String getContact()
	{
		return this.txtcontact.getText();
	}
	
	public String getGender()
	{
		if(this.rbtnm.isSelected())
			return "Male";
		
		else if(this.rbtnf.isSelected())
			return "Female";
		
		else
			return "Other";
	}
	
	public String getDoB()
	{
		return this.txtdob.getText();
	}	
	
	public String getbasesal()
	{
		return this.txtSal.getText();
	}
	
	public String getNumber()
	{
		return this.txtfindn.getText();
	}
	
	public void clear()
	{
		this.txtcontact.setText("");
		this.txtdob.setText("");
		//this.txtfindn.setText("");
		this.txtName.setText("");
		this.txtNum.setText("");
		this.txtSal.setText("");
	}
	
	
	
}

