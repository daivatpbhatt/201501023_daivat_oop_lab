/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 11 [EMPLOYEE] by using GUI, Event Handling and DataBase Storage.
FILE: EmployeeDisplayPanel Class.
Date: 14th April, 2016
*/

package employee_GUI;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class EmployeeDisplayPanel extends JPanel
{
	private JLabel lblIdValue = new JLabel();
	private JLabel lblNameValue = new JLabel();
	private JLabel lblPhoneValue = new JLabel();
	private JLabel lblGender = new JLabel();
	private JLabel lbldob = new JLabel();
	private JLabel lblbase = new JLabel();
	private JLabel lblnet = new JLabel();
	
	
	public EmployeeDisplayPanel(String strId, String strName, String strPhone, String Gender, String dob, String base, String net)
	{
		lblIdValue.setText(strId);
		lblNameValue.setText(strName);
		lblPhoneValue.setText(strPhone);
		lblGender.setText(Gender);
		lbldob.setText(dob);
		lblbase.setText(base);
		lblnet.setText(net);
		
		this.setLayout(new GridLayout(1, 0, 2, 2));
		
		this.add(lblIdValue);
		this.add(lblNameValue);
		this.add(lblPhoneValue);
		this.add(lblGender);
		this.add(lbldob);
		this.add(lblbase);
		this.add(lblnet);
		
		
		
	}
	
	
}
