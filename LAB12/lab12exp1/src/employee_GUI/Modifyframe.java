/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 11 [EMPLOYEE] by using GUI, Event Handling and DataBase Storage.
FILE: Modifyframe Class.
Date: 14th April, 2016
*/

package employee_GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import employee_listener.EmployeeActionListener;

@SuppressWarnings("serial")
public class Modifyframe extends JFrame
{
	//Panel for Modify Frame
	JPanel modpanel = new JPanel();
	JPanel modgendpan = new JPanel();
	JPanel modbtnpan = new JPanel();
	
	//Controls for modpanel Panel
	JLabel modlblName = new JLabel("Employee Name");
	JTextField modtxtName = new JTextField(10);	
	JLabel modlblNum = new JLabel("Employee Number");
	JTextField modtxtNum = new JTextField(5);	
	JLabel modlblGend = new JLabel("Gender");	
	JLabel modlbldob = new JLabel("Date of Birth");
	JTextField modtxtdob = new JTextField(10);	
	JLabel modlblcontact = new JLabel("Contact Number");
	JTextField modtxtcontact = new JTextField(10);	
	JLabel modlblSal = new JLabel("Basic Salary");
	JTextField modtxtSal = new JTextField(10);	
	
	//Controls for modgendpan Panel
	ButtonGroup modgend = new ButtonGroup();
	JRadioButton modrbtnm = new JRadioButton("Male");
	JRadioButton modrbtnf = new JRadioButton("Female");
	JRadioButton modrbtno = new JRadioButton("Other");	
	
	//Controls for modbtnpan Panel
	public JButton modbtnsubmit = new JButton("SUBMIT");
		
	public Modifyframe(Employee_GUI emp)
	{
		//Setting modify Frame Display Details.
		this.setTitle("MODIFY");
		this.setSize(500, 500);
		this.setLayout(new BorderLayout(10, 10));
		this.setBackground(Color.white);
		
		//Setting modpanel Display Details.
		modpanel.setLayout(new GridLayout(0, 2));
		modpanel.setSize(450, 450);
		modpanel.setBackground(Color.white);
		
		//Setting modgendpan Display Details.
		modgendpan.setLayout(new GridLayout(0, 4));
		modgendpan.setSize(200, 250);
				
		//Setting modbtnpan Display Details
		modbtnpan.setLayout(new GridLayout(0, 1));
		
		
		EmployeeActionListener listen = new EmployeeActionListener(emp, this);
		modbtnsubmit.addActionListener(listen);
		
		//Adding radio buttons to gend ButtonGroup.
		modgend.add(modrbtno);
		modgend.add(modrbtnm);
		modgend.add(modrbtnf);
				
				
		//Adding controls to modpanel Panel
		modpanel.add(modlblNum);
		modpanel.add(modtxtNum);
		modpanel.add(modlblName);
		modpanel.add(modtxtName);
		modpanel.add(new JLabel());
		modpanel.add(new JLabel());
		modpanel.add(modlbldob);
		modpanel.add(modtxtdob);
		modpanel.add(modlblcontact);
		modpanel.add(modtxtcontact);
		
		modpanel.add(new JLabel());
		modpanel.add(new JLabel());
		
		modpanel.add(modlblSal);
		modpanel.add(modtxtSal);

		modpanel.add(new JLabel());
		modpanel.add(new JLabel());
		
		//Adding controls to modgendpan Panel
		modgendpan.add(modlblGend);
		modgendpan.add(modrbtno);
		modgendpan.add(modrbtnm);
		modgendpan.add(modrbtnf);
		
		//Adding controls to modbtnpan Panel
		modbtnpan.add(modbtnsubmit);
		
		
		//Adding Panels to modify Frame
		this.add(modpanel, BorderLayout.NORTH);
		this.add(modgendpan, BorderLayout.CENTER);
		this.add(modbtnpan, BorderLayout.SOUTH);
				
	}			
		
	public String getmodID()
	{
		return this.modtxtNum.getText();
	}
	
	public String modgetName()
	{
		return this.modtxtName.getText();
	}
	
	public String modgetContact()
	{
		return this.modtxtcontact.getText();
	}
	
	public String modgetGender()
	{
		if(this.modrbtnm.isSelected())
			return "Male";
		
		else if(this.modrbtnf.isSelected())
			return "Female";
		
		else
			return "Other";
	}
	
	public String modgetDoB()
	{
		return this.modtxtdob.getText();
	}	
	
	public String modgetbasesal()
	{
		return this.modtxtSal.getText();
	}
}

