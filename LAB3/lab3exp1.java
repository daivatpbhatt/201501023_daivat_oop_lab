/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Create a linked list to store polynomials and perform functions.
Date: 29th January, 2016
*/


package Lab3exp1;
import java.util.Scanner;

class LinkedList 
{
	
	private Float data = null;
	private LinkedList next = null;

	public void add(Float newDetails)                              //Function to Append
	{ 
		if(data == null)
			this.data = newDetails;
		else
		{
			LinkedList curr;
			for(curr = this; curr.next!=null; curr = curr.next);

			curr.next = new LinkedList();
			curr = curr.next;
			curr.data = newDetails;
			curr.next = null;
		}
	}
	
	public void display()                                       //Function to Display the polynomial
	{
		if(this.data == null)
			System.out.println("No polynomial stored.");
		else
		{
			LinkedList curr;
			int i;
			for(curr = this, i=0; curr!=null; curr = curr.next,i++)
			{
				if(curr.data.floatValue()!=0)
				{
					System.out.print("("+curr.data.floatValue()+")x^"+i);
					
					if(curr.next!=null) 
					{
						System.out.print("+");
					}
				}
                                
                                else if(curr.data.floatValue()==0)
                                {
                                    System.out.println("0");
                                    break;                                    
                                }
				
			}
		}
	}
	
	public Float getElement(int i)                                     //Function to get a particular element in linkedlist
	{
		LinkedList curr;
		int cnt;
		for(curr = this,cnt=0; curr!=null && cnt<i; curr = curr.next, cnt++);
		
		return curr.data;
	}
	
	public void clear()
	{
		LinkedList curr;
		for(curr = this;curr!=null;)
		{
			LinkedList n=curr.next;
			curr.data=null;
			curr.next=null;
			curr=n;
		}
	}
}



public class lab3exp1 {
	

	public static void main(String[] args)
	{
		Scanner in  = new Scanner(System.in);
		
		LinkedList poly1 = new LinkedList();                //Polynomial 1
		LinkedList poly2 = new LinkedList();                //Polynomial 2
		LinkedList result = new LinkedList();               //Answer
		
		String senter;
		
		int temp1=0, temp2=0;
		int choice;
		while(true)
		{
			System.out.println("\n\n1. Enter Polynomials.");
                        System.out.println("2. Add Polynomials.");
                        System.out.println("3. Subtract Polynomials.");
                        System.out.println("4. Multiply two polynomials.");
                        System.out.println("5. Multiply a polynomial with a scalar.");
                        System.out.println("0. Exit.\n\n");
			choice = in.nextInt();
			switch(choice)
			{
				case 1:
					
					poly1.clear();
					poly2.clear();
					System.out.println("Press ENTER twice to exit.");
					System.out.println("\nEnter Polynomial 1: ");
					
					
					for(temp1=0;true;temp1++)
					{
						System.out.println("Enter coefficient of x^"+temp1+": ");
						if(temp1==0) 
                                                {
                                                    in.nextLine();
						}
						senter=in.nextLine();
						
						if(!senter.isEmpty()) 
						{
                                                    poly1.add(Float.parseFloat(senter));                 //Converting to Float
						}
						
						else                    //Breaking loop when blank enter is pressed
						{
                                                    System.out.print("Polynomial 1: ");
                                                    poly1.display();
                                                    break;
						}	
					}
					
					System.out.println("\n\nEnter Polynomial 2: ");
					
					for(temp2=0;true;temp2++)
					{
						System.out.println("Enter coefficient of x^"+temp2+": ");

						senter=in.nextLine();
						
						if(!senter.isEmpty()) 
						{
							poly2.add(Float.parseFloat(senter));
						}
						
						else 
						{
							System.out.print("Polynomial 2: ");
							poly2.display();
							break;
						}	
					}
					
					//Setting the size of both lists.
					if (temp1<temp2)
					{
						for(;temp1!=temp2;temp1++)
						{
							poly1.add((float)0);
						}
					}
					
					else if(temp1>temp2)
					{
						for(;temp1!=temp2;temp2++)
						{
							poly2.add((float)0);
						}
					}
					
					break;
					

				
				case 2:                            //Adding
					int cnt=0;
					for(;cnt!=temp1;cnt++)
					{
						result.add(poly2.getElement(cnt).floatValue()+poly1.getElement(cnt).floatValue());
					}
					
					System.out.print("The addition is: ");
					result.display();
					result.clear();
					break;
					

				
				case 3:                             //Subtracting
					
					System.out.println("\n1. Poly2-Poly1\n2. Poly1-Poly2");
					int ch=in.nextInt();
					
					if(ch==1)
					{
						cnt=0;
						for(;cnt!=temp1;cnt++)
						{
							result.add(poly2.getElement(cnt).floatValue()-poly1.getElement(cnt).floatValue());
						}
						
						System.out.print("Poly2 - Poly1:  ");
						result.display();
						result.clear();
					}
					
					else if(ch==2)
					{
						cnt=0;
						for(;cnt!=temp1;cnt++)
						{
							result.add(poly1.getElement(cnt).floatValue()-poly2.getElement(cnt).floatValue());
						}
						
						System.out.print("Poly1 - Poly2: ");
						result.display();
						result.clear();
					}
					
					else
					{
						System.out.println("Enter valid choice!");
					}
					break;


					
				case 4:
					
					float temp=0;
					
					for(int l=0;l!=temp1+temp2;l++)
					{
						temp=0;
						if(l<temp1)
						{
							for(int m=l;m>=0;m--)
							{
								temp=temp + (poly1.getElement(l-m).floatValue() * poly2.getElement(m).floatValue());
							}
						}
						
						if(l>=temp1)
						{
							for(int m=temp1-1;m>(l-temp1);m--)
							{
								temp=temp + (poly1.getElement(l-m).floatValue() * poly2.getElement(m).floatValue());
							}
						}
						result.add(temp);
					}
					
					result.display();
					result.clear();
					break;
					
					
					
				case 5:
					
					float cnst;
					
					System.out.println("Enter contsant: ");
					cnst=in.nextFloat();
					
					System.out.println("\n1. Multiply with p\n2. Multiply with q: ");
					ch=in.nextInt();
					
					if(ch==1)
					{
						cnt=0;
						for(;cnt!=temp1;cnt++)
						{
							
                                                    result.add(cnst*poly1.getElement(cnt).floatValue());
						}
						result.display();
						result.clear();
					}
					
					else if(ch==2)
					{
						cnt=0;
						for(;cnt!=temp1;cnt++)
						{
							result.add(cnst*poly2.getElement(cnt).floatValue());
						}
						System.out.println();
						result.display();
						result.clear();
					}
					
					else
					{
						System.out.println("Enter valid choice!");
					}

					break;


					
				case 0:
					return ;
				default : System.out.println("Invalid Entry!");
			}
		}
	}
}
