package Lab9exp1;

import java.util.*;

class LinkedList 
{
	
	private Float data = null;
	private LinkedList next = null;

	public void add(Float newDetails)                                   //Function to Append
	{
            try
            {
                if(this==null)
                    throw new NullPointerException("\nNo LinkedList Created!");
            }

            catch(NullPointerException err)
            {
                System.out.println("\nException Occured!"+err);
            }

            if(data == null)
                    this.data = newDetails;
            else
            {
                    LinkedList curr;
                    for(curr = this; curr.next!=null; curr = curr.next);

                    curr.next = new LinkedList();
                    curr = curr.next;
                    curr.data = newDetails;
                    curr.next = null;
            }
	}
	
	public void display()                                       //Function to Display the polynomial
	{
            
            try
            {
                if(this==null)
                    throw new NullPointerException("\nNo LinkedList Created!");
            }

            catch(NullPointerException err)
            {
                System.out.println("\nException Occured!"+err);
            }
            
            if(this.data == null)
                    System.out.println("No polynomial stored.");
            else
            {
                    LinkedList curr;
                    int i;
                    for(curr = this, i=0; curr!=null; curr = curr.next,i++)
                    {
                            if(curr.data.floatValue()!=0)
                            {
                                    System.out.print("("+curr.data.floatValue()+")x^"+i);

                                    if(curr.next!=null) 
                                    {
                                            System.out.print("+");
                                    }
                            }

                            else if(curr.data.floatValue()==0)
                            {
                                System.out.println("0");
                                break;                                    
                            }

                    }
            }
	}
	
	public Float getElement(int i)                                     //Function to get a particular element in linkedlist
	{
            try
            {
                if(this==null)
                    throw new NullPointerException("\nNo LinkedList Created!");
            }

            catch(NullPointerException err)
            {
                System.out.println("\nException Occured!"+err);
            }
            
            LinkedList curr;
            int cnt;
            for(curr = this,cnt=0; curr!=null && cnt<i; curr = curr.next, cnt++);

            return curr.data;
	}
	
	public void clear()
	{
            try
            {
                if(this==null)
                    throw new NullPointerException("\nNo LinkedList Created!");
            }

            catch(NullPointerException err)
            {
                System.out.println("\nException Occured!"+err);
            }
            
            LinkedList curr;
            for(curr = this;curr!=null;)
            {
                    LinkedList n=curr.next;
                    curr.data=null;
                    curr.next=null;
                    curr=n;
            }
	}
}
