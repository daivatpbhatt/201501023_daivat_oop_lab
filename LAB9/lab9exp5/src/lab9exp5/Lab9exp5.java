/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 6 [BANKING] by using exception handling.
Date: 24th March, 2016
*/


package lab9exp5;

import java.util.Scanner;

class NullPointer extends Exception
{
    NullPointer(String s)
    {
        super(s);        
    }
            
}

class Withdraw extends Exception
{
    String desc;
    double balamt;
    
    Withdraw(String s, double amt)
    {
        super(s);
        balamt=amt;
    }
}


public class Lab9exp5 
{
        
    public static void main(String args[]) 
    {
        Scanner in = new Scanner(System.in);
        byte ch, choice, acctype=0;
        String accnum, checkacc;
        double balance, odl;
        float roi;
        int[] date= new int[3];
        int[] today= new int[3];
        
        
        //Reference of Account class.
        Account Curr=null;
        Account Sav=null;
        
        System.out.println("1. Savings.");
        System.out.println("2. Current.");
        acctype = in.nextByte();
        
        System.out.println("\nEnter Date on which account was created: ");
        date[0]= in.nextInt();
        System.out.println("Enter Month on which account was created: ");
        date[1]= in.nextInt();
        System.out.println("Enter Year on which account was created: ");
        date[2]= in.nextInt();

        if(acctype==1)
        {
            System.out.println("\n\nEnter account number: ");
            accnum = in.next();
            System.out.println("Enter balance: ");
            balance = in.nextDouble();
            if(balance<500)
            {
                System.out.println("Minimum required balance is Rs. 500");
              
            }
            else
            {
                System.out.println("Enter rate of interest(Per Annum): ");
                roi = in.nextFloat();            

                Sav = new Savings(accnum, balance, roi);                        //Creating object and setting account number, balance and rate of interest.
                Sav.setDate(date);                              //Calling Member function to set date of account creation.
             
            }
            
        }

        else if(acctype==2)
        {
            System.out.println("\nEnter account number: ");
            accnum = in.next();
            System.out.println("Enter balance: ");
            balance = in.nextDouble();

            odl=balance*3;

            Curr=new Current(accnum, balance, odl);                        //Creating object and setting account number, balance and overdraft limit.
            Curr.setDate(date);                              //Calling Member function to set date of account creation.
        }
        
        if(Sav!=null)
        {
            System.out.println("\nEnter today's date: ");
            today[0]=in.nextInt();
            System.out.println("Enter today's month: ");
            today[1]=in.nextInt();
            System.out.println("Enter today's year: ");
            today[2]=in.nextInt();

            if(Sav!=null)
                ((Savings)Sav).checkdate(today);                              //Calling Member function to compare dates and change balance according to rate of interest.
        }
        
        
        
        
        while(true)
        {
            System.out.println("\n1. Display Account Details.");
            System.out.println("2. Transactions.");
            System.out.println("3. Delete an account.");
            System.out.println("4. Change date!");
            System.out.println("0. Exit");
            ch=in.nextByte();
            
            
            switch(ch)
            {

                case 1:
                {
                    if(Sav!=null && Sav.bal>500)
                    {
                        Sav.display();                              //Calling Member function to display details of Savings Account.
                    }
                    
                    else if(Sav!=null && Sav.bal<500)
                        System.out.println("The Savings Account couldn't be created. Create again!");
                    

                    else if(Curr!=null)
                    {
                        Curr.display();                              //Calling Member function to display details of Current Account.
                    }
                    
                    else
                        System.out.println("No Account created!!");


                }

                break;

                case 2:
                {
                    System.out.println("\n\nEnter Account Number to make Transactions: ");
                    checkacc = in.next();

                    if(Sav!=null && Sav.AccNum.equals(checkacc))                              //Calling Member function to check the validity of Account Number.
                    {
                        Sav.display();                                  //Calling Member function to display details of Savings Account.
                    }
                    
                    else if(Curr!=null && Curr.AccNum.equals(checkacc))                              //Calling Member function to check the validity of Account Number.
                    {
                        Curr.display();                                  //Calling Member function to display details of Current Account.
                    }
                    
                    else
                    {
                        System.out.println("\nEnter valid Account Number!");
                        break;                        
                    }
                        

                    System.out.println("\n1. Check Balance");
                    System.out.println("2. Deposit.");
                    System.out.println("3. Withdraw.");
                    System.out.println("\nEnter your choice: ");
                    choice = in.nextByte();

                    if(Sav!=null)
                    {  
                        if(choice==1)       //Check Balance.
                        {
                            if(Sav!=null)
                            {
                                System.out.println("\nBalance is: "+Sav.getBal());             //Calling Member function to display Savings Account balance.
                            }
                            
                            else
                                System.out.println("\nNo Savings account created!");
                        }

                        else if(choice==2)      //Deposit.
                        {
                            System.out.println("Enter amount to be deposited: ");

                             if(Sav!=null)
                            {
                                if(Sav.deposit(in.nextDouble()))                              //Calling Member function to deposit money into Savings Account.
                                System.out.println("\nAmount SUCCESSFULLY deposited!");

                                else
                                    System.out.println("\nAmount UNSUCCESSFULLY deposited!\nEnter valid amount!");
                            }

                        }

                        else if(choice==3)          //Withdraw.
                        {
                            System.out.println("Enter amount to be withdrawn: ");

                            if(Sav!=null)
                            {
                                try
                                {
                                    Sav.withdraw(in.nextDouble());
                                    System.out.println("\nAmount Successfully Withdrawn");
                                }
                                
                                catch(Withdraw err)
                                {
                                    System.out.println("\n\nException Occured!"+err+"\nAmount entered: "+err.balamt);
                                }

                                Sav.display();
                            }
                        }
                        
                    }
                    
                    else if(Curr!=null)
                    {
                        if(choice==1)           //Check Balance.
                        {
                            if(Curr!=null)
                                System.out.println("Balance is: "+Curr.getBal());                   //Calling Member function to display Current Account Balance.
                            
                            else
                                System.out.println("\nNo Current Account created!");
                        }
                        
                        else if(choice==2)      //Deposit.
                        {
                            if(Curr!=null)
                            {
                                if(Curr.deposit(in.nextDouble()))                              //Calling Member function to deposit money into Current Account.
                                System.out.println("\nAmount SUCCESSFULLY deposited!");

                                else
                                    System.out.println("\nAmount UNSUCCESSFULLY deposited!\nEnter valid amount!");
                            }
                            
                            else
                                System.out.println("\nNo Current Account created!");
                            
                        }
                        
                        else if(choice==3)                  //Withdraw.
                        {
                            if(Curr!=null)
                            {
                                try
                                {
                                    Curr.withdraw(in.nextDouble());
                                    System.out.println("\nAmount Successfully Withdrawn");
                                }
                                
                                catch(Withdraw err)
                                {
                                    System.out.println("\n\nException Occured!"+err+"\nAmount entered: "+err.balamt);
                                }

                                Curr.display();                                  //Calling Member function to display details of Current Account.
                            }
                            
                            else
                                System.out.println("\nNo Current Account created!");
                        }
                        
                    }
                    
                }
                
                break;
                
                case 3:
                {
                    System.out.println("\n\nEnter account number to delete: ");
                    checkacc = in.next();

                    if(Sav!=null && Sav.AccNum.equals(checkacc))                              //Calling Member function to check the validity of Account Number.
                    {
                        System.out.println("Your account is: \n\n");
                        Sav.display();                                  //Calling Member function to display details of Savings Account.
                        Sav=null;
                        System.out.println("\nAccount deleted successfully!");                        
                    }
                    
                    else if(Curr!=null && Curr.AccNum.equals(checkacc))                              //Calling Member function to check the validity of Account Number.
                    {
                        System.out.println("Your account is: \n\n");
                        Curr.display();                                         //Calling Member function to display details of Savings Account.
                        Curr=null;
                        System.out.println("\nAccount deleted successfully!");
                    }
                    
                    else
                    {
                        System.out.println("Enter Valid Account Number!");
                        break;                        
                    }
                    
                }
                break;
                
                case 4:
                {
                    System.out.println("\nEnter today's date: ");
                    today[0]=in.nextInt();
                    System.out.println("Enter today's month: ");
                    today[1]=in.nextInt();
                    System.out.println("Enter today's year: ");
                    today[2]=in.nextInt();
                    
                    if(Sav!=null)
                        ((Savings)Sav).checkdate(today);                              //Calling Member function to change the date to calculate transactions.
                    
                }
                
                break;
                
                case 0: return;                 //Exit.

            }
        }
        
    }
    
}
