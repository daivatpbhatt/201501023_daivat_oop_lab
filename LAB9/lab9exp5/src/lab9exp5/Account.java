/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Modify Question 1 of Assignment 6 [BANKING] by using exception handling.
Date: 24th March, 2016
*/


package lab9exp5;

import java.util.*;

abstract class Account                              //Abstract Class Account(Parent Class)
{
    //Data members
    protected String AccNum;
    protected double bal;
    public String type;
    public int[] date = new int[3];
    
    
    Account(String acc, double amt)                 //Constructor to set account number and balance.
    {
        AccNum=acc;
        bal=amt;
    }
    
    public double getBal()                          //Member function to get Balance
    {
        return bal;
    }

    
    public void setDate(int[] setdate)                          //Member function to set the date on which account was created.
    {
        date[0]=setdate[0];
        date[1]=setdate[1];
        date[2]=setdate[2];        
    }
    
    
    
    public boolean deposit(double amt)                          //Member function to deposit money into account.
    {
        if(amt>0)
        {
            bal+=amt;
            return true;
        }
        
        else            
            return false;
    }
    
    public abstract void display();                          //Abstract Member function to display account details.
    
    public abstract void withdraw(double amt) throws Withdraw;                          //Member function to withdraw money from the Account.
    
}



class Savings extends Account                               //Child Class of Class Account.
{
    private float Rate;
    
    public Savings(String accnum, double bal, float roi)                //Constructor to set account number, balance and rate of interest.
    {
        super(accnum, bal);
        Rate = roi;
        type = "Savings";
    }

    
    public void checkdate(int[] today)                          //Member function to check and compare today's date with the date of Account Creation.
    {
        int dt, mt, yr;
        int i;
        double temp=this.bal;
        
        dt=Math.abs(today[0]-date[0]);
                
        mt=today[1]-date[1];
        yr=today[2]-date[2];
        
        if(yr>=1)
        {
            for(i=0;i<yr;i++)
                    temp+=temp*this.Rate/100;
            if(mt>=3)
            {
                //if(dt==0)
                {
                    for(i=0;i<(mt/3);i++)
                        temp+=temp*this.Rate/400;
                }
                
            }
        }
        
        else
        {
            for(i=0;i<(mt/3);i++)
                temp+=temp*this.Rate/400;
        }
        
        this.bal=temp;       
        
    }
    
    
    public void display()                          //Member function to display account details.
    {
        try
        {
            if(this==null)
                throw new NullPointer("\nNo Account created!");
        }
        
        catch(NullPointer err)
        {
            System.out.println("\nException occured!"+err);
        }
            
        
        int i;
        System.out.println("\n*************ACCOUNT DETAILS*************");
        System.out.println("Account Number: " +AccNum);
        System.out.println("Account Balance: " +bal);
        System.out.println("Rate of Interest: "+Rate);
        System.out.print("Date of Creation: [");
        for(i=0;i<3;i++)
        {
            if(i<2)
                System.out.print(""+date[i]+"-");
            else
                System.out.print(""+date[i]+"]\n\n");
        }
        
        System.out.println("Account type: Savings.\n\n");
       
    }
    
    public void withdraw(double amount) throws Withdraw                          //Member function to withdraw from Savings Account.
    {
        try
        {
            if(amount<bal && amount>0)
            {
                bal-=amount;
            }
            
            else if(amount>bal)
                throw new Withdraw("\nCannot Withdraw Amount greater than Account Balance.", amount);
            
            else if(amount<0)
                throw new Withdraw("\nCannot Withdraw negative Amount.", amount);
            
        }
       
        catch(Withdraw err)
        {
            throw err;
        }
          
    }
    
}


class Current extends Account                           //Child Class of Class Account.
{
    private double overlim;
    
    public Current(String accnum, double amount, double odl)                    //Constructor to set account number, balance and overdraft limit.
    {
        super(accnum, amount);
        overlim=odl;
        type = "Current";
    }
    
    
    @Override
    public void display()                          //Member function to display account details.
    {
        int i;
        System.out.println("\n*************ACCOUNT DETAILS*************");
        System.out.println("Account Number: " +AccNum);
        System.out.println("Account Balance: " +bal);
        overlim=Math.abs(bal*3);
        System.out.println("OverDraft Limit: " +overlim+"");
        System.out.print("Date of Creation: [");
        for(i=0;i<3;i++)
        {
            if(i<2)
                System.out.print(""+date[i]+"-");
            else
                System.out.print(""+date[i]+"]\n\n");
        }
        
        System.out.println("Account type: Curernt\n\n");
                
    }
    
    public void withdraw(double amount) throws Withdraw                          //Member function to withdraw from Current Account.
    {
        try
        {
            if(amount<bal && amount>0)
            {
                bal-=amount;
                overlim = bal*3;
            }
            
            else if(amount>bal)
                throw new Withdraw("\nCannot Withdraw Amount greater than Account Balance.", amount);
            
            else if(amount<0)
                throw new Withdraw("\nCannot Withdraw negative Amount.", amount);
            
        }
       
        catch(Withdraw err)
        {
            throw err;
        }        
    }
    
    public boolean checkbal(double amt)                          //Member function to check balance of Current Account.
    {
        if(bal+amt>(bal+overlim))
            return false;
        
        else
            return true;
    }
}