/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Implement the Queue and CircularQueue data structure.
Date: 23rd February, 2016
*/

package lab6exp2;
import java.util.*;


class Item
{
    private String name, ID_code;
    protected int price;
    
    Item()
    {
        name=null;
        ID_code=null;
        price=0;
        
    }
    
    Item(String n, int p, String ID)
    {
        name = n;
        price = p;
        ID_code = ID;
    }
    
    public void display()
    {
        System.out.println("\nItem name: "+name);
        System.out.println("Item code: "+ID_code);
        System.out.println("Item price: "+price+"\n");
    }
    
}

class Queue
{
    public int size, front, end;
    static int i=0;
    Item[] queue;
    
    Queue(int s)
    {
        size = s;
        front =0;
        end = -1; 
        queue = new Item[size];
        
        for(i=0;i<size;i++)
        {
            queue[i]=new Item();
        }
    }
    
    public void EnQueue(Item I)
    { 
        if(i<size)
        {           
            queue[i]= I;
            
            end =i;
            i++;
        }
        
    }
    
    public void DisplayQueue()
    {
        int j;
        if(front<size)
        {
            System.out.println("\nThe queue is: ");
            for(j=front;j<size && queue[j]!=null;j++)
                queue[j].display();        
        }
        
        else
            System.out.println("The Queue is now Empty as well as Full!");
        
    }
    
    public boolean checkempty()
    {
        if(i<size)
            return true;
        
        else 
            return false;
    }
    
    public void DeQueue()
    {
        if(front<=end)
        {
            System.out.println("The first element is: ");
            queue[front].display();
            
            queue[front]=null;
            front++;
            
        }
        
        else
            System.out.println("The Queue is full as well as empty. LOL!");
        
    }
    
}

class CircularQueue extends Queue
{
    private int cnt;
           
    public CircularQueue(int size)
    {
        super(size);
        this.size=size;
        front=0;
        end=-1;
    }
    
    
    public boolean checkforfull()
    {
        if(cnt==size)
            return false;
        
        else
            return true;
    }
    
    
    @Override
    public boolean checkempty()
    {
        if(cnt==0)
            return true;
        
        else
            return false;
    }
        
    
    public void CEnQueue(Item I)
    {
        Scanner input=new Scanner(System.in);
        
		if(end<size-1)
		{
			System.out.println("\n\n"+end+"\n\n");
			end++;
                        System.out.println("\n\n"+end+"\n\n");
			if(queue[end].price==0)
			{
				System.out.println("Enter the Details of the Item");
				queue[end]=I;
                                queue[end].display();
			}
                        System.out.println("\n\n"+end+"\n\n");
		}
                
		else if(end==size-1)
		{
			end=0;
			if(queue[end].price==0)
			{
				System.out.println("enter the details of the item");
				queue[end]=I;
                                queue[end].display();
                                
			}
		}
		else 
		{
			end=-1;
			System.out.println("The Queue is Full.");
			
		}
                
                queue[end].display();
        
    }
    
    public void CDisplayQueue()
    {
        int k=front,j=end;
        System.out.println("\n\n"+end+"\n\n");
        
	if(k<j)
	{
            System.out.println("The Queue is:");
            for(;k<=j;k++)
            queue[k].display();
	}
        
	else if(k>=j)
	{
            System.out.println("The queue is:");
            for(j=0;j<size;j++)
            {
		queue[j].display();
            }
        }
        
    }
    
   
    

    public Item CDeQueue()
    {
        System.out.println(""+front);
        System.out.println(end + "\n");
        Item temp=null;
        
        if(this.checkempty())
            System.out.println("Queue is now empty!");
        
        else
        {
            temp=queue[front];
            queue[front]=null;
            front=(front+1)%size;
            cnt--;
        }
        
        return temp;
        
        
    }
    
    public int elements()
    {
        return cnt;
    }
}

public class Lab6exp2 
{

    public static void main(String[] args) 
    {
        int size;
        int i=0, p, cnt=0;
        byte ch1, ch2;
        String n, ID;
        
        Scanner in = new Scanner(System.in);
        
        System.out.println("Enter the size of the queue: ");
        size = in.nextInt();
        
        
        
        System.out.println("\n1. Linear Queue.");
        System.out.println("2. Circular Queue.");
        System.out.println("0. Exit");
        System.out.print("Enter your choice: ");
        ch1=in.nextByte();
        
        while(true)
        {
            switch(ch1)
            {
                case 1:
                {
                    Queue q = new Queue(size);
                    System.out.println("\n1. EnQueue.");
                    System.out.println("2. Display Queue.");
                    System.out.println("3. DeQueue");
                    System.out.println("0. Exit.");
                    System.out.print("Enter your choice: ");
                    ch2=in.nextByte();

                    switch(ch2)
                    {
                        case 1:
                        {
                            if(q.checkempty())
                            {
                                System.out.println("\nEnter item details: ");
                                System.out.println("\nName: ");
                                n=in.next();
                                System.out.println("ID Code: ");
                                ID=in.next();
                                System.out.println("Price: ");
                                p=in.nextInt();
                                Item item=new Item(n, p, ID);
                                System.out.println("\nItem details entered are: \n");
                                item.display();


                                q.EnQueue(item);
                            }
                            
                            else
                                System.out.println("\n\nQueue is full!\n");

                        }
                        
                        break;

                        case 2:
                        {
                            q.DisplayQueue();
                        }
                        
                        //break;
                        
                        case 3:
                        {
                            q.DeQueue();
                            q.DisplayQueue();
                        }
                        
                        break;
                        
                        case 0: return;
                    }

                }
                
                break;
                
                case 2:
                {
                    CircularQueue cq= new CircularQueue(size);
                    System.out.println("\n1. EnQueue.");
                    System.out.println("2. Display Queue.");
                    System.out.println("3. DeQueue");
                    System.out.println("0. Exit.");
                    System.out.print("Enter your choice: ");
                    ch2=in.nextByte();
                    
                    switch(ch2)
                    {
                        case 1:
                        {
                            if(cq.checkforfull())
                            {
                                System.out.println("\nEnter item details: ");
                                System.out.println("\nName: ");
                                n=in.next();
                                System.out.println("ID Code: ");
                                ID=in.next();
                                System.out.println("Price: ");
                                p=in.nextInt();
                                Item item=new Item(n, p, ID);
                                System.out.println("\nItem details entered are: \n");
                                item.display();


                                cq.CEnQueue(item);
                                
                                
                            }
                            
                            else
                                System.out.println("The queue is full! DeQueue to enter elements!");
                                
                        }
                        
                        break;
                        
                        case 2:
                        {
                            cq.CDisplayQueue();
                            //System.out.println("\nTotal elements in queue are: "+cq.elements());
                        }
                        
                        break;
                        
                        case 3:
                        {
                            Item temp=null;
                            temp=cq.CDeQueue();
                            System.out.println("The removed item is: ");
                            temp.display();
                        }
                        
                        break;
                        case 0: return;
                            
                            
                    }

                    
                    
                }
                
                break;
                    
                
                case 0: return;
            }
        }
        
    }
    
}
