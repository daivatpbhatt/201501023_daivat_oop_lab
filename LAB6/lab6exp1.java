/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Create a mini-banking system with appropriate Savings and Current classes.
Date: 22nd February, 2016
*/

package lab6exp1;

import java.util.*;

abstract class Account                              //Abstract Class Account(Parent Class)
{
    //Data members
    protected String AccNum;
    protected double bal;
    public String type;
    public int[] date = new int[3];
    
    
    Account(String acc, double amt)                 //Constructor to set account number and balance.
    {
        AccNum=acc;
        bal=amt;
    }
    
    public double getBal()                          //Member function to get Balance
    {
        return bal;
    }

    
    public void setDate(int[] setdate)                          //Member function to set the date on which account was created.
    {
        date[0]=setdate[0];
        date[1]=setdate[1];
        date[2]=setdate[2];        
    }
    
    
    
    public boolean deposit(double amt)                          //Member function to deposit money into account.
    {
        if(amt>0)
        {
            bal+=amt;
            return true;
        }
        
        else            
            return false;
    }
    
    public abstract void display();                          //Abstract Member function to display account details.
    
    public abstract boolean withdraw(double amt);                          //Member function to withdraw money from the Account.
    
    public abstract boolean checkbal(double amount);                          //Member function to check Balance of the Account.
 
}



class Savings extends Account                               //Child Class of Class Account.
{
    private float Rate;
    
    public Savings(String accnum, double bal, float roi)                //Constructor to set account number, balance and rate of interest.
    {
        super(accnum, bal);
        Rate = roi;
        type = "Savings";
    }

    
    public void checkdate(int[] today)                          //Member function to check and compare today's date with the date of Account Creation.
    {
        int dt, mt, yr;
        int i;
        double temp=this.bal;
        
        dt=Math.abs(today[0]-date[0]);
                
        mt=today[1]-date[1];
        yr=today[2]-date[2];
        
        if(yr>=1)
        {
            for(i=0;i<yr;i++)
                    temp+=temp*this.Rate/100;
            if(mt>=3)
            {
                //if(dt==0)
                {
                    for(i=0;i<(mt/3);i++)
                        temp+=temp*this.Rate/400;
                }
                
            }
        }
        
        else
        {
            for(i=0;i<(mt/3);i++)
                temp+=temp*this.Rate/400;
        }
        
        this.bal=temp;       
        
    }
    
    
    public void display()                          //Member function to display account details.
    {
        int i;
        System.out.println("\n*************ACCOUNT DETAILS*************");
        System.out.println("Account Number: " +AccNum);
        System.out.println("Account Balance: " +bal);
        System.out.println("Rate of Interest: "+Rate);
        System.out.print("Date of Creation: [");
        for(i=0;i<3;i++)
        {
            if(i<2)
                System.out.print(""+date[i]+"-");
            else
                System.out.print(""+date[i]+"]\n\n");
        }
        
        System.out.println("Account type: Savings.\n\n");
       
    }
    
    public boolean checkbal(double amount)                          //Member function to check balance of Savings Account.
    {
        if(bal-amount<500)
            return false;
        
        else
            return true;
    }
    
    public boolean withdraw(double amount)                          //Member function to withdraw from Savings Account.
    {
        if(this.checkbal(amount))
        {
            if(amount<bal && amount>0)
            {
                bal-=amount;
                return true;
            }
            
            else
                return false;
            
        }
       
        else
            return false;
          
    }
    
}


class Current extends Account                           //Child Class of Class Account.
{
    private double overlim;
    
    public Current(String accnum, double amount, double odl)                    //Constructor to set account number, balance and overdraft limit.
    {
        super(accnum, amount);
        overlim=odl;
        type = "Current";
    }
    
    
    @Override
    public void display()                          //Member function to display account details.
    {
        int i;
        System.out.println("\n*************ACCOUNT DETAILS*************");
        System.out.println("Account Number: " +AccNum);
        System.out.println("Account Balance: " +bal);
        overlim=Math.abs(bal*3);
        System.out.println("OverDraft Limit: " +overlim+"");
        System.out.print("Date of Creation: [");
        for(i=0;i<3;i++)
        {
            if(i<2)
                System.out.print(""+date[i]+"-");
            else
                System.out.print(""+date[i]+"]\n\n");
        }
        
        System.out.println("Account type: Curernt\n\n");
                
    }
    
    public boolean withdraw(double amt)                          //Member function to withdraw from Current Account.
    {
        if(this.checkbal(amt))
        {
            bal-=amt;
            overlim=bal*3;
            return true;
            
        }
        
        else
            return false;        
    }
    
    public boolean checkbal(double amt)                          //Member function to check balance of Current Account.
    {
        if(bal+amt>(bal+overlim))
            return false;
        
        else
            return true;
    }
}


public class Lab6exp1 
{
    public static void main(String args[]) 
    {
        Scanner in = new Scanner(System.in);
        byte ch, choice, acctype=0;
        String accnum, checkacc;
        double balance, odl;
        float roi;
        int[] date= new int[3];
        int[] today= new int[3];
        
        
        //Reference of Account class.
        Account Curr=null;
        Account Sav=null;
        
        System.out.println("1. Savings.");
        System.out.println("2. Current.");
        acctype = in.nextByte();
        
        System.out.println("\nEnter Date on which account was created: ");
        date[0]= in.nextInt();
        System.out.println("Enter Month on which account was created: ");
        date[1]= in.nextInt();
        System.out.println("Enter Year on which account was created: ");
        date[2]= in.nextInt();

        if(acctype==1)
        {
            System.out.println("\n\nEnter account number: ");
            accnum = in.next();
            System.out.println("Enter balance: ");
            balance = in.nextDouble();
            if(balance<500)
            {
                System.out.println("Minimum required balance is Rs. 500");
              
            }
            else
            {
                System.out.println("Enter rate of interest(Per Annum): ");
                roi = in.nextFloat();            

                Sav = new Savings(accnum, balance, roi);                        //Creating object and setting account number, balance and rate of interest.
                Sav.setDate(date);                              //Calling Member function to set date of account creation.
             
            }
            
        }

        else if(acctype==2)
        {
            System.out.println("\nEnter account number: ");
            accnum = in.next();
            System.out.println("Enter balance: ");
            balance = in.nextDouble();

            odl=balance*3;

            Curr=new Current(accnum, balance, odl);                        //Creating object and setting account number, balance and overdraft limit.
            Curr.setDate(date);                              //Calling Member function to set date of account creation.
        }
        
        if(Sav!=null)
        {
            System.out.println("\nEnter today's date: ");
            today[0]=in.nextInt();
            System.out.println("Enter today's month: ");
            today[1]=in.nextInt();
            System.out.println("Enter today's year: ");
            today[2]=in.nextInt();

            if(Sav!=null)
                ((Savings)Sav).checkdate(today);                              //Calling Member function to compare dates and change balance according to rate of interest.
        }
        
        
        
        
        while(true)
        {
            System.out.println("\n1. Display Account Details.");
            System.out.println("2. Transactions.");
            System.out.println("3. Delete an account.");
            System.out.println("4. Change date!");
            System.out.println("0. Exit");
            ch=in.nextByte();
            
            
            switch(ch)
            {

                case 1:
                {
                    if(Sav!=null && Sav.bal>500)
                    {
                        Sav.display();                              //Calling Member function to display details of Savings Account.
                    }
                    
                    else if(Sav!=null && Sav.bal<500)
                        System.out.println("The Savings Account couldn't be created. Create again!");
                    

                    else if(Curr!=null)
                    {
                        Curr.display();                              //Calling Member function to display details of Current Account.
                    }
                    
                    else
                        System.out.println("No Account created!!");


                }

                break;

                case 2:
                {
                    System.out.println("\n\nEnter Account Number to make Transactions: ");
                    checkacc = in.next();

                    if(Sav!=null && Sav.AccNum.equals(checkacc))                              //Calling Member function to check the validity of Account Number.
                    {
                        Sav.display();                                  //Calling Member function to display details of Savings Account.
                    }
                    
                    else if(Curr!=null && Curr.AccNum.equals(checkacc))                              //Calling Member function to check the validity of Account Number.
                    {
                        Curr.display();                                  //Calling Member function to display details of Current Account.
                    }
                    
                    else
                    {
                        System.out.println("\nEnter valid Account Number!");
                        break;                        
                    }
                        

                    System.out.println("\n1. Check Balance");
                    System.out.println("2. Deposit.");
                    System.out.println("3. Withdraw.");
                    System.out.println("\nEnter your choice: ");
                    choice = in.nextByte();

                    if(Sav!=null)
                    {  
                        if(choice==1)       //Check Balance.
                        {
                            if(Sav!=null)
                            {
                                System.out.println("\nBalance is: "+Sav.getBal());             //Calling Member function to display Savings Account balance.
                            }
                            
                            else
                                System.out.println("\nNo Savings account created!");
                        }

                        else if(choice==2)      //Deposit.
                        {
                            System.out.println("Enter amount to be deposited: ");

                             if(Sav!=null)
                            {
                                if(Sav.deposit(in.nextDouble()))                              //Calling Member function to deposit money into Savings Account.
                                System.out.println("\nAmount SUCCESSFULLY deposited!");

                                else
                                    System.out.println("\nAmount UNSUCCESSFULLY deposited!\nEnter valid amount!");
                            }

                        }

                        else if(choice==3)          //Withdraw.
                        {
                            System.out.println("Enter amount to be withdrawn: ");

                            if(Sav!=null)
                            {
                                if(Sav.withdraw(in.nextDouble()))                              //Calling Member function to withdraw from Savings Account.
                                    System.out.println("\nAmount SUCCESSFULLY withdrawn!\n\n");

                                else
                                    System.out.println("\nAmount UNSUCCESSFULLY withdrawn!\nEnter valid amount!\n\n");

                                Sav.display();                                      //Calling Member function to display details of Savings Account.

                            }
                        }
                        
                    }
                    
                    else if(Curr!=null)
                    {
                        if(choice==1)           //Check Balance.
                        {
                            if(Curr!=null)
                                System.out.println("Balance is: "+Curr.getBal());                   //Calling Member function to display Current Account Balance.
                            
                            else
                                System.out.println("\nNo Current Account created!");
                        }
                        
                        else if(choice==2)      //Deposit.
                        {
                            if(Curr!=null)
                            {
                                if(Curr.deposit(in.nextDouble()))                              //Calling Member function to deposit money into Current Account.
                                System.out.println("\nAmount SUCCESSFULLY deposited!");

                                else
                                    System.out.println("\nAmount UNSUCCESSFULLY deposited!\nEnter valid amount!");
                            }
                            
                            else
                                System.out.println("\nNo Current Account created!");
                            
                        }
                        
                        else if(choice==3)                  //Withdraw.
                        {
                            if(Curr!=null)
                            {
                                if(Curr.withdraw(in.nextDouble()))                              //Calling Member function to withdraw from Current Account.
                                System.out.println("\nAmount SUCCESSFULLY withdrawn!");

                                else
                                    System.out.println("\nAmount UNSUCCESSFULLY withdrawn!\nEnter valid amount!\n\n");

                                Curr.display();                                  //Calling Member function to display details of Current Account.
                            }
                            
                            else
                                System.out.println("\nNo Current Account created!");
                        }
                        
                    }
                    
                }
                
                break;
                
                case 3:
                {
                    System.out.println("\n\nEnter account number to delete: ");
                    checkacc = in.next();

                    if(Sav!=null && Sav.AccNum.equals(checkacc))                              //Calling Member function to check the validity of Account Number.
                    {
                        System.out.println("Your account is: \n\n");
                        Sav.display();                                  //Calling Member function to display details of Savings Account.
                        Sav=null;
                        System.out.println("\nAccount deleted successfully!");                        
                    }
                    
                    else if(Curr!=null && Curr.AccNum.equals(checkacc))                              //Calling Member function to check the validity of Account Number.
                    {
                        System.out.println("Your account is: \n\n");
                        Curr.display();                                         //Calling Member function to display details of Savings Account.
                        Curr=null;
                        System.out.println("\nAccount deleted successfully!");
                    }
                    
                    else
                    {
                        System.out.println("Enter Valid Account Number!");
                        break;                        
                    }
                    
                }
                break;
                
                case 4:
                {
                    System.out.println("\nEnter today's date: ");
                    today[0]=in.nextInt();
                    System.out.println("Enter today's month: ");
                    today[1]=in.nextInt();
                    System.out.println("Enter today's year: ");
                    today[2]=in.nextInt();
                    
                    if(Sav!=null)
                        ((Savings)Sav).checkdate(today);                              //Calling Member function to change the date to calculate transactions.
                    
                }
                
                break;
                
                case 0: return;                 //Exit.

            }
        }
        
    }
    
}