/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Write the first two programs of Assignment 1 in Java. PROGRAM 2 HERE.
Date: 19th January, 2016
*/

package PreviousLab2;

import java.util.Scanner;

class customer                  //Customer Class
{
    private int cust_ID;
    private String cust_name;
    
    public void readcust()          //Read Customer Details
    {
        Scanner input = new Scanner(System.in);
        
        System.out.println("Enter Customer name: ");
        cust_name = input.next();
        
        System.out.println("Enter Customer number: ");
        cust_ID = input.nextInt();
               
    }
    
    public void displaycust()           //Display Customer Details.
    {
        System.out.println("Customer name: "+cust_name);
        System.out.println("Customer number: "+cust_ID);
    }
};

class product                       //Product Class
{
    private int prod_ID;
    private String prod_name;
    private int qty;
    private float price;
    
    public void readprod()                  //Read Product Details
    {
        Scanner input = new Scanner(System.in);


		System.out.println("Enter Item Number: ");
		prod_ID = input.nextInt();

		System.out.println("Enter Item Name: ");
		prod_name = input.next();

		System.out.println("Enter Item Price: ");
		price = input.nextFloat();

		System.out.println("Enter Item Quantity: ");
		qty = input.nextInt();
    }
    
    public void displayprod()                   //Display Product Details
    {
        System.out.println("No: " +prod_ID);
	System.out.println("Name: "+prod_name);
	System.out.println("Price: "+price);
	System.out.println("Quantity: "+qty);
    }
};


public class PreviousLab2                       //Class containing MAIN Function
{

    public static void main(String[] args) 
    {
    
        Scanner input = new Scanner(System.in);
        customer cust = new customer();             //Object of Customer class
        product prod =  new product();                //Object of Product Class
        int ch, temp1, temp2;
        String name;
        
        while(true)
        {
            System.out.println("1. Customer");
            System.out.println("2. Product");
            System.out.println("3. Bill");
            System.out.println("Enter choice: ");
            ch = input.nextInt();

            switch(ch)
            {
                case 1:
                    cust.readcust();
                break;

                case 2:
                    prod.readprod();
                break;

                case 3:
                    cust.displaycust();
                    prod.displayprod();
            }

        }
    }
}
