/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Write the first two programs of Assignment 1 in Java. PROGRAM 1 HERE.
Date: 19th January, 2016
*/
package PreviousLab1;

import java.util.Scanner;

class customer
{
    private String cust_name;
    private String cust_address;
    private int currAccNum, savAccNum;
    private float currAccBal, savAccBal;
   
    customer()
    {
        currAccBal = 0;
        savAccBal = 0;
    }
     
        
    public void readdetails()
    {
        float curr, sav;
        Scanner input =  new Scanner(System.in);
        
        System.out.println("Enter name: ");
        cust_name = input.next();
        System.out.println("Enter address: ");
        cust_address = input.next();
        
        System.out.println("Enter Account Number(Current): ");
        currAccNum = input.nextInt();
        System.out.println("Enter Account Number(Savings): ");
        savAccNum = input.nextInt();
        
        System.out.println("Enter balance(Current): ");
        curr = input.nextFloat();
        setCurrBal(curr);        
        System.out.println("Enter Balance(Savings): ");
        sav = input.nextFloat();
        setSavBal(sav);
    }
    
    public void displaydetails()
    {
        float curr, sav;
        System.out.println("Name: "+cust_name);
        System.out.println("Address: "+cust_address);
        System.out.println("Current Account Number: "+currAccNum);
        System.out.println("Savings Account Number: "+savAccNum);
        
        
    }
    
    public void setCurrBal(float curr)
    {
        currAccBal = curr;
    }
    
    public void setSavBal(float sav)
    {
        savAccBal = sav;
    }
    
    public float getCurrBal()
    {
        return currAccBal;
    }
    
    public float getSavBal()
    {
        return savAccBal;
    }
    
   
    
};

public class PreviousLab1 
{

    public static void main(String[] args) 
    {
        int ch=1, ch1, ch2;
        float curr, sav, amt;
        customer cust = new customer();
        
        Scanner input =  new Scanner(System.in);
        
        while(ch!=0)
        {
            System.out.println("1. Add a customer.");
            System.out.println("2. Show details of a customer.");
            System.out.println("3. Perform Transactions.");
            System.out.println("0. Exit.");
            System.out.println("Enter your choice.");
            ch = input.nextInt();


            switch(ch)
            {
                case 1:
                    cust.readdetails();

                break;

                case 2:
                {   
                    cust.displaydetails();
                    curr = cust.getCurrBal();
                    sav = cust.getSavBal();
                    System.out.println("Current Account Balance: "+curr);
                    System.out.println("Savings Account Balance: "+sav);
                }

                break;
                case 3:
                {                
                    System.out.println("1. Show Bank Balance.");
                    System.out.println("2. Withdraw.");
                    System.out.println("3. Deposit.");
                    System.out.println("Enter your choice.");
                    ch1 = input.nextInt();

                    switch(ch1)
                    {
                        case 1:
                        {
                            curr = cust.getCurrBal();
                            sav = cust.getSavBal();
                            System.out.println("Current Account Balance: "+curr);
                            System.out.println("Savings Account Balance: "+sav);
                        }

                        break;

                        case 2:
                        {
                            System.out.println("1. Current.");
                            System.out.println("2. Savings.");
                            System.out.println("Enter Account type: ");
                            ch2 = input.nextInt();

                            if(ch2 == 1)
                            {
                                curr = cust.getCurrBal();
                                if(curr<1000)
                                {
                                    System.out.println("Minimum Balance Required!");                           
                                }

                                else
                                {
                                    System.out.println("Current Account Balance: "+curr);
                                    System.out.println("");
                                    System.out.println("Enter amount: ");
                                    amt = input.nextFloat();
                                    curr = cust.getCurrBal();
                                    curr = curr - amt;
                                    cust.setCurrBal(curr);


                                }                         

                            }

                            else if(ch2 == 2)
                            {
                                sav = cust.getSavBal();
                                if(sav<1000)
                                {
                                    System.out.println("Minimum Balance Required!");                           
                                }

                                else
                                {
                                    System.out.println("Current Savings Balance: "+sav);
                                    System.out.println("");
                                    System.out.println("Enter amount: ");
                                    amt = input.nextFloat();
                                    sav = cust.getSavBal();
                                    sav = sav - amt;
                                    cust.setSavBal(sav);

                                }

                            }
                        }

                        break;

                        case 3:
                        {
                            System.out.println("1. Current.");
                            System.out.println("2. Savings.");
                            System.out.println("Enter Account type: ");
                            ch2 = input.nextInt();

                            if(ch2 == 1)
                            {
                                System.out.println("Enter amount: ");
                                amt = input.nextFloat();
                                curr = cust.getCurrBal();
                                curr = curr + amt;
                                cust.setCurrBal(curr);
                                System.out.println("");
                                System.out.println("");
                                System.out.println("AMOUNT DEPOSITED!");

                            }

                            else if(ch2 == 2)
                            {
                                System.out.println("Enter amount: ");
                                amt = input.nextFloat();
                                sav = cust.getSavBal();
                                sav = sav+ amt;
                                cust.setSavBal(sav);
                                
                                System.out.println("");
                                System.out.println("");
                                System.out.println("AMOUNT DEPOSITED!");

                            }
                        }
                        
                            default:
                            System.out.println("");
                            System.out.println("Enter Valid Choice!");

                    }



                }
                
                break;
                
                default:
                    System.out.println("");
                    System.out.println("Enter Valid Choice!");
                
               
            }
        
            
        }
        
        
        
    }
    
 }