/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Create a linked list of Employees with a feature to edit it.
Date: 24th January, 2016
*/


import java.util.Scanner;




class Employee {
	
	private int number;
	private String name, dateofbirth, contact;
	private char gender;
	private double baseSal, netSal=this.calNetSal(baseSal);
	
	//Getter functions
	public int getNo()
        {
            return number;
        }
	
	public String getName()
        {
            return name;
        }
	
	public String getDateofBirth()
        {
            return dateofbirth;
        }
	
	public String getContact()
        {
            return contact;
        }
	
	public char getGender()
        {
            return gender;
        }
	
	public double getBaseSal()
        {
            return baseSal;
        }
	
	public double getNetSal()
	{
		return netSal;
	}
	
	//Setter functions
	
	public void setNo(int i)
	{
		if (i>0)
			number=i;
		
	}
	
	public void setName(String sname)
	{
		name=sname;
	}
	
	public void setDateofbirth(String ndob)
	{
		dateofbirth=ndob;
	}
	
	public void setContact(String ncont)
	{
		contact=ncont;
	}
	
	public void setGender(char ngend)
	{
		gender=ngend;
	}
	
	public void setSalary(double fbase)
	{
		baseSal=fbase;
	}
	
	//print data
	public void print()
	{
		System.out.println("\nNumber: " + number);
		System.out.println("Name: " +name);
		System.out.println("DOB: " +dateofbirth);
		System.out.println("Gender: "+gender);
		System.out.println("Contact: "+contact);
		System.out.println("Base Sal: "+baseSal);
		System.out.println("Net Sal: "+calNetSal(baseSal));
	}
	
	//Calculating Gross Salary
	
	public double calGrossSal(double BaseSal)
	{
		double DA=(0.6)*BaseSal;
		double HRA=0.12*BaseSal;
		double TA=12000;
		
		double GrossSal=BaseSal + DA + HRA + TA;
		return GrossSal;
		
	}
	
	public double calNetSal(double BS)
	{
		double GrossSal=calGrossSal(BS);
		double PF=0.1*BS;
		double PT=200;
		double TA=12000;
		double TDS=0.1*(GrossSal - TA - PF - PT);
		double TD=PF + PT + TDS;
		double NetSal=GrossSal-TD;
		netSal=NetSal;
		return NetSal;
	}
	
	public Employee(int number, String name, String dateofbirth, char gender, double baseSal)
	{
		this.number=number;
		this.name=name;
		this.dateofbirth=dateofbirth;
		this.gender=gender;
		this.baseSal=baseSal;
	}
	
	public Employee()
	{
		
	}
}
	




class LinkedList 
{
	
	private Employee data = null;
	private LinkedList next = null;



	public void add(Employee newDetails)
	{ 
		if(data == null)
			this.data = newDetails;
		else
		{
			LinkedList curr;
			for(curr = this; curr.next!=null; curr = curr.next);

			curr.next = new LinkedList();
			curr = curr.next;
			curr.data = newDetails;
			curr.next = null;
		}
	}
	
	static int cnt;
	public Employee get(int i)
	{
		LinkedList curr;
		for(curr = this,cnt=0; curr.next!=null && cnt<i; curr = curr.next, cnt++);
		
		return curr.data;
	}

	public void display()
	{
		if(this.data == null)
			System.out.println("\nList is Empty!");
		else
		{
			LinkedList curr;
			for(curr = this; curr!=null; curr = curr.next)
			{
				curr.data.print();
				System.out.println();
			}
		}
	}
	
	public int find(String findname)
	{
		LinkedList curr;
		for(curr = this, cnt=0; curr!=null; curr = curr.next,cnt++)
		{
			if(curr.data.getName().equals(findname))
			{
				return cnt;
			}
			
		}
		
		return -1;
		
	}
	
	public LinkedList remove(int i)
	{
		LinkedList curr,prev,head=this;
		for(curr = this,cnt=0; curr.next!=null && cnt<i; curr = curr.next, cnt++);
		for(prev = this,cnt=0; prev.next!=null && cnt<i-1; prev = prev.next, cnt++);
		
		if(cnt==0)
		{
			return curr.next;
		}
		
		else 
		{
			prev.next=curr.next;
			return head;
		}
	}

}



class Lab2exp2 
{
	private static Scanner input;

	public static void main(String[] args)
	{
		input = new Scanner(System.in);
		LinkedList emp=new LinkedList();
                int no; 
                double baseSal;
                String name;
                char gender;
                String dateofbirth;
                
		
		Employee temp=new Employee();
		
		
		
		while(true)
		{
			System.out.println("\n1. View List.");
			System.out.println("2. Enter New Employee.");
			System.out.println("3. Edit Existing Employee.");
			System.out.println("4. Search Employee.");
			System.out.println("5. Remove Existing Employee.");
			System.out.println("0. Exit.");
			
			int ch=input.nextInt();
			switch(ch)
			{
				case 1:
                                    emp.display();
				
				break;
				
				case 2:
                                {
					System.out.println("Enter no: ");
					no=input.nextInt();
					
					System.out.println("Enter Name:");
					name=input.next();
					
					System.out.println("Enter Date of Birth:");
					dateofbirth=input.next();
					
					System.out.println("Enter Gender:");
					gender=(char) input.next().charAt(0);
					
					System.out.println("Enter Base Salary:");
					baseSal=input.nextDouble();
					
					temp= new Employee(no,name,dateofbirth,gender,baseSal);
					
					emp.add(temp);
                                }
                                
				break;
					
				case 3:
					
					System.out.println("\nEnter Name:");
					name=input.next();
					
					int index=emp.find(name);
					
					if(index!=-1)
					{
						while(true)
						{
						System.out.println("\n1. Change Number.");
						System.out.println("2. Change Name.");
						System.out.println("3. Change Date of Birth.");
						System.out.println("4. Change Gender.");
						System.out.println("5. Change base salary.");
						System.out.println("6. Change Contact.");
						System.out.println("0. Apply Changes.");
						
						ch=input.nextInt();
						switch(ch)
						{
							case 1:
								System.out.println("Enter Number: ");
								no=input.nextInt();
								emp.get(index).setNo(no);
								break;
								
							case 2:
								System.out.println("Enter Name: ");
								name=input.next();
								emp.get(index).setName(name);
								break;
								
							case 3:
								System.out.println("Enter Date of Birth: ");
								dateofbirth=input.next();
								emp.get(index).setDateofbirth(dateofbirth);
								break;
								
							case 4:
								System.out.println("Enter Gender: ");
								gender=(char) input.next().charAt(0);
								emp.get(index).setGender(gender);
								break;
								
							case 5:
								System.out.println("Enter base salary: ");
								baseSal=input.nextDouble();
								emp.get(index).setSalary(baseSal);
								break;
								
							case 6:
								System.out.println("Enter contact: ");
								String cntct=input.next();
								emp.get(index).setContact(cntct);
								break;
								
							case 0:
								System.out.println("Changes Applied!");
								break;
							}
						
							if(ch==0)break;
						
							}
						}
					
						else
							System.out.println("Wrong Name");
					break;
					
				case 4:
					System.out.println("\nEnter Name:");
					name=input.next();
					
					index=emp.find(name);
					
					if(index!=-1)
					{
						emp.get(index).print();
					}
					
					else
						System.out.println("Wrong Name");
					break;
					
					
				case 5:
					System.out.println("\nEnter Name:");
					name=input.next();
					
					index=emp.find(name);
					
					if(index!=-1)
					{
						emp=emp.remove(index);
					}
					
					else
						System.out.println("Wrong Name");
					
					break;
					
				case 0:
					return;
					
			}
			
			
			
		}
	
	}
}