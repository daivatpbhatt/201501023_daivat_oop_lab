/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Create a dynamically sized vector and perform functions.
Date: 17th February, 2016
*/

package lab5exp2;

import java.util.*;
import java.lang.Math;
import static java.lang.Math.pow;

class Vector
{
    int[] vector = new int[3];
    int dim;
   
    
    Vector(int i)               //Constructor to  initialise values.
    {
        int j;
        dim = i;
        for(j=0;j<3;j++)
        {
            vector[j]=0;
        }
        
    }
    
    public void assign()        //Member function to assign values.
    {
        Scanner in = new Scanner(System.in);
        int i;
        
        for(i=0;i<dim;i++)
        {
            System.out.println("Enter coordinate[" +(i+1)+"] : ");
            vector[i]=in.nextInt();
        }
    }
    
    public void display()        //Member function to display values.
    {
        int i;
        System.out.print("\n(");
        
        if(dim==0)
            System.out.println("Enter valid dimensions!!");
        else
        {
            for(i=0;i<dim;i++)
            {
                if(i<dim-1)
                    System.out.print(""+vector[i]+", ");

                else if(i==dim-1)
                    System.out.print(""+vector[i]+")");
            }
            System.out.print("\n");
        }
            
        
        
    }
    
    public void update()        //Member function to update values.        
    {
        Scanner in = new Scanner(System.in);
        int i;
        System.out.println("The dimenstions of this vector are: "+this.dim+"\n");
        
        for(i=0;i<dim;i++)
        {
            System.out.println("Enter new coordinate[" +(i+1)+"] : ");
            vector[i]=in.nextInt();
        }
    }
    
    public void magnitude()        //Member function to calculate magnitude.
    {
        int i;
        float magnitude=0;
        
        for(i=0;i<dim;i++)
        {
            magnitude+= pow(vector[i], 2);
        }
        magnitude= (float)pow(magnitude, 0.5);
        
        System.out.println("\nThe magnitude of the vector is: "+magnitude+"\n");
    }
    
    public void add(Vector V2)        //Member function to add two vectors.
    {
        int i, dimadd=0;
        
        if(dim>V2.dim)
            dimadd=dim;
        else if(dim<=V2.dim)
            dimadd=V2.dim;
        
        
        int[] addition = new int[dimadd];
        
        for(i=0;i<dimadd;i++)
        {
            addition[i]=vector[i]+V2.vector[i];
        }
        
        System.out.print("\n\nThe addition is: \n\n(");
        
        for(i=0;i<dimadd;i++)
        {
            if(i<dimadd-1)
                System.out.print(""+addition[i]+", ");
            
            else if(i==dim-1)
                System.out.print(""+addition[i]+")");
        }
        System.out.println("\n");
       
    }
    
    public void subtract(Vector V2)             //Member function to subtract two vectors
    {
        int i, dimsub=0;
        
        if(dim>V2.dim)
            dimsub=dim;
        else if(dim<=V2.dim)
            dimsub=V2.dim;
        
        
        int[] subtraction = new int[dimsub];
        
        for(i=0;i<dimsub;i++)
        {
            subtraction[i]=vector[i]-V2.vector[i];
        }
        
        System.out.print("\nThe subtraction is: \n\n(");
        
        for(i=0;i<dimsub;i++)
        {
            if(i<dimsub-1)
                System.out.print(""+subtraction[i]+", ");
            
            else if(i==dim-1)
                System.out.print(""+subtraction[i]+")");
        }
        System.out.println("\n");
    }
    
    public void scalarprod(Vector V2)        //Member function to find scalar product of two vectors.
    {
        int i, dimscal=0, scalar = 0;
        
        if(dim>V2.dim)
            dimscal=dim;
        else if(dim<=V2.dim)
            dimscal=V2.dim;
        
        for(i=0;i<dimscal;i++)
        {
            scalar +=vector[i]*V2.vector[i];
        }
        
        System.out.print("\nThe scalar product is: "+scalar+"\n\n");
      
    }
    
    public void vectorprod(Vector V2)        //Member function to find vector product of two vectors.
    {
        int i;
       
        int [] multiplyprod=new int[3];
        
        for(i=0;i<3;i++)
        {
            multiplyprod[i]=0;
        }
        
        multiplyprod[0]=vector[1]*V2.vector[2] - vector[2]*V2.vector[1];
        multiplyprod[1]=-(vector[0]*V2.vector[2] - vector[2]*V2.vector[0]);
        multiplyprod[2]=vector[0]*V2.vector[1] - vector[1]*V2.vector[0];
        
        System.out.print("The vector product is: \n(");
        for(i=0;i<3;i++)
            {
                if(i<3-1)
                    System.out.print(""+multiplyprod[i]+", ");

                else if(i==3-1)
                    System.out.print(""+multiplyprod[i]+")");
            }
            System.out.print("\n");
        
        
        
    }
    
    public void multiply(int constant)        //Member function to multiply a constant with the vector.
    {
        int i;
        
        for(i=0;i<dim;i++)
        {
            vector[i] =  vector[i]*constant;
        }
        
        System.out.println("The new vector is: \n");
        display();
       
    }
    
    public void polar()        //Member function to find the polar coordinates of the vector.
    {
        double r=0, theta, phi=0;
        int i;
        
        for(i=0;i<dim;i++)
        {
            r+= pow(vector[i], 2);
        }
        r = (float)pow(r, 0.5);
            
        if(r!=0)
        {
            theta = (vector[2]/r);
 
            r = r*10000;
            r =  Math.floor(r);
            r=r/10000;
            theta = Math.acos(vector[2]/r);
            theta= theta*10000;
            theta =  Math.floor(theta);
            theta=theta/10000;
            
            System.out.println("\nPolar coordinates(r, θ, φ) are: \n");
           
            if(vector[0]!=0)
            {
                phi = (Math.tan(vector[1]/vector[0]));
                
                

                phi = phi*10000;
                phi =  Math.floor(phi);
                phi = phi/10000;

                System.out.println("("+r+", "+theta+", "+phi+")");
            }
            
            else
                System.out.println("("+r+", "+theta+", 0)");
                
            }   
         
       else
            System.out.println("\nPolar coordinate conversion is not possible. Possibly, vector is at the origin.\n");
       
    }
   
    
}


public class Lab5exp2 
{
    public static void main(String[] args) 
    {
        int dim, dim2, addnew=0, constant;
        byte ch=0, choice=0;
        
        Scanner in = new Scanner(System.in);
        
        System.out.println("Enter the dimensions of first vector [MUST BE LESS THAN OR EQUAL TO 3]: ");
        dim = in.nextInt();
        
        Vector V1 = new Vector(dim);                                //Creating Vector1(new object)
        System.out.println("\nEnter for first Vector: \n");
        V1.assign();                                            //Calling Member function to assign values.
        
        Vector V2 = null;                                       //Creating Vector2(new object)
        System.out.println("\nEnter dimensions for second Vector? (1 - Yes; 0 - Zero)");
        addnew=in.nextInt();
                    
        if(addnew==1)
        {
            System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
            dim2 =  in.nextInt();
                
            System.out.println("\nEnter for second vector: \n");
            V2 = new Vector(dim2);
            V2.assign();                              //Calling Member function to assign values.
           
        }
        
        while(true)
        {
            
            System.out.println("\n1. Display the vectors.");
            System.out.println("2. Update values of the vector.");
            System.out.println("3. Find the magnitude of the vector.");
            System.out.println("4. Add two vectors.");
            System.out.println("5. Subtract two vectors.");
            System.out.println("6. Find dot(scalar) product of two vectors.");
            System.out.println("7. Find cross(vector) product of two vectors.");
            System.out.println("8. Multiply a scalar with a vector.");
            System.out.println("9. Calculate the polar coordinates of the vector.");
            System.out.println("0. Exit.");
            System.out.println("\nEnter your choice: \n");

            ch = in.nextByte();
            
            switch(ch)
            {
                case 1:
                {
                    System.out.println("\n\nFirst Vector is: ");
                    V1.display();                              //Calling Member function to display values
                    
                    if(V2 == null)
                        System.out.print("\n\nThe second vector hasn't been entered yet!\n");
                    
                    else if(V2 != null)
                    {
                        System.out.println("\n\nSecond vector is: ");
                        V2.display();                              //Calling Member function to display values.
                    }
                        
                    
                }
                break;
                
                case 2:
                {
                    System.out.println("\n\nUpdate Vector1 or Vector2? : ");
                    choice=in.nextByte();
                    if(choice==1)
                        V1.update();                              //Calling Member function to update values.
                    
                    if(choice == 2)
                    {
                        if(V2==null)
                        {
                            System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                            System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                            dim2 =  in.nextInt();

                            System.out.println("Enter for second vector: \n");
                            V2 = new Vector(dim2);
                            V2.assign();                              //Calling Member function to assign values to new vector.
                        }
                    
                       else if(V2 != null)
                            V2.update();                              //Calling Member function to update values.
                        
                    }
                    
                }
                break;
                
                case 3:
                {
                    System.out.println("\nFind magnitude of Vector1 or Vector2? : ");
                    System.out.println("Enter your choice: ");
                    choice=in.nextByte();
                    
                    if(choice==1)
                        V1.magnitude();                              //Calling Member function calculate magnitude of the vector.
                    
                    else if(choice==2)
                    {
                        if(V2==null)
                        {
                            System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                            System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                            dim2 =  in.nextInt();

                            System.out.println("Enter for second vector: \n");
                            V2 = new Vector(dim2);
                            V2.assign();                              //Calling Member function to assign values to new vector.
                        }
                        
                        else if(V2 != null)
                        {
                            V2.magnitude();                              //Calling Member function to calculate magnitude of the vector.
                        }
                    }
                   
                    
                }
                break;
                
                case 4:
                {
                    if(V2==null)
                    {
                        System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                        System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                        dim2 =  in.nextInt();

                        System.out.println("\nEnter for second vector: \n");
                        V2 = new Vector(dim2);
                        V2.assign();                              //Calling Member function to assign values to new vector.
                        System.out.println("\n\n");
                        V1.add(V2);                              //Calling Member function to add two vectors.
                                
                    }
                    
                    else if(V2 != null)
                    {                              //Calling Member function to add two vectors.
                        V1.add(V2);
                    }
                    
                }
                
                break;
                
                case 5:
                {
                    System.out.println("1. V1 - V2.");
                    System.out.println("2. V2 - V1.");
                    System.out.println("Enter your choice: ");
                    choice=in.nextByte();
                    
                    if(V2==null)
                    {
                        System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                        System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                        dim2 =  in.nextInt();

                        System.out.println("Enter for second vector: \n");
                        V2 = new Vector(dim2);
                        V2.assign();                              //Calling Member function to assign values to new vector.
                    }
                    
                    if(choice==1)
                        V1.subtract(V2);                              //Calling Member function to subtract Vector 2 FROM Vector 1.
                    
                    else if(choice==2)
                    {
                        if(V2 != null)
                        {
                            V2.subtract(V1);                              //Calling Member function to subtract Vector 1 FROM Vector 2.
                        }
                    }
                        
                    else
                            System.out.println("\nEnter valid choice!!");
                    
                }
                
                break;
                
                case 6:
                {
                    if(V2==null)
                    {
                        System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                        System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                        dim2 =  in.nextInt();

                        System.out.println("Enter for second vector: \n");
                        V2 = new Vector(dim2);
                        V2.assign();                              //Calling Member function to assign values to new vector.
                        
                        V1.scalarprod(V2);                              //Calling Member function to find scalar product.
                    }
                    
                    else if(V2 != null)
                    {
                        V1.scalarprod(V2);                              //Calling Member function to find scalar product.
                    }
                    
                }
                
                break;
                
                case 7:
                {
                    System.out.println("1. V1 x V2.");
                    System.out.println("2. V2 x V1.");
                    System.out.println("Enter your choice: ");
                    choice=in.nextByte();
                    
                    if(V2==null)
                    {
                        System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                        System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                        dim2 =  in.nextInt();

                        System.out.println("Enter for second vector: \n");
                        V2 = new Vector(dim2);
                        V2.assign();                              //Calling Member function to assign values to new vector.
                    }
                    
                    if(choice==1)
                        V1.vectorprod(V2);                              //Calling Member function to find V1 x V2.
                    
                    else if(choice==2)
                    {
                        if(V2 != null)
                        {
                            V2.vectorprod(V1);                              //Calling Member function to V2 x V1.
                        }
                    }
                        
                    else
                            System.out.println("\nEnter valid choice!!");
               
                }
                
                break;
                case 8:
                {
                    System.out.println("\nEnter the constant: ");
                    constant = in.nextInt();
                    
                    System.out.println("\nMultiply with Vector1 or Vector2? : ");
                    choice = in.nextByte();
                    
                    if(choice==1)
                        V1.multiply(constant);                              //Calling Member function to multiply constant with vector.
                    
                    else if(choice == 2)
                    {
                        if(V2==null)
                        {
                            System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                            System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                            dim2 =  in.nextInt();

                            System.out.println("Enter for second vector: \n");
                            V2 = new Vector(dim2);
                            V2.assign();                              //Calling Member function to assign values to new vector.

                            V2.multiply(constant);                              //Calling Member function to multiply constant with vector.
                        }

                        else if(V2 != null)
                        {
                            V2.multiply(constant);                              //Calling Member function to multiply constant with vector.
                        }

                    }
                    
                    else
                        System.out.println("\nEnter valid choice!!");
                    
                }
                
                break;
                
               case 9:
                {
                    System.out.println("\nFind Polar Coordinates of Vector1 or Vector2? : ");
                    choice = in.nextByte();
                    
                    if(choice==1)
                        V1.polar();                              //Calling Member function to calculate polar coordinates of vector.
                    
                    else if(choice == 2)
                    {
                        if(V2==null)
                        {
                            System.out.println("\nThe second vector hasn't been entered yet! Enter here!\n");
                            System.out.println("\nEnter dimensions of the second vector[MUST BE LESS THAN OR EQUAL TO 3]: ");
                            dim2 =  in.nextInt();

                            System.out.println("Enter for second vector: \n");
                            V2 = new Vector(dim2);
                            V2.assign();                              //Calling Member function to assign values to new vector.

                            V2.polar();                              //Calling Member function to calculate polar coordinates of vector.
                        }

                        else if(V2 != null)
                        {
                            V2.polar();                              //Calling Member function to calculate polar coordinate of vector.
                        }

                    
                    }
                    
                    else
                        System.out.println("\nEnter valid choice!!");
                }
                break;
                
                case 0: return;                                             //To exit the loop.
                
                default: System.out.println("Enter valid choice!");
            }
        }
        
    }
    
}