/*
Name: Daivat Bhatt
Roll Number: 201501023
Class: ICT, Batch 2015
Experiment: Create dynamically sized matrices and perform functions.
Date: 17th February, 2016
*/


package lab5exp1;

import java.util.*;

class Matrix
{
    private int rows;
    private int cols;
    private int [][] matrix;
    
    Matrix(int i, int j)                //Constructor to inititalise values.
    {
        rows = i;
        cols = j;
        matrix = new int[rows][cols];
        
    }
    
    Matrix(Matrix curr)                 //Copy Constructor to create another object with same values.
    {
        int i, j;
        rows=curr.rows;
        cols = curr.cols;
        matrix = new int[rows][cols];
        
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                matrix[i][j]=curr.matrix[i][j];
            }
        }
    }
    
    public void readMatrix()                                //Member Function to insert values.
    {
        Scanner in = new Scanner(System.in);
        int i, j;
        
        
        for(i=0;i<rows;i++)
        {
            System.out.println("\n");
            System.out.print("For Row #"+(i+1)+" : \n");
            
            for(j=0;j<cols;j++)
            {
                System.out.println("Enter element ["+(i+1)+"]["+(j+1)+"]: ");
                matrix[i][j]= in.nextInt();
            }
        }
        System.out.print("\n\n");
        
    }
    
    public void displayMatrix()                                //Member Function to display the matrix.
    {
        int i, j;
        System.out.print("\n");
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                System.out.print(+matrix[i][j]);
                System.out.print("\t");
            }
            
            System.out.print("\n");
        }
        System.out.print("\n");
        
    }
    
    @Override
    public String toString()                                //Member Function to convert the values into string.
    {
        String displayM = new String();
        int i, j;
        
        displayM+="\n\n";
        
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                displayM+=matrix[i][j];
                displayM+="\t";
            }
            
            displayM+="\n";
        }
        
        return displayM;
    }
    
    public byte checkforempty()                                //Member Function to check whether matrix is empty or not.
    {
        int i, j, cnt=0;
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                if(matrix[i][j]==0)
                    cnt++;
            }
        }
        
        if(cnt==(rows*cols))
        {
            System.out.println("\nThe matrix is empty! Enter a matrix first!");
        
            return 0;
        }
        
        else
            return 1;
            
            
    }
        
    
    public void transpose()                                //Member Function to find transpose of matrix.
    {
        int[][] transpose = new int[rows][cols];
        
        int i, j = 0;
        
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                if(i==j)
                {
                    transpose[i][j]=matrix[i][j];
                }
                
                else if(i!=j)
                {
                    transpose[j][i]=matrix[i][j];
                }
                    
            }
        }
        
        System.out.println("\nThe tranpose of the matrix is: \n");
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                System.out.print(+transpose[i][j]);
                System.out.print("\t");
            }
            
            System.out.print("\n");
        }
        System.out.print("\n");
        
        
    }
    
    
    public void add(Matrix M2)                                //Member Function to add two matrices.
    {
        int i, j;
        
        
        if(M2.rows!=rows || M2.cols!=cols)
        {
            System.out.println("Both the matrices must have the same rows and columns !!");
        }
        
        else if(M2.rows== rows && M2.cols == cols)
        {
            int[][] addmatrix = new int[rows][cols];
            
            for(i=0;i<rows;i++)
            {
                for(j=0;j<cols;j++)
                {
                    addmatrix[i][j] = matrix[i][j]+M2.matrix[i][j];
                }
            }
            System.out.println("\nThe result of the addition of the two matrices is: ");
            
            for(i=0;i<rows;i++)
            {
                for(j=0;j<cols;j++)
                {
                    System.out.print(+addmatrix[i][j]);
                    System.out.print("\t");
                }

                System.out.print("\n");
            }
            System.out.print("\n");
            
        }
    }
    
    public void multiply(Matrix M2)                                //Member Function to multiply two matrices.
    {
        int i, j, k;
        if(M2.rows!=cols)
        {
            System.out.println("The matrices cannot be multiplied with each other !");
            System.out.println("\nEnter the rows of matrix 2 as the same as columns of matrix 1!");            
        }
        
        else if(M2.rows==cols)
        {
            int[][] multiplymatrix =  new int[rows][M2.cols];
            
            for(i=0;i<rows;i++)
            {
                for(j=0;j<M2.cols;j++)
                {
                    for(k=0;k<cols;k++)
                    {
                        multiplymatrix[i][j]=multiplymatrix[i][j]+ matrix[i][k]*M2.matrix[k][j];
                    }
                    
                    
                }
            }
            
            System.out.println("\nThe result of the multiplication of the two matrices is: ");
            
            for(i=0;i<rows;i++)
            {
                for(j=0;j<M2.cols;j++)
                {
                    System.out.print(+multiplymatrix[i][j]);
                    System.out.print("\t");
                }

                System.out.print("\n");
            }
            System.out.print("\n");
            
        }
            
        
    }
    
    public void multiply(int constant)                                //Member Function to multiply constant with a matrix.
    {
        int i, j;
        
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                matrix[i][j]=constant*matrix[i][j];
            }
        }
    }
    
    public boolean checkelements()                                //Member Function to check elements for validity to check for magic square.
    {
        int i, j, k, cnt=0, flag=0;
        
        for(i=0;i<rows;i++)
        {
            for(j=0;j<cols;j++)
            {
                if(matrix[i][j]>rows*rows)
                    return false;
                else
                    cnt++;
                
            }
        }
        
        if(cnt==(rows*cols))
            flag++;
        
        cnt=0;
        for(k=0;k<rows;k++)
        {
            for(i=0;i<rows && i!=k;i++)
            {
                for(j=0;j<cols;j++)
                {
                    if(matrix[k][j]==matrix[i][j])
                        return false;
                    else
                        cnt++;

                }
            }
        }
        
        if(cnt==(rows*cols))
            flag++;
        
        if(flag==2)
            return true;
        
        else
            return false;
                
        
    }
    
    
    public void magicsquare()                                //Member Function to check whether matrix is a magic square.
    {
        if(rows!=cols)
            System.out.println("\nThe matrix is not a perfect square!");
        
        else if(rows==cols)
        {
            int row1sum=0, row2sum=0, col1sum=0, col2sum=0, row3sum=0, col3sum=0, diag1sum=0, diag2sum=0, actualsum=0;
            int i, j;
            actualsum=rows*((rows*rows)+1)/2;
            
            System.out.println("\nThe actual sum of a magic square of " +rows+"x"+cols+" dimenstion will be: "+actualsum);
            System.out.println("\n\n");
            
            for(j=0;j<cols;j++)
            {
                row1sum+=matrix[0][j];
            }
            
            if(row1sum!=actualsum)
                System.out.println("The matrix is not a magic square!");
            
            else
            {
                for(j=0;j<cols;j++)
                {
                    row2sum+=matrix[1][j];
                }
                
                if(row2sum!=actualsum)
                    System.out.println("The matrix is not a magic square!");
                
                else
                {
                    for(j=0;j<cols;j++)
                    {
                        row3sum+=matrix[2][j];
                    }

                    if(row3sum!=actualsum)
                        System.out.println("The matrix is not a magic square!");
                    
                    else
                    {
                        for(i=0;i<rows;i++)
                        {
                            col1sum+=matrix[i][0];
                        }

                        if(col1sum!=actualsum)
                            System.out.println("The matrix is not a magic square!");
                        
                        else
                        {
                            for(i=0;i<rows;i++)
                            {
                                col2sum+=matrix[i][1];
                            }

                            if(col2sum!=actualsum)
                                System.out.println("The matrix is not a magic square!");
                            
                            else
                            {
                                for(i=0;i<rows;i++)
                                {
                                    col3sum+=matrix[i][2];
                                }

                                if(col3sum!=actualsum)
                                    System.out.println("The matrix is not a magic square!");
                                
                                else
                                {
                                    for(i=0, j=i;i<rows;i++, j++)
                                    {
                                        diag1sum+=matrix[i][j];
                                    }
                                    
                                    if(diag1sum!=actualsum)
                                        System.out.println("The matrix is not a magic square!");
                                    
                                    else
                                    {
                                        for(i=2, j=0; i>=0 && j<cols; i--, j++)
                                        {
                                            diag2sum+=matrix[i][j];
                                        }
                                        
                                        if(diag1sum!=actualsum)
                                            System.out.println("The matrix is not a magic square!");
                                        
                                        else
                                            System.out.println("\nThe given matrix IS A MAGIC SQUARE!\n\n");
                                        
                                        
                                    }
                                       
                                }
                            }
                            
                        }
                    
                    }
                    
                }
                
                
            }
            
        }
    }
  
}


public class Lab5exp1 
{

    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        int rows, cols, rows2, cols2, cnt;
        byte check;
        byte ch=0;
        int constant;
        boolean mat;
        
        System.out.println("Enter number of ROWS for Matrix 1:");
        rows = in.nextInt();
        
        System.out.println("Enter number of COLUMNS for Matrix 1:");
        cols = in.nextInt();
        
        Matrix M1 = new Matrix(rows, cols);                 //Creating Matrix1(new object)
        Matrix M2;                                      //Creating Matrix2(new object)
        
        
        
        
        System.out.print("\n\n");
        
        while(true)
        {
            System.out.println("1. Enter values in matrix.");
            System.out.println("2. Display values of the matrix.");
            System.out.println("3. Display values as string.");
            System.out.println("4. Find the transpose of the matrix.");
            System.out.println("5. Add two matrices.");
            System.out.println("6. Multiply two matrices.");
            System.out.println("7. Multiply a matrix with a scalar.");
            System.out.println("8. Check if the matrix is a magic square.");
            System.out.println("9. Display copied matrix using constructor.");
            System.out.println("0. Exit.");
            System.out.println("Enter your choice: ");
            ch= in.nextByte();
            
            switch(ch)
            {
                case 1: 
                {
                    M1.readMatrix();                                    //Calling Member Function to insert values.
                }
                
                break;
                
                case 2:
                {
                    check = M1.checkforempty();                                    //Calling Member Function to check whether matrix is empty or not.
                    if(check==1)
                    {
                        System.out.println("The matrix entered is: \n");
                        M1.displayMatrix();                                   //Calling Member Function to display the matrix..
                    }
                        
                    
                    else if(check==0)
                    {
                        System.out.println("\nEnter matrix 1 here: \n");
                        M1.readMatrix();                                    //Calling Member Function to insert values.
                        System.out.println("The matrix entered is: \n");
                        M1.displayMatrix();                                    //Calling Member Function to display the matrix.
                    }
                }
                break;
                
                case 3:
                {
                    M1.toString();                                    //Calling Member Function to convert the values to string.
                    System.out.println("The matrix entered is: \n" + M1);
                }
                
                break;
                
                case 4:
                {
                    check = M1.checkforempty();                                    //Calling Member Function to check whether matrix is empty or not.
                    if(check==1)
                        M1.transpose();                                    //Calling Member Function to find tranpose of the matrix.
                    
                    else if(check==0)
                    {
                        System.out.println("\nEnter matrix 1 here: \n");
                        M1.readMatrix();                                    //Calling Member Function to insert values.
                        M1.transpose();                                    //Calling Member Function to find tranpose of the matrix.
                    }
                }
                break;
                
                case 5:
                {
                    System.out.println("\nNOTE: For addition, the rows and columns of both matrices must be same!!\n");
                    System.out.println("Enter number of ROWS for Matrix 2: ");
                    rows2 = in.nextInt();
                    System.out.println("Enter number of COLUMNS for Matrix 2: ");
                    cols2 = in.nextInt();
                    M2 = new Matrix(rows2, cols2);                      //Initialising the object.
                    M2.readMatrix();                                    //Calling Member Function to insert values.
                    System.out.println("The matrix entered is: ");
                    M2.displayMatrix();                                    //Calling Member Function to display the matrix.
                    
                    check = M1.checkforempty();                                    //Calling Member Function to check whether matrix is empty or not.
                    
                    if(check==1)
                        M1.add(M2);                                    //Calling Member Function to add two matrices.
                    
                    else if(check==0)
                    {
                        System.out.println("\nEnter matrix 1 here: \n");
                        M1.readMatrix();                                    //Calling Member Function to insert values.
                        M1.add(M2);                                    //Calling Member Function to add two matrices.
                    }
                    
                }
                
                break;
                
                case 6:
                {
                    System.out.println("\nNOTE: For multiplication, the columns of first matrix and rows of second matrix must be same!!\n");
                    System.out.println("Enter number of ROWS for Matrix 2: ");
                    rows2 = in.nextInt();
                    System.out.println("Enter number of COLUMNS for Matrix 2: ");
                    cols2 = in.nextInt();
                    M2 = new Matrix(rows2, cols2);                  //Initialising the object.
                    M2.readMatrix();                                    //Calling Member Function to insert values.
                    System.out.println("The matrix entered is: ");
                    M2.displayMatrix();                                    //Calling Member Function to display the matrix.
                    
                    check = M1.checkforempty();                                    //Calling Member Function to check whether matrix is empty or not.
                    
                    if(check==1)
                        M1.multiply(M2);                                    //Calling Member Function to multiply two matrices.
                    
                    else if(check==0)
                    {
                        System.out.println("\nEnter matrix 1 here: \n");
                        M1.readMatrix();                                    //Calling Member Function to insert values.
                        M1.multiply(M2);                                    //Calling Member Function to multiply two matrices.
                    }
                    
                }
                
                break;
                
                case 7:
                {
                    System.out.println("Enter the constant: ");
                    constant = in.nextInt();
                    
                    check = M1.checkforempty();                                    //Calling Member Function to check whether matrix is empty or not.
                    
                    if(check==1)
                    {
                        M1.multiply(constant);                                    //Calling Member Function to multiply a constant with the matrix.
                        System.out.println("\nThe matrix after multiplication is: \n");
                        M1.displayMatrix();                                    //Calling Member Function to display the matrix.
                    }
                        
                    
                    else if(check==0)
                    {
                        M1.readMatrix();                                    //Calling Member Function to insert values into the matrix.
                        M1.multiply(constant);                                    //Calling Member Function to multiply a constant with the matrix.
                        System.out.println("\nThe matrix after multiplication is: \n");
                        M1.displayMatrix();                                    //Calling Member Function to display the matrix.
                    }
                    
                }
                break;
                
                case 8:
                {
                    check = M1.checkforempty();                                    //Calling Member Function to check whether matrix is empty or not.
                    
                    if(check==1)
                    {
                        mat=M1.checkelements();                                    //Calling Member Function to check whether matrix is valid to be magic square.
                        if(mat==true)
                            M1.magicsquare();                                    //Calling Member Function to check whether matrix is a magic square or not.
                    }
                        
                    
                    else if(check==0)
                    {
                        System.out.println("\nEnter matrix 1 here: \n");
                        M1.readMatrix();                                    //Calling Member Function to insert values.
                        mat=M1.checkelements();                                    //Calling Member Function to check whether matrix is valid to be magic square.
                        if(mat==true)
                            M1.magicsquare();                                    //Calling Member Function to check whether matrix is a magic square or not.
                        
                        else if(mat==false)
                            System.out.println("The matrix is not a magic square!");
                    }
                    
                }
                break;
                
                case 9:
                {
                    Matrix copy = new Matrix(M1);                                    //Calling Copy Constructor to create a copy of the object.
                    System.out.println("The copied matrix is: \n");
                    copy.displayMatrix();                                    //Calling Member Function(of Copied Object) to display the matrix.
                  
                }
                break;
                
                case 0: return;                             //To exit the loop.
                    
                     
                    
            }
        }
        
    }
    
}